<?php
  /**
   * Setup
   */
  $cs = Yii::app()->clientScript;
  $themePath = Yii::app()->theme->baseUrl;

  /**
   * StyleSheets BootSwatch Cosmo
   */
  $cs->registerCssFile($themePath . '/assets/bootstrap/css/bootstrap.css');

  $cs->registerCssFile($themePath . '/assets/bootstrap/css/bootswatch.min.css');

  /**
   * StyleSheets FontAwesome
   */
  $cs->registerCssFile($themePath . '/assets/fontawesome/css/font-awesome.min.css');

  /**
   * Main StyleSheet
   */
  $cs->registerCssFile($themePath . '/assets/css/main.css');

  /**
   * JavaScripts
  */
  $cs->registerCoreScript('jquery', CClientScript::POS_END);
  $cs->registerCoreScript('jquery.ui', CClientScript::POS_END);
  $cs->registerScriptFile($themePath . '/assets/bootstrap/js/bootstrap.min.js', CClientScript::POS_END);
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>ClickTecnologie</title>
		<meta property="og:title" content="ClickTecnologie">
        <meta property="og:description" content="TransitaliaExpress - <descrizione da inserire>">
        <meta property="og:image" content="">
        <meta property="og:site_name" content="ClickTecnologie">
        <meta name="Description" content="ClickTecnologie - <descrizione da inserire>">
        <meta name="Keywords" content="">
        <meta name="author" content="">
        <meta name="copyright" content="">
        <meta http-equiv="Reply-to" content="">
        <meta http-equiv="content-language" content="IT">
        <meta name="ROBOTS" content="INDEX,FOLLOW">
        <meta name="creation_Date" content="05/04/2013">
        <meta name="revisit-after" content="7 days">  
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Google Analytics JS
        <script type="text/javascript">
           var _gaq = _gaq || [];
           _gaq.push(['_setAccount', 'UA-36417136-2']);
           _gaq.push(['_trackPageview']);

               (function() { 
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
              })();
        </script>-->
	</head>
	<body>
		<div id="mainmenu"></div>
		<?php echo $content; ?>
		<div id="footer"></div>
	</body>
</html>
