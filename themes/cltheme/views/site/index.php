<?php
	$themePath = Yii::app()->theme->baseUrl;
	$cs = Yii::app()->clientScript;
  	$cs->registerCssFile($themePath . '/assets/css/home.css');
?>
<div id="overlay"></div>
<div class="container-fluid">
	<nav class="navbar navbar-default navbar-home" role="navigation"></nav>
	<div class="jumbotron" id="home-payoff"></div>
</div>