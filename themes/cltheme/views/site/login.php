<?php
	$themePath = Yii::app()->theme->baseUrl;
?>
<style type="text/css">
    body{
        background-color: #4A90E2;
    }
</style>
<div class="container">
	<div class="row">
        <div class="col-md-6">
            <?php echo CHtml::image($themePath . '/assets/images/logo.png'); ?>
        </div>
		<div class="col-md-6">
			<h1>Login</h1>
			<div class="form">
				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'login-form',
					'enableClientValidation'=>true,
					'clientOptions'=>array(
						'validateOnSubmit'=>true,
					),
				)); ?>
					<p class="note">I campi contrassegnati da <span class="required">*</span> sono obbligatori.</p>
					<div class="form-group">
						<?php echo $form->labelEx($model, 'username'); ?>
						<?php echo $form->textField($model, 'username', array("class" => "form-control")); ?>
						<?php echo $form->error($model,'username'); ?>
					</div>
					<div class="form-group">
						<?php echo $form->labelEx($model,'password'); ?>
						<?php echo $form->passwordField($model,'password', array("class" => "form-control")); ?>
						<?php echo $form->error($model,'password'); ?>
					</div>

					<div class="form-group">
						<?php echo CHtml::submitButton('Login', array("class" => "btn btn-success btn-block btn-login")); ?>
					</div>
				<?php $this->endWidget(); ?>
			</div><!-- form -->
		</div>
	</div>
</div>