<?php

class m151031_102755_add_update_time_cliente extends CDbMigration
{
	public function up()
	{
        $this->addColumn("tbl_cliente", "update_time", "datetime NOT NULL");
	}
}