<?php

class m151106_103029_add_updatetime_trattativa extends CDbMigration
{
	public function up()
	{
        $this->addColumn("tbl_trattative", "update_time", "datetime NOT NULL");
	}

    public function down()
    {
        $this->dropColumn("tbl_trattative", "update_time");
    }
}