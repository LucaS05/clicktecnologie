<?php

class ApiController extends Controller
{
	public function actionView()
	{
	    // Check if id was submitted via GET
	    if(!isset($_GET['id']))
	        $this->_sendResponse(500, 'Error: Parameter <b>id</b> is missing' );
	 
	    switch($_GET['model'])
	    {
	        case "cliente":
                $idCliente = Yii::app()->request->getParam("id");
                if(isset($idCliente)){
                    $modelCliente = Cliente::model()->findByPk($idCliente);
                    if(isset($modelCliente)){
                        $model = $modelCliente->getAttributes();
                        if(isset($modelCliente->privato)){
                            $model["cod_fisc"] = $modelCliente->privato->codfisc;
                            $model["privato"] = true;
                        } else if(isset($modelCliente->societa)){
                            $model["rag_soc"] = $modelCliente->societa->getNomeSocieta();
                            $model["tipoSocieta"] = $modelCliente->societa->getTipoSocieta();
                            $model["piva"] = $modelCliente->societa->piva;
                            $model["societa"] = true;
                        }
                    } else{
                        $model = null;
                    }
                } else {
                    $this->_sendResponse(500, "Errore: Parametro <b>id</b> mancante" );
                }
	            break;

            case "trattativa":
                $codice = Yii::app()->request->getParam("id");
                if(isset($codice)){
                    $idAgente = Yii::app()->user->idagente;
                    $trattative = Trattative::model()->findAll("codice=:codice",
                        array(":codice" => $codice));
                    if(isset($trattative)){
                        $modelTrattativa = null;
                        foreach($trattative as $trattativa){
                            $agenteTrattativa = $trattativa->trattativaClie->agente;
                            if($agenteTrattativa === $idAgente) {
                                $modelTrattativa = $trattativa;
                            }
                        }
                        if(isset($modelTrattativa)){
                            $modelCliente = $modelTrattativa->trattativaClie;
                            $modelCliente->rag_soc = $modelCliente->getViewRagSoc();
                            $modelTrattativa->cliente = $modelCliente;
                            $model = $modelTrattativa;
                        } else {
                            $this->_sendResponse(404, "Trattativa con codice " . $codice . " non trovata!");
                        }
                    } else{
                        $this->_sendResponse(404, "Trattativa con codice " . $codice . " non trovata!");
                    }
                }
                break;

	        default:
	            $this->_sendResponse(501, sprintf(
	                'Mode <b>view</b> is not implemented for model <b>%s</b>',
	                $_GET['model']) );
	            Yii::app()->end();
	    }

	    // Did we find the requested model? If not, raise an error
	    if(is_null($model))
	        $this->_sendResponse(404, 'No Item found with id '.$_GET['id']);
	    else
	        $this->_sendResponse(200, CJSON::encode($model), "application/json");
	}

	public function actionList()
	{
	    switch($_GET['model'])
	    {
	        case 'documenti':
                //TODO: Aggiungere controllo sul tipo di utente
                $criteria = new CDbCriteria();
                $criteria->order = "data ASC";
                $models = array();
                $documenti = Documento::model()->findAll($criteria);
                foreach($documenti as $documento){
                    $modelAgente = Agente::model()->findByPk($documento->autore);
                    $file = basename($documento->path);
                    $documento->path = $file;
                    $attrsDocumento = $documento->attributes;
                    $attrsDocumento["autore"] = $modelAgente->nome . " " . $modelAgente->cognome;
                    $documento->setAttributes($attrsDocumento);
                    array_push($models, $documento);
                }
            break;

            case 'agenti':
                $criteria = new CDbCriteria();
                $criteria->order = "nome ASC";
                $models = array();
                $agenti = Agente::model()->findAll($criteria);

                foreach ($agenti as $agente) {
                    array_push($models, $agente);
                }
                break;

            case "tipoagente":
                $models = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('tbl_tipoagente')
                    ->queryAll();
                break;

            case 'messaggi':
                $criteria = new CDbCriteria();
                $criteria->order = "data DESC";
                $models = array();
                $messaggi = Messaggio::model()->findAll($criteria);

                foreach ($messaggi as $messaggio) {
                    $trimCont = (strlen($messaggio->contenuto) > 124) ? substr($messaggio->contenuto, 0, 121) . '...' : $messaggio->contenuto;
                    $messaggio->contenuto = $trimCont;
                    $messaggio->mittente = Agente::model()->findByPk(2);
                    array_push($models, $messaggio);
                }
                break;

            case 'messaggio':
                $idMessaggio = $_GET["idmsg"];
                $models = array();
                $messaggio = Messaggio::model()->findByPk($idMessaggio);
                array_push($models, $messaggio);
                break;

            case 'modcod':
                $codMod = Yii::app()->request->getParam("codice");
                $modello = Modello::model()->find("codice=:cod", array(":cod" => $codMod));
                $modello->nome = $modello->nomeModello->nome;
                $modello->tipo = $modello->tipoModello->nome;
                $models = array("modello" => $modello);
                break;

            case 'modwatt':
                $models = array();
                $idTipo = Yii::app()->request->getParam("tipo");
                $watt = Yii::app()->request->getParam("watt");
                $tipoPlafoniera = TipoPlafoniere::model()->find("nome=:nome", array(":nome" => $idTipo));
                $modelli = Modello::model()->findAll(
                    "watt_nom=:watt AND tipo=:tipo", array(":watt" => $watt, ":tipo" => $tipoPlafoniera->id));
                foreach ($modelli as $modello) {
                    $modAttributes = $modello->attributes;
                    $wattInduzione = $modello->watt_nom;
                    $modAttributes["nome_induzione"] = $modello->nomeModello->nome;
                    $modAttributes["watt_induzione"] = $wattInduzione;
                    array_push($models, $modAttributes);
                }
                $data = $this->modelGrid($models, 3);
                $models = array("modelli" => $data);
                break;

            case 'modello':
                //SELECT m.id, m.codice, s.nome, m.watt_nom, m.prezzo, m.watt_reali, t.nome AS tipo
                //FROM tbl_modello AS m, tbl_modname AS s, tbl_modtipo AS t
                //WHERE m.nome = s.id AND s.nome LIKE "%ar%" AND m.tipo = t.id
                $nomeCodMod = Yii::app()->request->getParam("mod");
                $wattMin = Yii::app()->request->getParam("watt_min");
                $wattMax = Yii::app()->request->getParam("watt_max");
                $wattEx = Yii::app()->request->getParam("watt");
                $prezzoMin = Yii::app()->request->getParam("prezzo_min");
                $prezzoMax = Yii::app()->request->getParam("prezzo_max");
                $prezzoEx = Yii::app()->request->getParam("prezzo");
                $idTipo = Yii::app()->request->getParam("tipo");

                $queryMod = Yii::app()->db->createCommand();

                $queryWhere = array('and', 'm.stato = 1', "m.nome = s.id", "m.tipo = t.id");
                $queryParams = array();

                if(isset($nomeCodMod)){
                    $orNomeCod = array('or',
                        array('like', 'm.codice', '%'. $nomeCodMod .'%'),
                        array('like', 's.nome', '%'. $nomeCodMod .'%'));
                    array_push($queryWhere, $orNomeCod);
                }

                if(isset($idTipo)){
                    parse_str($idTipo);
                    if(is_array($tipo)){
                        if(count($tipo) > 1){
                            $i = 0;
                            $orArray = array('or');
                            foreach($tipo as $elem){
                                $orParam = ":tipo" . $i;
                                array_push($orArray, "t.id=" . $orParam);
                                $queryParams[$orParam] = $elem;
                                $i++;
                            }
                            array_push($queryWhere, $orArray);
                        } else if(count($tipo) == 1){
                            array_push($queryWhere, "t.id=:tipo");
                            $queryParams[":tipo"] = $tipo[0];
                        }
                    } else {
                        array_push($queryWhere, "t.id=:tipo");
                        $queryParams[":tipo"] = $idTipo;
                    }
                }

                if(isset($wattMin) && isset($wattMax)){
                    array_push($queryWhere, 'm.watt_nom>=:wattmin');
                    array_push($queryWhere, 'm.watt_nom<=:wattmax');
                    $queryParams[":wattmin"] = $wattMin;
                    $queryParams[":wattmax"] = $wattMax;
                    $queryMod->order('watt_nom asc');
                }

                if(isset($wattEx)){
                    array_push($queryWhere, 'm.watt_nom=:watt');
                    $queryParams[":watt"] = $wattEx;
                }

                if(isset($prezzoMin) && isset($prezzoMax)){
                    array_push($queryWhere, 'm.prezzo>=:prezzomin');
                    array_push($queryWhere, 'm.prezzo<=:prezzomax');
                    $queryParams[":prezzomin"] = $prezzoMin;
                    $queryParams[":prezzomax"] = $prezzoMax;
                    $queryMod->order('prezzo asc');
                }

                if(isset($prezzoEx)){
                    array_push($queryWhere, 'm.prezzo=:prezzo');
                    $queryParams[":prezzo"] = $prezzoEx;
                }

                $queryMod->select('m.id, m.codice, s.nome, m.watt_nom, m.prezzo, m.watt_reali, t.nome as tipo');
                $queryMod->from('tbl_modello as m, tbl_modname as s, tbl_modtipo as t');

                if(count($queryParams) > 0){
                    $queryMod->where($queryWhere, $queryParams);
                    $modelli = $queryMod->queryAll();
                } else {
                    $queryMod->where($queryWhere);
                    $modelli = $queryMod->queryAll();
                }

                $models = array("modelli" => $modelli);
                break;

            case 'codici':
                $models = Yii::app()->db->createCommand()
                    ->selectDistinct('m.codice')
                    ->from('tbl_modello as m')
                    ->queryAll();
                break;

            case 'nomi':
                $nomeMod = Yii::app()->request->getParam("nomemod");
                $models = Yii::app()->db->createCommand()
                    ->selectDistinct('t.nome')
                    ->from('tbl_modello as m, tbl_modname as t')
                    ->where(array('and', array('like', 't.nome', '%' . $nomeMod . '%'), 'm.nome = t.id'))
                    ->queryAll();
                break;

            case 'plafoniere':
                $queryTipi = Yii::app()->db->createCommand()
                        ->selectDistinct("tbl_tipoplafoniere.nome")
                        ->from("tbl_tipoplafoniere")
                        ->join("tbl_modello", "tbl_tipoplafoniere.id = tbl_modello.tipo")
                        ->where("tbl_modello.stato = 1")
                        ->queryAll();
                $models = array("tipi" => $queryTipi);
                break;

            case 'clienti':
                $clienti = Cliente::model()->getClientiAgente();
                $tempClienti = array();
                foreach($clienti as $cliente){
                    $clienteAttributes = $cliente->getAttributes();
                    if(isset($cliente->societa)){
                        $clienteAttributes["rag_soc"] = $cliente->societa->getNomeSocieta();
                        $clienteAttributes["tipoSocieta"] = $cliente->societa->getTipoSocieta();
                    }
                    array_push($tempClienti, $clienteAttributes);
                }
                $models = array("clienti" => $tempClienti);
                break;

            case 'trattative':
                $models = array();
                $clienti = Cliente::getClientiAgente();
                foreach($clienti as $cliente){
                    foreach($cliente->trattatives as $trattativa){
                        $trattativa->cliente = $trattativa->getRagSocCliente();
                        array_push($models, $trattativa);
                    }
                }
                usort($models, function($a, $b) {
                    return strnatcmp($a->codice, $b->codice);
                });
                break;

            case 'lampesistenti':
                $models = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('tbl_tipolampade')
                    ->queryAll();
                break;

            case 'probabilita':
                $models = Yii::app()->db->createCommand()
                    ->select('id, nome')
                    ->from('tbl_probabilita')
                    ->queryAll();
                break;

            case 'wattplaf':
                $tipoPlafoniera = Yii::app()->request->getParam("tipo_plaf");
                $wattPlaf = $this->getWattPlafoniera($tipoPlafoniera);
                $models = array("watt" => $wattPlaf);
                break;

            case 'sostituzione':
                $data = $wattFilter = $prezzoFilter = array();
                $tipoLampada = Yii::app()->request->getParam("tipo_lampada");
                $wattLampada = Yii::app()->request->getParam("watt_lampada");
                $tipoPlafoniera = Yii::app()->request->getParam("tipo_plaf");

                $rows = Yii::app()->db->createCommand()
                    ->select('idinduzione, codice, nome_induzione, watt_induzione, prezzo')
                    ->from('sostituzionemodelli')
                    ->where(array('and',
                                array('like', 'sostituzionemodelli.tipo_lampada', '%' . $tipoLampada . '%'),
                                'sostituzionemodelli.watt_lampada=:wattlampada',
                                'sostituzionemodelli.tipo_plafoniera=:tipoplaf'),
                            array(":wattlampada" => $wattLampada, ":tipoplaf" => $tipoPlafoniera))
                    ->queryAll();

                $selectedWatt = $rows[0]["watt_induzione"];

                if(is_null($selectedWatt)){
                    $wattSostituzione = array();
                    $tempWattSost = Yii::app()->db->createCommand()
                        ->selectDistinct("watt_induzione")
                        ->from("sostituzionemodelli")
                        ->where(array("and",
                            "sostituzionemodelli.watt_lampada=:wattlampada",
                            "tipo_lampada=:tipolampada"), array(":wattlampada" => $wattLampada, ":tipolampada" => $tipoLampada))
                        ->queryAll();
                    foreach($tempWattSost as $watt){
                        array_push($wattSostituzione, $watt["watt_induzione"]);
                    }
                }

                $wattFilter = $this->getWattPlafoniera($tipoPlafoniera);

                $minMaxPrezzoPlaf = Yii::app()->db->createCommand()
                    ->select(array("MIN(prezzo) minPrezzo", "MAX(prezzo) maxPrezzo"))
                    ->from("tbl_modello")
                    ->join("tbl_tipoplafoniere", "tbl_modello.tipo = tbl_tipoplafoniere.id")
                    ->where("tbl_tipoplafoniere.nome=:tipoPlaf", array(":tipoPlaf" => $tipoPlafoniera))
                    ->queryAll();

                $data = $this->modelGrid($rows, 3);

                foreach($minMaxPrezzoPlaf as $prezzoPlaf){
                    array_push($prezzoFilter, $prezzoPlaf["minPrezzo"]);
                    array_push($prezzoFilter, $prezzoPlaf["maxPrezzo"]);
                }

                $models = array(
                    "modelli" => $data,
                    "wattFilter" => $wattFilter,
                    "prezzoFilter" => $prezzoFilter,
                    "selectedWatt" => $selectedWatt,
                    "wattSostituzione" => $wattSostituzione
                );

                break;

	        default:
	            // Model not implemented error
	            $this->_sendResponse(501, sprintf(
	                'Error: Mode <b>list</b> is not implemented for model <b>%s</b>',
	                $_GET['model']) );
	            Yii::app()->end();
	    }

	    if(empty($models)) {
	        $this->_sendResponse(200, CJSON::encode($models) );
	    } else {
	        $this->_sendResponse(200, CJSON::encode($models), "application/json");
	    }
	}

    public function actionCreate(){
        switch($_GET['model']){
            case 'modello':
                $model = new Trattative();
                $modelliTrattativa = CJSON::decode(file_get_contents("php://input"));
                if(isset($modelliTrattativa["modelli"])){
                    foreach($modelliTrattativa["modelli"] as $modello){
                        echo CVarDumper::dump($modelliTrattativa, 10, true);
                        $modelloEsistente = $modello["exMod"];
                        $codice = $modello["clMod"]["mod"]["codice"];
                        $clMod = Modello::model()->find("codice=:cod", array(":cod" => $codice));
                        if($modelloEsistente["status"]){
                            echo CVarDumper::dump($modelloEsistente, 10, true);
                        }
                    }
                }
                break;
            default:
                $this->_sendResponse(501,
                    sprintf('La modalità <b>create</b> non è implementata per il modello <b>%s</b>',
                        $_GET['model']) );
                Yii::app()->end();
        }
    }

	private function _sendResponse($status = 200, $body = '', $content_type = 'text/html')
	{
	    // set the status
	    $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
	    header($status_header);
	    // and the content type
	    header('Content-type: ' . $content_type);
	 
	    // pages with body are easy
	    if($body != '')
	    {
	        // send the body
	        echo $body;
	    }
	    // we need to create the body if none is passed
	    else
	    {
	        // create some body messages
	        $message = '';
	 
	        // this is purely optional, but makes the pages a little nicer to read
	        // for your users.  Since you won't likely send a lot of different status codes,
	        // this also shouldn't be too ponderous to maintain
	        switch($status)
	        {
	            case 401:
	                $message = 'You must be authorized to view this page.';
	                break;
	            case 404:
	                $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
	                break;
	            case 500:
	                $message = 'The server encountered an error processing your request.';
	                break;
	            case 501:
	                $message = 'The requested method is not implemented.';
	                break;
	        }
	 
	        // servers don't always have a signature turned on 
	        // (this is an apache directive "ServerSignature On")
	        $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];
	 
	        // this should be templated in a real-world solution
	        $body = '
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
	<html>
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	    <title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
	</head>
	<body>
	    <h1>' . $this->_getStatusCodeMessage($status) . '</h1>
	    <p>' . $message . '</p>
	    <hr />
	    <address>' . $signature . '</address>
	</body>
	</html>';
	 
	        echo $body;
	    }
	    Yii::app()->end();
	}

	private function _getStatusCodeMessage($status)
	{
	    // these could be stored in a .ini file and loaded
	    // via parse_ini_file()... however, this will suffice
	    // for an example
	    $codes = Array(
	        200 => 'OK',
	        400 => 'Bad Request',
	        401 => 'Unauthorized',
	        402 => 'Payment Required',
	        403 => 'Forbidden',
	        404 => 'Not Found',
	        500 => 'Internal Server Error',
	        501 => 'Not Implemented',
	    );
	    return (isset($codes[$status])) ? $codes[$status] : '';
	}

    /**
     * La funzione si occupa di organizzare l'intero array $rows in un array organizzato in gruppi.
     * Ciascun gruppo è formato da groupNum elementi che è a sua volta un array ($temp).
     * L'array finale facilita la visualizzazione dei modelli nell'interfaccia utente secondo il layout:
     * (Supponendo gruppi da 3)
     *                                                              
     *        +-----------------------------------------------+    
     *        |                                               |    
     *        |   +---------+    +---------+    +---------+   |    
     *        |   |         |    |         |    |         |   |    
     *        |   |         |    |         |    |         |   |    
     *        |   |         |    |         |    |         |   |    
     *        |   |         |    |         |    |         |   |    
     *        |   +---------+    +---------+    +---------+   |    
     *        |                                               |
     *        +-----------------------------------------------+    
     *                                                          
     * In questo modo l'elaborazione non è affidata al client, che si occuperà solo di visualizzare i dati.
     *
     * @param $rows     Array che rappresenta i risultati provenienti dal database.
     * @param $groupNum Indica il numero di elementi presenti in un singolo gruppo
     * @return array    Un array organizzato in gruppi
     */
    private function modelGrid($rows, $groupNum){
        $data = $temp = array();
        foreach($rows as $index => $modello){
            if(($index + 1) % $groupNum != 0){
                array_push($temp, $modello);
                if(($index + 1) == count($rows)){
                    array_push($data, $temp);
                }
            } else {
                array_push($temp, $modello);
                array_push($data, $temp);
                $temp = array();
            }
        }

        return $data;
    }

    /**
     * Ritorna l'elenco dei wattaggi possibili per un dato tipo di plafoniera.
     *
     * @param $tipoPlafoniera Tipo di plafoniera di cui ottenere i watt.
     * @return mixed Elenco dei possibili wattaggi.
     */
    private function getWattPlafoniera($tipoPlafoniera){
        $wattFilter = array();
        $wattPlaf = Yii::app()->db->createCommand()
            ->selectDistinct("watt_nom")
            ->from("tbl_modello")
            ->join("tbl_tipoplafoniere", "tbl_modello.tipo = tbl_tipoplafoniere.id")
            ->where("tbl_tipoplafoniere.nome=:tipoPlaf", array(":tipoPlaf" => $tipoPlafoniera))
            ->order('watt_nom asc')
            ->queryAll();
        foreach($wattPlaf as $wattArray){
            array_push($wattFilter, $wattArray["watt_nom"]);
        }
        return $wattFilter;
    }

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('list', 'view', 'create'),
                'roles' => array('agente')
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}
}