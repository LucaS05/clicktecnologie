<?php
return array(
    'class' => 'CDbAuthManager',
    'connectionID' =>'db',
    'itemTable' => 'tbl_auth_item',
    'itemChildTable' => 'tbl_auth_item_child',
    'assignmentTable' => 'tbl_auth_assignment',
);