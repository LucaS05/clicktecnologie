<?php

/**
 * This is the model class for table "tbl_modello".
 *
 * The followings are the available columns in table 'tbl_modello':
 * @property integer $id
 * @property string $codice
 * @property integer $nome
 * @property integer $watt_nom
 * @property string $prezzo
 * @property integer $watt_reali
 * @property integer $stato
 * @property integer $tipo
 * @property string $foto
 *
 * The followings are the available model relations:
 * @property Modname $nome0
 * @property Tipoplafoniere $tipo0
 * @property Lampade[] $tblLampades
 * @property Trattative[] $tblTrattatives
 */
class Modello extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_modello';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('codice, nome, watt_nom, prezzo, watt_reali, stato, tipo, foto', 'required'),
			array('nome, watt_nom, watt_reali, stato, tipo', 'numerical', 'integerOnly'=>true),
			array('codice, foto', 'length', 'max'=>255),
			array('prezzo', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, codice, nome, watt_nom, prezzo, watt_reali, stato, tipo, foto', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'nomeModello' => array(self::BELONGS_TO, 'NomeModello', 'nome'),
			'tipoModello' => array(self::BELONGS_TO, 'TipoPlafoniere', 'tipo'),
			'tblLampades' => array(self::MANY_MANY, 'Lampade', 'tbl_sostituzione(tipo_induzione, lampada)'),
			'tblTrattatives' => array(self::MANY_MANY, 'Trattative', 'tbl_trattmodcl(idmod, idtratt)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'codice' => 'Codice',
			'nome' => 'Nome',
			'watt_nom' => 'Watt Nom',
			'prezzo' => 'Prezzo',
			'watt_reali' => 'Watt Reali',
			'stato' => 'Stato',
			'tipo' => 'Tipo',
			'foto' => 'Foto',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('codice',$this->codice,true);
		$criteria->compare('nome',$this->nome);
		$criteria->compare('watt_nom',$this->watt_nom);
		$criteria->compare('prezzo',$this->prezzo,true);
		$criteria->compare('watt_reali',$this->watt_reali);
		$criteria->compare('stato',$this->stato);
		$criteria->compare('tipo',$this->tipo);
		$criteria->compare('foto',$this->foto,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Modello the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
