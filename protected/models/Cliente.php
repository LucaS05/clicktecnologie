<?php

/**
 * This is the model class for table "tbl_cliente".
 *
 * The followings are the available columns in table 'tbl_cliente':
 * @property integer $id
 * @property string $rag_soc
 * @property integer $agente
 * @property integer $citta
 * @property string $prima_visita
 * @property string $referente
 * @property string $email_referente
 * @property string $telefono_referente
 * @property integer $stato
 * @property string $data
 * @property string $update_time
 *
 * The followings are the available model relations:
 * @property Comuni $relCitta
 * @property Agente $agente0
 * @property Privato $privato
 * @property Schedacliente[] $schedaclientes
 * @property Societa $societa
 * @property Trattative[] $trattatives
 */
class Cliente extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_cliente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rag_soc, agente, citta, prima_visita, referente, email_referente, telefono_referente', 'required'),
			array('agente, citta, stato', 'numerical', 'integerOnly' => true),
            array('rag_soc, agente, citta, prima_visita, stato, referente, email_referente, telefono_referente', 'safe'),
			array('rag_soc, referente', 'length', 'max' => 255),
			array('email_referente', 'length', 'max' => 50),
			array('telefono_referente', 'length', 'max' => 20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, rag_soc, agente, citta, prima_visita, stato, referente, email_referente, telefono_referente', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'relCitta' => array(self::BELONGS_TO, 'Comuni', 'citta'),
            'agente0' => array(self::BELONGS_TO, 'Agente', 'agente'),
            'privato' => array(self::HAS_ONE, 'Privato', 'cliente'),
            'schedaclientes' => array(self::HAS_MANY, 'Schedacliente', 'cliente'),
            'societa' => array(self::HAS_ONE, 'Societa', 'cliente'),
            'trattatives' => array(self::HAS_MANY, 'Trattative', 'cliente')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'rag_soc' => 'Rag Soc',
			'agente' => 'Agente',
			'citta' => 'Citta',
			'prima_visita' => 'Prima Visita',
			'stato' => 'Stato',
			'referente' => 'Referente',
			'email_referente' => 'Email Referente',
			'telefono_referente' => 'Telefono Referente',
			'data' => 'Data',
			'update_time' => 'Update Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('rag_soc',$this->rag_soc,true);
		$criteria->compare('agente',$this->agente);
		$criteria->compare('citta',$this->citta);
		$criteria->compare('prima_visita',$this->prima_visita,true);
		$criteria->compare('stato',$this->stato);
		$criteria->compare('referente',$this->referente,true);
		$criteria->compare('email_referente',$this->email_referente,true);
		$criteria->compare('telefono_referente',$this->telefono_referente,true);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('update_time',$this->update_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cliente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function getClientiAgente(){
        $idAgente = Yii::app()->user->idagente;
        $criteria = new CDbCriteria();
        $criteria->addCondition("agente=:agente AND stato = 1");
        $criteria->params = array(':agente' => $idAgente);
        return Cliente::model()->findAll($criteria);
    }

    public function afterFind(){
        parent::afterFind();
        $cittaCliente = Comuni::model()->findByPk($this->citta);
        $this->citta = $cittaCliente->nome;
    }

    public function getViewRagSoc(){
        $ragSoc = null;
        if(isset($this->privato)){
            $ragSoc = $this->privato->getViewRagSoc();
        } else if(isset($this->societa)){
            $ragSoc = $this->societa->getViewRagSoc();
        }
        return $ragSoc;
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'data',
                'updateAttribute' => 'update_time',
                'setUpdateOnCreate' => true,
            )
        );
    }
}
