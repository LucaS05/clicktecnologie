<?php

/**
 * This is the model class for table "tbl_trattative".
 *
 * The followings are the available columns in table 'tbl_trattative':
 * @property integer $id
 * @property integer $cliente
 * @property integer $prob
 * @property integer $mese_acq
 * @property integer $stato
 * @property string $codice
 * @property string $data
 * @property integer $importo
 * @property integer $causale
 * @property integer $gg_scadenza
 * @property string $note
 * @property integer $categoria
 *
 * The followings are the available model relations:
 * @property Businessplan[] $businessplans
 * @property Categorietratt $trattativaCat
 * @property Causale $trattativaCaus
 * @property Cliente $trattativaClie
 * @property Mesi $trattativaMese
 * @property Statitrat $stato0
 * @property Probabilita $trattativaProb
 * @property Modello[] $tblModellos
 * @property Tipolampade[] $tblTipolampades
 */
class Trattative extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_trattative';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cliente, prob, mese_acq, codice, importo, causale, gg_scadenza, categoria', 'required'),
			array('cliente, prob, mese_acq, stato, importo, causale, gg_scadenza, categoria', 'numerical', 'integerOnly'=>true),
			array('codice', 'length', 'max'=>255),
			array('data, note', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, cliente, prob, mese_acq, stato, codice, data, importo, causale, gg_scadenza, note, categoria', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'businessplans' => array(self::HAS_MANY, 'Businessplan', 'trattativa'),
			'trattativaCat' => array(self::BELONGS_TO, 'Categorietratt', 'categoria'),
			'trattativaCaus' => array(self::BELONGS_TO, 'Causale', 'causale'),
			'trattativaClie' => array(self::BELONGS_TO, 'Cliente', 'cliente'),
			'trattativaMese' => array(self::BELONGS_TO, 'Mesi', 'mese_acq'),
			'stato0' => array(self::BELONGS_TO, 'Statitrat', 'stato'),
			'trattativaProb' => array(self::BELONGS_TO, 'Probabilita', 'prob'),
			'tblModellos' => array(self::MANY_MANY, 'Modello', 'tbl_trattmodcl(idtratt, idmod)'),
			'tblTipolampades' => array(self::MANY_MANY, 'Tipolampade', 'tbl_trattmodes(idtratt, idesis)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cliente' => 'Cliente',
			'prob' => 'Prob',
			'mese_acq' => 'Mese Acq',
			'stato' => 'Stato',
			'codice' => 'Codice',
			'data' => 'Data',
			'importo' => 'Importo',
			'causale' => 'Causale',
			'gg_scadenza' => 'Gg Scadenza',
			'note' => 'Note',
			'categoria' => 'Categoria',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cliente',$this->cliente);
		$criteria->compare('prob',$this->prob);
		$criteria->compare('mese_acq',$this->mese_acq);
		$criteria->compare('stato',$this->stato);
		$criteria->compare('codice',$this->codice,true);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('importo',$this->importo);
		$criteria->compare('causale',$this->causale);
		$criteria->compare('gg_scadenza',$this->gg_scadenza);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('categoria',$this->categoria);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Trattative the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function findTrattativeCliente($idcliente){
        $criteria = new CDbCriteria();
        $criteria->addCondition("cliente=:idcliente AND stato != :stato");
        $criteria->params = array(':stato' => 6, ':idcliente' => $idcliente);
        return Trattative::model()->findAll($criteria);
    }

    public function getRagSocCliente(){
        $cliente = $this->trattativaClie;
        $ragSoc = null;
        if(isset($cliente)){
            $ragSoc = $cliente->getViewRagSoc();
        }
        return $ragSoc;
    }

    public function afterFind(){
        parent::afterFind();
        $this->categoria = $this->trattativaCat->nome;
        $this->causale = $this->trattativaCaus->nome;
        $this->prob = ucfirst($this->trattativaProb->nome);
        $this->mese_acq = $this->trattativaMese->nome;
        $dataTrattativa = new DateTime($this->data);
        $this->data = $dataTrattativa->format("d/m/Y");
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'data',
                'updateAttribute' => 'update_time',
                'setUpdateOnCreate' => true,
            )
        );
    }
}
