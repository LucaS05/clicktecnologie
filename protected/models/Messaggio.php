<?php

/**
 * This is the model class for table "tbl_messaggio".
 *
 * The followings are the available columns in table 'tbl_messaggio':
 * @property integer $id
 * @property integer $mittente
 * @property integer $destinatario
 * @property string $contenuto
 * @property string $data
 * @property integer $stato
 *
 * The followings are the available model relations:
 * @property User $destinatario0
 * @property User $mittente0
 */
class Messaggio extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_messaggio';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('contenuto, data, stato', 'required'),
			array('mittente, destinatario, stato', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, mittente, destinatario, contenuto, data, stato', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'destinatario0' => array(self::BELONGS_TO, 'User', 'destinatario'),
			'mittente0' => array(self::BELONGS_TO, 'User', 'mittente'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'mittente' => 'Mittente',
			'destinatario' => 'Destinatario',
			'contenuto' => 'Contenuto',
			'data' => 'Data',
			'stato' => 'Stato',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('mittente',$this->mittente);
		$criteria->compare('destinatario',$this->destinatario);
		$criteria->compare('contenuto',$this->contenuto,true);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('stato',$this->stato);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Messaggio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'data'
            )
        );
    }
}
