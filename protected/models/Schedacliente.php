<?php

/**
 * This is the model class for table "tbl_schedacliente".
 *
 * The followings are the available columns in table 'tbl_schedacliente':
 * @property integer $id
 * @property string $nome
 * @property string $cognome
 * @property integer $piva
 * @property string $cod_fisc
 * @property string $indirizzo
 * @property integer $cap
 * @property string $email
 * @property integer $mod_pag
 * @property integer $fido
 * @property integer $cliente
 *
 * The followings are the available model relations:
 * @property Cltbanche[] $cltbanches
 * @property Cltmodpag[] $cltmodpags
 * @property Cliente $cliente0
 */
class Schedacliente extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_schedacliente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nome, cognome, piva, cod_fisc, indirizzo, cap, email, mod_pag, fido, cliente', 'required'),
			array('piva, cap, mod_pag, fido, cliente', 'numerical', 'integerOnly'=>true),
			array('nome, cognome, indirizzo, email', 'length', 'max'=>255),
			array('cod_fisc', 'length', 'max'=>16),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nome, cognome, piva, cod_fisc, indirizzo, cap, email, mod_pag, fido, cliente', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cltbanches' => array(self::HAS_MANY, 'Cltbanche', 'cliente'),
			'cltmodpags' => array(self::HAS_MANY, 'Cltmodpag', 'cliente'),
			'cliente0' => array(self::BELONGS_TO, 'Cliente', 'cliente'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nome' => 'Nome',
			'cognome' => 'Cognome',
			'piva' => 'Piva',
			'cod_fisc' => 'Cod Fisc',
			'indirizzo' => 'Indirizzo',
			'cap' => 'Cap',
			'email' => 'Email',
			'mod_pag' => 'Mod Pag',
			'fido' => 'Fido',
			'cliente' => 'Cliente',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('cognome',$this->cognome,true);
		$criteria->compare('piva',$this->piva);
		$criteria->compare('cod_fisc',$this->cod_fisc,true);
		$criteria->compare('indirizzo',$this->indirizzo,true);
		$criteria->compare('cap',$this->cap);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('mod_pag',$this->mod_pag);
		$criteria->compare('fido',$this->fido);
		$criteria->compare('cliente',$this->cliente);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Schedacliente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
