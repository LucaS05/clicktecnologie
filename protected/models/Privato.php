<?php

/**
 * This is the model class for table "tbl_privato".
 *
 * The followings are the available columns in table 'tbl_privato':
 * @property integer $id
 * @property integer $cliente
 * @property string $codfisc
 *
 * The followings are the available model relations:
 * @property Cliente $relCliente
 */
class Privato extends CActiveRecord implements ClienteI
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName(){
		return 'tbl_privato';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules(){
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cliente, codfisc', 'required'),
			array('cliente', 'numerical', 'integerOnly'=>true),
			array('codfisc', 'length', 'max'=>16),
            array('codfisc', 'unique'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, cliente, codfisc', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations(){
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'relCliente' => array(self::BELONGS_TO, 'Cliente', 'cliente'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels(){
		return array(
			'id' => 'ID',
			'cliente' => 'Cliente',
			'codfisc' => 'Codfisc',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search(){
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cliente',$this->cliente);
		$criteria->compare('codfisc',$this->codfisc,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Privato the static model class
	 */
	public static function model($className=__CLASS__){
		return parent::model($className);
	}

    public function beforeSave(){
        if(parent::beforeSave()) {
            $this->codfisc = strtoupper($this->codfisc);
            return true;
        }
        return false;
    }

    public function getViewRagSoc(){
        return $this->relCliente->rag_soc;
    }
}