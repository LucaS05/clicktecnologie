<?php

class TrattativeController extends Controller
{
    private function getModelTipoCliente($cliente, $modelCliente){
        $tipoCliente = null;
        if(isset($cliente) && isset($modelCliente)){
            if($this->isPrivato($cliente)){
                $tipoCliente = new Privato();
                $tipoCliente->codfisc = $cliente["cod_fisc"];
            }else if($this->isSocieta($cliente)){
                $tipoCliente = new Societa();
                $tipoCliente->piva = $cliente["piva"];
                $modelCliente->rag_soc .= " " . $cliente["tipoSocieta"];
            }
        }
        return $tipoCliente;
    }

    private function getModelCliente($cliente, $idAgente){
        $modelCliente = null;
        if(isset($idAgente)){
            $modelCliente = new Cliente();
            $comune = $cliente["comune"];
            $modelComune = null;
            $criteria = new CDbCriteria();
            $criteria->addCondition("nome=:nome");
            $criteria->params = array(':nome' => $comune["provincia"]);

            $provincia = Province::model()->find($criteria);
            foreach($provincia->comuni as $c){
                if($c->nome = $comune["comune"]){
                    $modelComune = $c;
                    break;
                }
            }
            $modelCliente->citta = $modelComune->id;
            $modelCliente->rag_soc = $cliente["rag_soc"];
            if(isset($cliente["referente"])){
                $modelCliente->referente = $cliente["referente"];
            }
            $modelCliente->agente = $idAgente;
        }
        return $modelCliente;
    }

    private function getCliente($cliente, $idAgente){
        $modelCliente = null;
        if(isset($cliente) && isset($idAgente)) {
            $criteria = new CDbCriteria();
            $criteria->addCondition("id=:idcliente AND agente=:agente");
            $criteria->params = array(':agente' => $idAgente, ':idcliente' => $cliente);
            $modelCliente = Cliente::model()->find($criteria);
        }
        return $modelCliente;
    }

    private function getSocieta($partitaIva){
        $modelSocieta = null;
        if(isset($partitaIva)) {
            $criteria = new CDbCriteria();
            $criteria->addCondition("piva=:piva");
            $criteria->params = array(':piva' => $partitaIva);
            $modelSocieta = Societa::model()->find($criteria);
        }
        return $modelSocieta;
    }

    private function getPrivato($codiceFiscale){
        $modelPrivato = null;
        if(isset($codiceFiscale)) {
            $criteria = new CDbCriteria();
            $criteria->addCondition("codfisc=:cf");
            $criteria->params = array(':cf' => $codiceFiscale);
            $modelPrivato = Privato::model()->find($criteria);
        }
        return $modelPrivato;
    }

    private function isSocieta($cliente){
        $societa = false;
        if(isset($cliente)){
            $societa = $cliente["societa"];
        }
        return $societa;
    }

    private function isPrivato($cliente){
        $privato = false;
        if(isset($cliente)){
            $privato = $cliente["privato"];
        }
        return $privato;
    }

    private function isClienteEsistente($cliente, $idAgente){
        $modelCliente = false;
        if(isset($cliente) && isset($idAgente)) {
            if ($this->isSocieta($cliente)) {
                $piva = $cliente["piva"];
                $modelSocieta = $this->getSocieta($piva);
                if (isset($modelSocieta->cliente)) {
                    $modelCliente = $this->getCliente($modelSocieta->cliente, $idAgente);
                }else{
                    $modelCliente = false;
                }
            } else if($this->isPrivato($cliente)){
                $codfisc = $cliente["cod_fisc"];
                $modelPrivato = $this->getPrivato($codfisc);
                if (isset($modelPrivato->cliente)) {
                    $modelCliente = $this->getCliente($modelPrivato->cliente, $idAgente);
                } else {
                    $modelCliente = false;
                }
            }
        }
        return $modelCliente;
    }

    private function getModelAgente($idAgente){
        $modelAgente = null;
        if(isset($idAgente)){
            $criteria = new CDbCriteria();
            $criteria->addCondition("id=:idagente");
            $criteria->params = array(':idagente' => $idAgente);
            $modelAgente = Agente::model()->find($criteria);
        }
        return $modelAgente;
    }

    private function getIDByNome($table, $nome){
        $data = Yii::app()->db->createCommand()
            ->select('*')
            ->from($table)
            ->where('nome=:nome', array(':nome' => $nome))
            ->queryRow();
        return $data["id"];
    }

    private function getCausale($causale){
        if(isset($causale)){
            return $this->getIDByNome("tbl_causale", $causale);
        }
    }

    private function getCategoria($categoria){
        return $this->getIDByNome("tbl_categorietratt", $categoria);
    }

    private function getMese($mese){
        return $this->getIDByNome("tbl_mesi", $mese);
    }

    private function getProbabilita($prob){
        return $this->getIDByNome("tbl_probabilita", $prob["nome"]);
    }

    private function getClientiAgente(){
        return Cliente::model()->getClientiAgente();
    }

    private function getNumeroTrattativa($idAgente){
        $clienti = $this->getClientiAgente();
        $count = 0;
        foreach($clienti as $cliente){
            $trattative = Trattative::model()->findTrattativeCliente($cliente->id);
            $count += count($trattative);
        }
        return $count + 1;
    }

    private function getCodiceTrattativa($modelAgente){
        $codice = $modelAgente->nome[0];
        $codice .= $modelAgente->cognome[0];
        $codice .= date("Y");
        $codice .= $this->getNumeroTrattativa($modelAgente->id);
        return $codice;
    }


    private function getModelTrattativa($trattativa, $modelAgente){
        $modelTrattativa = null;
        if(isset($trattativa) && isset($modelAgente)){
            $modelTrattativa = new Trattative();
            $modelTrattativa->causale = $this->getCausale($trattativa["causale"]);
            $modelTrattativa->categoria = $this->getCategoria($trattativa["categoria"]);
            $modelTrattativa->mese_acq = $this->getMese($trattativa["mese"]);
            $modelTrattativa->prob = $this->getProbabilita($trattativa["probabilita"]);
            $modelTrattativa->importo = $trattativa["importo"];
            $modelTrattativa->gg_scadenza = $trattativa["scadenza"];
            $modelTrattativa->codice = $this->getCodiceTrattativa($modelAgente);
            if(isset($trattativa["note"])){
                $modelTrattativa->note = $trattativa["note"];
            }
        }
        return $modelTrattativa;
    }

    /**
     * Il metodo crea una trattativa a partire da un oggetto JSON proviente da una richiesta POST.
     * La trattativa viene salvata nello stato iniziale secondo lo schema:
     * (1) Trattativa -> (2) Creazione Business Plan -> (3) Creazione Preventivo -> (4) Inserimento cliente.
     * La trattativa inserita in questo metodo si troverà nello stato 1.
     * Se la trattativa è stata impostata correttamente, allora viene inserita nel database calcolando
     * i campi necessari.
     * - Il codice della trattativa è calcolato così:
     *      Codice Trattativa = Iniziali Agente + Anno + Numero + [revisione]
     *      Esempio: Mario Rossi, 2015, Trattativa #2: mr201502
     *               Mario Rossi, 2015, Trattativa #2, Revisione 1: mz201501_rev1
     * - La Causale della trattativa è anzitutto determinata dall'oggetto JSON.
     *   Se questa invece è stata aggiunta e quindi la proprietà id non è stata settata,
     *   Verrà utlizzato il nome proveniente dall'oggetto JSON per crearne una nuova.
     *   L'id della causale appena creata verrà impostato come l'id della nuova trattativa.
     * - La categoria segue la stessa strategia della causale
     * - Il mese viene prima letto dall'oggetto JSON e poi usando il nome viene determinato
     *   l'id corrispondente attraverso una query
     * 
     */
	public function actionCreate()
    {
        $trattativa = file_get_contents("php://input");

        if(isset($trattativa)){
            $idAgente = Yii::app()->user->idagente;
            $datiTrattativa = CJSON::decode($trattativa);
            $decTrattativa = $datiTrattativa["trattativa"];
            $decCliente = $datiTrattativa["cliente"];

            if(isset($decCliente) && isset($decTrattativa)){
                $modelAgente = $this->getModelAgente($idAgente);
                $modelTrattativa = $this->getModelTrattativa($decTrattativa, $modelAgente);
                $modelCliente = $this->isClienteEsistente($decCliente, $modelAgente->id);
                if($modelCliente != false){
                    $modelTrattativa->cliente = $modelCliente->id;
                } else {
                    $modelCliente = $this->getModelCliente($decCliente, $idAgente);
                    $modelTipoCliente = $this->getModelTipoCliente($decCliente, $modelCliente);
                    if($modelCliente->save()){
                        $modelTipoCliente->cliente = $modelCliente->id;
                        if($modelTipoCliente->save() == false){
                            header("HTTP/1.1 500 Dati relativi al cliente non validi!");
                            header("Content-Type: application/json");
                            echo CJSON::encode($modelTipoCliente->getErrors());
                            Yii::app()->end();
                        } else{
                            $modelTrattativa->cliente = $modelCliente->id;
                        }
                    }else{
                        header("HTTP/1.1 500 Dati relativi al cliente non validi!");
                        header("Content-Type: application/json");
                        echo CJSON::encode($modelCliente->getErrors());
                        Yii::app()->end();
                    }
                }
                if($modelTrattativa->save()){
                    header("HTTP/1.1 200 Salvataggio della trattativa avvenuto correttamente!");
                    header("Content-Type: application/json");
                    echo CJSON::encode($modelTrattativa);
                    Yii::app()->end();
                } else {
                    header("HTTP/1.1 500 Dati relativi alla trattativa non validi!");
                    header("Content-Type: application/json");
                    echo CJSON::encode($modelTrattativa->getErrors());
                    Yii::app()->end();
                }
            }
        }
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('create'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
}