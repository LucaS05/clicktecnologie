<?php

class PartialsController extends Controller
{
	public $layout = "//layouts/partial";

	public function actionAgenti()
	{
		$this->render("agente/_agenti");
	}

    public function actionCreateAgente()
    {
        $this->render("agente/_create");
    }

    public function actionRiepAgente()
    {
        $this->render("agente/_riepilogo");
    }

	public function actionTableDocumenti()
	{
		$this->renderPartial("_tabledocumenti");
	}

    public function actionDocumentoUpload()
    {
        $model = new Documento();
        $this->render("_documentoupload", array("model" => $model));
    }

	public function actionClienti()
	{
		$this->render("cliente/_clienti");
	}

    public function actionCreateCliente()
    {
        $this->render("cliente/_create");
    }

    public function actionUpdateCliente()
    {
        $this->render("cliente/_update");
    }

    public function actionRiepCliente()
    {
        $this->renderPartial("cliente/_riepilogo");
    }

    public function actionCliente()
    {
        $this->renderPartial("cliente/_view");
    }

    public function actionAddAgente()
    {
        $this->render("_addagente");
    }

    public function actionMessaggi()
    {
        $this->render("_messaggi");
    }

    public function actionTrattative()
    {
        $this->renderPartial("_tabletrattative");
    }

    public function actionAddTrattativa()
    {
        $numCausali = Yii::app()->db->createCommand()
            ->select("count(*) as causnum")
            ->from("tbl_causale")
            ->order("nome asc")
            ->queryAll();
        $this->render("_addtrattativa", array("numCausali" => (int)$numCausali[0]["causnum"]));
    }

    public function actionModWizard()
    {
        $this->renderPartial("_modwizard");
    }

    public function actionButtonEsistente()
    {
        $this->renderPartial("wizard/_buttonEsistente");
    }

    public function actionWizModelli()
    {
        $this->renderPartial("wizmodelli/_wizAddmodelli");
    }

    public function actionWizModelloClick()
    {
        $this->renderPartial("wizmodelli/_wizModClick");
    }

    public function actionWizModelloEsistente()
    {
        $this->renderPartial("wizmodelli/_wizModEsistenti");
    }

    public function actionWizCommittente()
    {
        $this->renderPartial("wizmodelli/_wizCommittente");
    }

    public function actionWizRiepilogo()
    {
        $this->renderPartial("wizmodelli/_wizRiepilogo");
    }

    public function actionDirectiveDropDown()
    {
        $this->renderPartial("directives/_dropdown");
    }

    public function actionRiepTrattativa()
    {
        $this->renderPartial("_rieptrattativa");
    }

    public function actionTrattativa()
    {
        $this->renderPartial("_trattativa");
    }

    public function actionModelliTrattativa(){
        $this->renderPartial("_modelliTrattativa");
    }

    public function actionRiepilogoTrattativa()
    {
        $this->renderPartial("_riepilogoTrattativa");
    }
}