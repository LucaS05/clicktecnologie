<?php

class DefaultController extends Controller
{

    public $layout='/layouts/main';

	public function actionIndex()
	{
        if(Yii::app()->user->checkAccess('admin'))
            $this->render('admin_index');
        else if(Yii::app()->user->checkAccess('agente'))
            $this->render('agente_index');
	}

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('index'),
                'users'=>array('@'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }
}