<?php

class AgenteController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'roles'=>array('admin'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionCreate(){
        $postData = CJSON::decode(file_get_contents("php://input"));
        $agente = $postData["agente"];
        if(isset($agente)){
            $modelUser = new User();
            $modelAgente = new Agente();
            $modelUser->username = $agente["username"];
            $modelUser->password = $agente["password"];
            $modelAgente->nome = $agente["nome"];
            $modelAgente->cognome = $agente["cognome"];
            $modelAgente->cellulare = $agente["telefono"];
            $modelAgente->email = $agente["email"];
            $idTipo = Yii::app()->db->createCommand()
                ->select("id")
                ->from("tbl_tipoagente")
                ->where("nome = :nome", array(":nome" => $agente["nome"]))
                ->queryRow();
            $modelAgente->tipo = (int)$idTipo["id"];
            $modelUser->stato = 1;
            if($modelUser->save()){
                Yii::app()->authManager->assign("agente", $modelUser->id);
                $modelAgente->user_id = $modelUser->id;
                if($modelAgente->save()){
                    header("HTTP/1.1 200 Agente salvato con successo!");
                    Yii::app()->end();
                    //CVarDumper::dump($modelAgente, 10, true);
                }else{
                    $modelUser->delete();
                    header("HTTP/1.1 500 Errore nel salvataggio dell'agente!");
                    header("Content-Type: application/json");
                    echo CJSON::encode($modelAgente->getErrors());
                    Yii::app()->end();
                }
            }else{
                header("HTTP/1.1 500 Errore nella creazione dell'utente associato all'agente!");
                header("Content-Type: application/json");
                echo CJSON::encode($modelUser->getErrors());
                Yii::app()->end();
            }
        }
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Agente']))
		{
			$model->attributes=$_POST['Agente'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Agente');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Agente('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Agente']))
			$model->attributes=$_GET['Agente'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Agente the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Agente::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Agente $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='agente-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
