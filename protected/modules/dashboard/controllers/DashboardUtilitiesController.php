<?php

class DashboardUtilitiesController extends Controller
{
	public function actionComuni(){
        $comune = Yii::app()->request->getParam("com");
        if(isset($comune)){
            $queryMod = Yii::app()->db->createCommand()
                ->select("c.id, c.nome as comune, p.nome as provincia")
                ->from("tbl_comuni as c, tbl_province as p")
                ->where(array('and', array('like', 'c.nome', $comune . '%'), 'c.id_provincia = p.id'))
                ->order("c.nome asc")
                ->limit("10")
                ->queryAll();
        }
        $comuni = array("comuni" => $queryMod);
        echo CJSON::encode($comuni);
    }

    public function actionProvince(){
        $provincia = Yii::app()->request->getParam("prov");
        if(isset($provincia)) {
            $queryMod = Yii::app()->db->createCommand()
                ->select("id, nome")
                ->from("tbl_province")
                ->where(array('like', 'nome', '%' . $provincia . '%'))
                ->order("nome asc")
                ->limit("10")
                ->queryAll();
        }
        echo CJSON::encode($queryMod);
    }

    public function actionCausale(){
        $causale = Yii::app()->request->getParam("c");
        if(isset($causale)){
            $queryMod = Yii::app()->db->createCommand()
                ->select("*")
                ->from("tbl_causale")
                ->where(array('like', 'nome', '%'. $causale . '%'))
                ->order("nome asc")
                ->limit("10")
                ->queryAll();
        } else {
            $queryMod = Yii::app()->db->createCommand()
                ->select("*")
                ->from("tbl_causale")
                ->order("nome asc")
                ->queryAll();
        }
        $causali = array("causali" => $queryMod);
        echo CJSON::encode($causali);
    }

    public function actionCategoria(){
        $categoria = Yii::app()->request->getParam("c");
        if(isset($categoria)){
            $queryMod = Yii::app()->db->createCommand()
                ->select("*")
                ->from("tbl_categorietratt")
                ->where(array('like', 'nome', '%'. $categoria . '%'))
                ->order("nome asc")
                ->limit("10")
                ->queryAll();
        } else {
            $queryMod = Yii::app()->db->createCommand()
                ->select("*")
                ->from("tbl_categorietratt")
                ->order("nome asc")
                ->queryAll();
        }
        $categorie = array("categorie" => $queryMod);
        echo CJSON::encode($categorie);
    }

    public function actionTipiSocieta(){
        $querySoc = Yii::app()->db->createCommand()
            ->select("*")
            ->from("tbl_tiposocieta")
            ->queryAll();
        $societa = array("societa" => $querySoc);
        echo CJSON::encode($societa);
    }

    /**
     *
     */
    public function actionValidaCliente(){
        $tipoCliente = Yii::app()->request->getParam("tipo");
        if(isset($tipoCliente)){
            $criteria = new CDbCriteria();
            $checkDato = null;
            switch($tipoCliente){
                case "societa":
                    $checkDato = Yii::app()->request->getParam("piva");
                    $criteria->addCondition("piva=:piva");
                    $criteria->params = array(':piva' => $checkDato);
                    $modelSocieta = Societa::model()->find($criteria);
                    break;
                case "privato":
                    $checkDato = Yii::app()->request->getParam("codfisc");
                    $criteria->addCondition("codfisc=:cf");
                    $criteria->params = array(':cf' => $checkDato);
                    $modelSocieta = Privato::model()->find($criteria);
                    break;
            }
            if(isset($modelSocieta)){
                $modelCliente = $modelSocieta->relCliente;
                $agente = Agente::model()->findByPk($modelCliente->agente);
                if( $agente->id != Yii::app()->user->idagente ){
                    $errData = array(
                        "errore" => "invalid",
                        "rag_soc" => $modelCliente->rag_soc,
                        "ag_nome" => $agente->nome ,
                        "ag_cognome" => $agente->cognome
                    );
                    header("HTTP/1.1 500 Cliente appartente ad un altro agente!");
                    header("Content-Type: application/json");
                    echo CJSON::encode($errData);
                    Yii::app()->end();
                } if($agente->id == Yii::app()->user->idagente){
                    header("HTTP/1.1 500 Stai reinserendo un tuo cliente!");
                    header("Content-Type: application/json");
                    $errData = array(
                        "errore" => "duplicato",
                        "rag_soc" => $modelCliente->rag_soc
                    );
                    echo CJSON::encode($errData);
                    Yii::app()->end();
                    header("HTTP/1.1 200 Stesso cliente!");
                } else {
                    header("HTTP/1.1 200 Cliente valido!");
                    Yii::app()->end();
                }
            }
        }
    }

    private function getClientiByRagSoc($ragioneSociale){
        if(isset($ragioneSociale)){
            $ragSoc = addcslashes($ragioneSociale, '%_');
            $criteria = new CDbCriteria();
            $criteria->addCondition("agente=:idagente AND stato = 1");
            $criteria->addCondition("rag_soc LIKE :rag_soc");
            $criteria->params = array("idagente" => Yii::app()->user->idagente, "rag_soc" => "%$ragSoc%");
            return Cliente::model()->findAll($criteria);
        }
    }

    private function getPrivatiByRagSoc($ragioneSociale){
        if(isset($ragioneSociale)){
            $privati = array();
            $clienti = $this->getClientiByRagSoc($ragioneSociale);
            foreach($clienti as $cliente){
                if(isset($cliente->privato)){
                    $clienteAttributes = $cliente->getAttributes();
                    $clienteAttributes["cod_fisc"] = $cliente->privato->codfisc;
                    array_push($privati, $clienteAttributes);
                }
            }
            return $privati;
        }
    }

    private function getSocietaByRagSoc($ragioneSociale){
        if(isset($ragioneSociale)){
            $societa = array();
            $clienti = $this->getClientiByRagSoc($ragioneSociale);
            foreach($clienti as $cliente){
                if(isset($cliente->societa)){
                    $clienteAttributes = $cliente->getAttributes();
                    $clienteAttributes["piva"] = $cliente->societa->piva;
                    $nomeSocieta = $cliente->societa->getNomeSocieta();
                    $tipoSocieta = $cliente->societa->getTipoSocieta();
                    $clienteAttributes["rag_soc"] = $nomeSocieta . " " . $tipoSocieta;
                    array_push($societa, $clienteAttributes);
                }
            }
            return $societa;
        }
    }

    private function getClienti($tipoCliente){
        $ragioneSociale = Yii::app()->request->getParam("rag_soc");
        if(isset($ragioneSociale)){
            $clienti = null;
            if($tipoCliente === "privato"){
                $clienti = $this->getPrivatiByRagSoc($ragioneSociale);
            }else if($tipoCliente === "societa"){
                $clienti = $this->getSocietaByRagSoc($ragioneSociale);
            }
            return array("clienti" => $clienti);
        }else{
            header("HTTP/1.1 400 Ragione sociale non specificata! Parametro rag_soc mancante.");
            Yii::app()->end();
        }
    }

    public function actionValidaAgente(){
        $username = Yii::app()->request->getParam("username");
        if(isset($username)){
            $criteria = new CDbCriteria();
            $criteria->addCondition("username=:username");
            $criteria->params = array(':username' => $username);
            $modelUtente = User::model()->find($criteria);
            if(isset($modelUtente)){
                header("HTTP/1.1 500 Esiste un gia' un agente con questo username!");
                Yii::app()->end();
            }else{
                header("HTTP/1.1 200 Agente valido!");
                Yii::app()->end();
            }
        }
    }

    public function actionSocieta(){
        header("Content-Type: application/json");
        echo CJSON::encode($this->getClienti("societa"));
    }

    public function actionPrivato(){
        header("Content-Type: application/json");
        echo CJSON::encode($this->getClienti("privato"));
    }

    public function filters(){
        return array(
            'accessControl',
        );
    }

    public function accessRules(){
        return array(
            array('allow',
                'actions' => array('validaCliente', 'tipiSocieta', 'categoria', 'causale', 'province',
                    'comuni', 'societa', 'privato'),
                'roles' => array('agente'),
            ),
            array('allow',
                'actions' => array('validaAgente'),
                'roles' => array('admin'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }
}