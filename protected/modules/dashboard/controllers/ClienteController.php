<?php

class ClienteController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

    private function getIDCitta($nomeCitta){
        $idComune = null;
        $criteria = new CDbCriteria();
        $criteria->addCondition("nome=:nome");
        $criteria->params = array(':nome' => $nomeCitta);
        $comuneModel = Comuni::model()->find($criteria);
        if(isset($comuneModel)){
            $idComune = $comuneModel->id;
        }
        return $idComune;
    }

    private function isPrivato($clienteData){
        if(isset($clienteData)){
            return $clienteData["privato"] === true;
        }
    }

    private function isSocieta($clienteData){
        if(isset($clienteData)){
            return $clienteData["societa"] === true;
        }
    }

    private function createModelCliente($clienteData){
        $modelCliente = null;
        if(isset($clienteData)){
            $modelCliente = new Cliente();
            $modelCliente->rag_soc = $clienteData["rag_soc"];
            if($this->isSocieta($clienteData)){
                $modelCliente->rag_soc = $modelCliente->rag_soc . "#" . $clienteData["tipoSocieta"];
            }
            $modelCliente->agente = Yii::app()->user->idagente;
            $primaVisita = new DateTime($clienteData["prima_visita"]);
            $modelCliente->prima_visita = $primaVisita->format("Y-m-d");
            $referenteCliente = $clienteData["referente"];
            $modelCliente->referente = $referenteCliente["nome"];
            $modelCliente->email_referente = $referenteCliente["email"];
            $modelCliente->telefono_referente = $referenteCliente["telefono"];
            $modelCliente->citta = $this->getIDCitta($clienteData["citta"]);
        }
        return $modelCliente;
    }

    private function getModelCliente($clienteData){
        $modelCliente = null;
        if(isset($clienteData)){
            if($this->isSocieta($clienteData)){
                $ragSocCliente = $clienteData["orag_soc"] . "#" . $clienteData["otipoSocieta"];
            }else if($this->isPrivato($clienteData)){
                $ragSocCliente = $clienteData["orag_soc"];
            }
            $modelCliente = Cliente::model()->find("rag_soc=:ragsoc",
                array(":ragsoc" => $ragSocCliente));
        }
        return $modelCliente;
    }

    private function createModelTipoCliente($clienteData){
        $modelTipoCliente = null;
        if(isset($clienteData)){
            if ($this->isPrivato($clienteData)) {
                $modelTipoCliente = new Privato();
                $modelTipoCliente->codfisc = $clienteData["cod_fisc"];
            } else if ($this->isSocieta($clienteData)) {
                $modelTipoCliente = new Societa();
                $modelTipoCliente->piva = $clienteData["piva"];
            }
        }
        return $modelTipoCliente;
    }

    private function updateModelTipoCliente($modelCliente, $clienteData){
        $modelTipoCliente = null;
        if(isset($clienteData)){
            if($this->isPrivato($clienteData)){
                $modelCliente->privato->codfisc = $clienteData["cod_fisc"];
                $modelTipoCliente = $modelCliente->privato;
            } else if($this->isSocieta($clienteData)){
                $modelCliente->societa->piva = $clienteData["piva"];
                $modelTipoCliente = $modelCliente->societa;
            }
        }
        return $modelTipoCliente;
    }

	public function actionCreate(){
        $postData = CJSON::decode(file_get_contents("php://input"));
        if(isset($postData["cliente"])) {
            $clienteData = $postData["cliente"];
            $modelCliente = $this->createModelCliente($clienteData);
            $modelTipoCliente = null;
            if($modelCliente->save()){
                $modelTipoCliente = $this->createModelTipoCliente($clienteData);
                $modelTipoCliente->cliente = $modelCliente->id;
                if($modelTipoCliente->save()){
                    header("HTTP/1.1 200 Cliente salvato con successo!");
                    Yii::app()->end();
                }else{
                    $modelCliente->delete();
                    header("HTTP/1.1 500 Errore nel salvataggio del cliente!");
                    header("Content-Type: application/json");
                    echo CJSON::encode($modelTipoCliente->getErrors());
                    Yii::app()->end();
                }
            } else {
                header("HTTP/1.1 500 Errore nella creazione del cliente!");
                header("Content-Type: application/json");
                echo CJSON::encode($modelCliente->getErrors());
                Yii::app()->end();
            }
        } else {
            header("HTTP/1.1 500 Richiesta errata. Dati del cliente mancanti!");
            Yii::app()->end();
        }
	}

	public function actionUpdate(){
        $postData = CJSON::decode(file_get_contents("php://input"));
        if(isset($postData["cliente"])){
            $clienteData = $postData["cliente"];
            $tempModel = $this->createModelCliente($clienteData);
            $modelCliente = $this->getModelCliente($clienteData);
            if(isset($modelCliente)){
                $modelCliente->setAttributes($tempModel->getAttributes());
                if($modelCliente->validate()){
                    $modelTipoCliente = $this->updateModelTipoCliente($modelCliente, $clienteData);
                    if($modelTipoCliente->validate()){
                        $modelTipoCliente->update();
                        $modelCliente->update();
                        header("HTTP/1.1 200 Cliente modificato con successo!");
                        Yii::app()->end();
                    } else {
                        Yii::log("Errore. Dati del cliente errati!", CLogger::LEVEL_ERROR);
                        echo CJSON::encode($modelTipoCliente->getErrors());
                        Yii::app()->end();
                    }
                } else {
                    Yii::log("Errore. Dati del cliente errati!", CLogger::LEVEL_ERROR);
                    echo CJSON::encode($modelCliente->getErrors());
                    Yii::app()->end();
                }

            } else {
                throw new CHttpException(404, "Cliente originario non trovato!");
            }
        } else {
            throw new CHttpException(500, "Richiesta errata. Dati del cliente mancanti!");
        }
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Cliente');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Cliente('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Cliente']))
			$model->attributes=$_GET['Cliente'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Cliente the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Cliente::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Cliente $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='cliente-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
