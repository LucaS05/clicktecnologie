<?php 
	$cs = Yii::app()->clientScript;
	$cs->registerScriptFile($this->module->assetsUrl . '/js/agenti/agenti.js', CClientScript::POS_END);
    $cs->registerScriptFile($this->module->assetsUrl . '/js/agenti/controllers.js', CClientScript::POS_END);
    $cs->registerScriptFile($this->module->assetsUrl . '/js/agenti/clcostants.js', CClientScript::POS_END);
    $cs->registerScriptFile($this->module->assetsUrl . '/js/ui-bootstrap-tpls.min.js', CClientScript::POS_END);
?>
<div id="sidebar">
	<ul class="list-unstyled" id="mainmenu">
        <!--
        <li class="menu-spedizioni">
            <?php
                $serUrl = $this->createUrl("#/documenti") . "/";
                $linkHtml = '<i class="fa fa-file-text-o"></i>Documenti';
                echo CHtml::link($linkHtml, $serUrl, array("is-active-nav" => "#/"));
            ?>
        </li>
        <li class="menu-spedizioni">
            <?php
                $serUrl = $this->createUrl("#/messaggi") . "/";
                $linkHtml = '<i class="fa fa-envelope-o"></i>Messaggi';
                echo CHtml::link($linkHtml, $serUrl, array("is-active-nav" => "#/"));
            ?>
        </li>-->
        <li class="menu-spedizioni">
            <a ui-sref="clienti" ui-sref-opts="{ reload: true }" ui-sref-active="active">Clienti</a>
        </li>
        <li class="menu-spedizioni">
            <a ui-sref="trattative" ui-sref-opts="{ reload: true }" ui-sref-active="active">Trattative</a>
        </li>
        <li class="menu-spedizioni" id="logout">
            <a href="<?php echo Yii::app()->createAbsoluteUrl('site/logout'); ?>">Logout</a>
        </li>
	</ul>
	<div id="btm-toolbar">

	</div>
</div>
<div ui-view id="main-dashboard"></div>