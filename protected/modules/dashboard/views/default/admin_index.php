<?php 
	$cs = Yii::app()->clientScript;
	$cs->registerScriptFile($this->module->assetsUrl . '/js/main.js', CClientScript::POS_END);
    $cs->registerScriptFile($this->module->assetsUrl . '/js/controllers.js', CClientScript::POS_END);
    $cs->registerScriptFile($this->module->assetsUrl . '/js/agente.service.js', CClientScript::POS_END);
?>
<div id="sidebar">
    <ul class="list-unstyled" id="mainmenu">
        <li class="menu-spedizioni">
            <a ui-sref="agenti" ui-sref-opts="{ reload: true }" ui-sref-active="active">Agenti</a>
        </li>
        <li class="menu-spedizioni" id="logout">
            <a href="<?php echo Yii::app()->createAbsoluteUrl('site/logout'); ?>">Logout</a>
        </li>
    </ul>
    <div id="btm-toolbar">

    </div>
</div>
<div ui-view id="main-dashboard"></div>