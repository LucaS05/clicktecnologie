<div class="container-fluid">
    <div class="row">
        <div id="header-trattativa" class="col-md-12">
            <div id="nometrattativa">
                Trattativa <span id="codtrattativa">#{{codTrattativa}}</span>
            </div>
            <ul class="list-inline clmenu clmenu-trattativa">
                <li class="menu-item" ui-sref=".riepilogo" ui-sref-active="active">
                    Riepilogo
                </li>
                <div ng-if="false">
                    <li class="menu-item" ui-sref=".modelli" ui-sref-active="active">
                        Modelli
                    </li>
                    <li class="menu-item" ui-sref=".businessplan" ui-sref-active="active">
                        Business Plan
                    </li>
                    <li class="menu-item" ui-sref=".scheda" ui-sref-active="active">
                        Scheda Cliente
                    </li>
                </div>
            </ul>
        </div>
    </div>
    <div id="trattativa-main" class="row">
        <div id="trattativa-wrapper" ui-view></div>
    </div>
</div>