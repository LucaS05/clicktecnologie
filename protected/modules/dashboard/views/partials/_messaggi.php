<?php
	$cs = Yii::app()->clientScript;
    $cs->registerCssFile($this->module->assetsUrl . '/css/messaggi.css');
?>
<div class="container-fluid">
    <div class="row">
        <div ng-if="messaggi.length == 0" id="empty-filter">Nessun messaggio presente</div>
        <div ng-if="messaggi.length != 0">
            <div id="msg-list" class="col-md-3">
                <div ng-repeat="msg in messaggi" ng-click="getMessaggio(msg);" class="msg-item">
                    <div class="msg-item-wrapper">
                        <div>
                            <p style="display: inline-block;"><strong>{{msg.mittente.nome}} {{msg.mittente.cognome}}</strong></p>
                            <div class="msg-item-data">
                                <p>{{msg.data}}</p>
                            </div>
                        </div>
                        <p>{{msg.contenuto}}</p>
                    </div>
                </div>
            </div>
            <div id="messaggio" class="col-md-9">
                <p>{{messaggio.mittente}}</p>
                <p>{{messaggio.data}}</p>
                <p>{{messaggio.contenuto}}</p>
            </div>
        </div>
    </div>
</div>