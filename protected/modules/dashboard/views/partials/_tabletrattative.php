<div class="container-fluid" ui-view>
    <div class="row">
        <div id="toolbar" class="col-md-12">
            <div class="col-md-10"><h1 class="toolbar__title">Trattative</h1></div>
            <div class="col-md-2">
                <a ui-sref=".nuova" class="btn cl-addbtn">
                    <i class="fa fa-plus"></i> Nuova Trattativa
                </a>
            </div>
        </div>
    </div>
    <div class="row tablewrap">
        <div ng-if="trattative.length == 0" id="empty-filter">Nessuna trattativa presente</div>
        <table ng-if="trattative.length != 0" id="click-table" class="cltable-trattative table table-striped table-hover">
            <thead>
                <tr>
                    <th>Codice</th>
                    <th>Cliente</th>
                    <th>Valore Offerta</th>
                    <th>Data</th>
                    <th>Note</th>
                </tr>
            </thead>
            <tbody>
                <tr class="cltable__row"
                    ng-repeat="trt in trattative"
                    ui-sref="trattativa.riepilogo({codTrattativa: trt.codice})">
                    <td class="cltable__cell">{{trt.codice}}</td>
                    <td class="cltable__cell">{{trt.cliente}}</td>
                    <td class="cltable__cell">€ {{trt.importo}}</td>
                    <td class="cltable__cell">{{trt.data}}</td>
                    <td class="cltable__cell">
                        {{trt.note | limitTo : 50}}{{trt.note.length > 50 ? '...' : ''}}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>