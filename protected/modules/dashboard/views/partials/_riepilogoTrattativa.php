<h2 id="trattativa-title">Riepilogo</h2>
<h4 id="trattativa-subtitle">Hai creato questa trattativa il {{trattativa.data}}</h4>
<div class="row">
    <div class="col-md-8">
        <div class="infotrattativa col-md-12">
            <div class="infotitle row">
                <h4 class="infotitle__main col-md-9">Anagrafica Cliente</h4>
                <div class="infotitle__btnedit col-md-3">
<!--                    <a ui-sref=".nuovo" class="btn btn-block cl-addbtn">
                        <i class="fa fa-plus"></i> Modifica
                    </a>-->
                </div>
            </div>
            <div class="infotrattativa__main">
                <div class="row">
                    <div class="col-md-4">
                        <h4 class="infotrattativa__cat">Ragione Sociale</h4>
                        <p class="infotrattativa__dato">{{trattativa.cliente.rag_soc}}</p>
                    </div>
                    <div class="col-md-5">
                        <h4 class="infotrattativa__cat">Citta</h4>
                        <p class="infotrattativa__dato">{{trattativa.cliente.citta}}</p>
                    </div>
                    <div class="col-md-3">
                        <h4 class="infotrattativa__cat">Prima Visita</h4>
                        <p class="infotrattativa__dato">{{trattativa.cliente.prima_visita}}</p>
                    </div>
                </div>
                <div class="row">
                    <h4 class="infotrattativa__subtitle col-md-12">Referente</h4>
                    <div class="col-md-4">
                        <h4 class="infotrattativa__cat">Nome e Cognome</h4>
                        <p class="infotrattativa__dato">{{trattativa.cliente.referente}}</p>
                    </div>
                    <div class="col-md-5">
                        <h4 class="infotrattativa__cat">Email</h4>
                        <p class="infotrattativa__dato">{{trattativa.cliente.email_referente}}</p>
                    </div>
                    <div class="col-md-3">
                        <h4 class="infotrattativa__cat">Telefono</h4>
                        <p class="infotrattativa__dato">{{trattativa.cliente.telefono_referente}}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="infotrattativa col-md-12">
            <div class="infotitle row">
                <h4 class="infotitle__main col-md-9">Informazioni Trattativa</h4>
                <div class="infotitle__btnedit col-md-3">
<!--                    <a ui-sref=".nuovo" class="btn btn-block cl-addbtn">
                        <i class="fa fa-plus"></i> Modifica
                    </a>-->
                </div>
            </div>
            <div class="infotrattativa__main">
                <div class="row">
                    <div class="col-md-4">
                        <h4 class="infotrattativa__cat">Mese di Acquisizione</h4>
                        <p class="infotrattativa__dato">{{trattativa.mese_acq}}</p>
                    </div>
                    <div class="col-md-5">
                        <h4 class="infotrattativa__cat">Valore Offerta</h4>
                        <p class="infotrattativa__dato">{{trattativa.importo}}</p>
                    </div>
                    <div class="col-md-3">
                        <h4 class="infotrattativa__cat">Validità Offerta</h4>
                        <p class="infotrattativa__dato">{{trattativa.gg_scadenza}} giorni</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h4 class="infotrattativa__cat">Causale</h4>
                        <p class="infotrattativa__dato">{{trattativa.causale}}</p>
                    </div>
                    <div class="col-md-5">
                        <h4 class="infotrattativa__cat">Categoria</h4>
                        <p class="infotrattativa__dato">{{trattativa.categoria}}</p>
                    </div>
                    <div class="col-md-3">
                        <h4 class="infotrattativa__cat">Probabilità</h4>
                        <p class="infotrattativa__dato">{{trattativa.prob}}</p>
                    </div>
                </div>
                <div class="row" ng-if="trattativa.note">
                    <div class="col-md-12">
                        <h4 class="infotrattativa__cat">Note</h4>
                        <p class="infotrattativa__dato">{{trattativa.note}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4"></div>
</div>