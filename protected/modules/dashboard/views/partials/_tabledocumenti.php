<div id="maincontroller">
	<div id="toolbar">
        <div class="container-fluid">
            <div class="row">
                <a href="#/documenti/carica" class="btn btn-default" id="creaspedizione">
                    <i class="fa fa-upload"></i> Carica Documento
                </a>
                <div class="col-lg-3 col-md-12">
                    <div id="date-filter">
                        <div id="main-filter">
                            <i class="fa fa-calendar-o"></i>
                            <span>
                                <span id="date-range">
                                    <span id="start-date">
                                        <?php 
                                            echo date("j m Y", strtotime('-30 day')); 
                                        ?>
                                    </span>
                                    <i class="fa fa-arrow-right"></i>
                                    <span id="end-date">
                                        <?php 
                                            echo date("j m Y"); 
                                        ?>
                                    </span>
                                </span>
                                <span id="date-range-msg">Seleziona una data</span>
                            </span>
                            <b class="caret"></b>
                        </div>
                        <div id="btn-reset"><i class="fa fa-times"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div ng-if="documenti.length == 0" id="empty-filter">Nessun documento presente</div>
	<table ng-if="documenti.length != 0" id="transita-table" class="table table-striped table-hover header-fixed">
		<thead>
			<tr>
                <th>Nome Documento</th>
				<th>Data</th>
				<th>Autore</th>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="doc in documenti">
                <td>{{doc.path}}</td>
				<td>{{doc.data}}</td>
				<td>{{doc.autore}}</td>
			</tr>
		</tbody>
	</table>
	<script type="text/javascript">
		$(document).ready(function() {

			var btnResetDateFilter = $("#btn-reset");
			btnResetDateFilter.click(function(){
				btnResetDateFilter.hide();
				$("span#date-range-msg").show();
				$("#date-range").hide();
				angular.element('#maincontroller').scope().resetShip();
			});

			$('#date-filter').daterangepicker(
				{ startDate: moment().subtract('days', 29),
					endDate: moment(),
					format: 'DD/MM/YYYY',
					locale: {
						applyLabel: 'Applica',
                		cancelLabel: 'Annulla',
                		fromLabel: 'Dal',
                		toLabel: 'Al',
                		monthNames: ["Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic"],
                		daysOfWeek: ["Do", "Lu", "Ma", "Me", "Gi", "Ve", "Sa"]
					}
				}, function(start, end) {
						btnResetDateFilter.show();
						$("span#date-range-msg").hide();
						$("span#date-range").css("display", "inline-block");
						$("span#start-date").text(moment(start).format("DD/MM/YYYY"));
						$("span#end-date").text(moment(end).format("DD/MM/YYYY"));
						angular.element('#maincontroller').scope().queryDate(start, end);
			});
		});
	</script>
</div>