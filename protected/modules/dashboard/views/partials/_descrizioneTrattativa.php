<div ng-if="cliente.rag_soc">
    <p id="tratt-desc">Stai inserendo una trattativa per
        <span ng-show="!cliente.rag_soc">ragione sociale</span>
        <span>{{cliente.rag_soc}}<span ng-if="cliente.societa">{{cliente.tipoSocieta}}</span></span>.
        Il cliente è di <span ng-show="!cliente.citta">città</span> <span>{{cliente.citta}}</span>.
    </p>
</div>