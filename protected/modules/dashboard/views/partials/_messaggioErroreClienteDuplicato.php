<p>
    Il cliente <strong>{{ragSocErr}} <span ng-if="cliente.tipoSocieta">{{societa.tipo}}</strong>
    <span ng-if="cliente.societa">con partita iva <strong>{{cliente.piva}}</strong></span>
    <span ng-if="cliente.privato">con codice fiscale <strong>{{cliente.cod_fisc}}</strong></span>
    è già un tuo cliente!
</p>