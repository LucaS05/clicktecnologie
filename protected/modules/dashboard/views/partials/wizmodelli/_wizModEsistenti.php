<h3 class="wizard-title">Modello esistente</h3>
<h4 class="wizard-title-desc">Scegli la tipologia, quantità e wattaggio dei modelli esistenti</h4>
<div class="row">
    <div id="selezione-esistenti">
        <div ng-repeat = "mod in modesistenti"
             ng-click  = "modSelected(mod, $index)"
             ng-class  = "{'active' : buttonSel.btnIndex == $index, 'th-mod' : ($index % 3) == 0}"
             class="btn btn-exmod">
                {{mod.nome}}
            <i class="fa fa-check sel-badge"></i>
        </div>
    </div>
    <div id="watt-qta" class="clearfix">
        <div class="col-md-4">
            <span class="counter-label">Quantità</span>
            <div counter value="quantita" editable class="wizard-counter"></div>
        </div>
        <div class="col-md-4">
            <span class="counter-label">Watt</span>
            <div counter value="watt" editable step="10" class="wizard-counter"></div>
        </div>
    </div>
    <div id="wizard-controls" class="clearfix">
        <div class="col-md-3">
            <a  ui-sref="trattativa.wizard.committente"
                id="wizard-prev"
                class="btn btn-block">Indietro</a>
        </div>
        <div class="col-md-3 pull-right">
            <a ui-sref="trattativa.wizard.clickmodel"
               id="wizard-next"
               ng-click="addExModel()"
               class="btn btn-success btn-block">Avanti: Modello Click</a>
        </div>
    </div>
</div>