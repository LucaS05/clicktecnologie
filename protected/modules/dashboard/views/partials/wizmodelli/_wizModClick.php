<h3 class="wizard-title">Modello Click</h3>
<h4 class="wizard-title-desc">Scegli il tipo di plafoniera, il modello click e la relativa quantità</h4>
<div id="lista-plafoniere" ng-hide="plafselected">
    <div ng-repeat="tipoplaf in tipoplafoniere"
         ng-click  = "tipoPlafSelected(tipoplaf)"
         ng-class  = "{'active' : buttonSel.btnIndex == $index}"
         class="btn btn-exmod">
        {{tipoplaf.nome}}
        <i class="fa fa-check sel-badge"></i>
    </div>
</div>
<div id="lista-modelli" ng-show="plafselected">
    <div class="row">
        <div id="filters" class="col-md-12">
            <div class="form-horizontal">
                <div id="change-filters" class="clearfix">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="select-watt" class="control-label col-md-2 filter-label">Tipo Plafoniera</label>
                            <div class="btncambiotipo tipoplafoniera col-md-10">
                                <div class="tipo col-md-8">{{tipoPlafoniera.nome}}</div>
                                <div class="btn-cambio col-md-4" ng-click="cambioPlafoniera()">Cambia Plafoniera</div>
                            </div>
                        </div>
                    </div>
                    <div ng-hide="!clickselected">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="select-watt" class="control-label col-md-2 filter-label">Modello Click</label>
                                <div class="btncambiotipo tipomodello col-md-10">
                                    <div class="tipo col-md-8">{{modClickSelected.nome}}</div>
                                    <div class="btn-cambio col-md-4" ng-click="cambioModello()">Cambia Modello</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                    <div ng-if="!clickselected">
                        <div class="col-md-12">
                                <div class="form-group">
                                    <label for="select-watt" class="control-label col-md-2 filter-label">Watt Lampada</label>
                                    <div class="select col-md-10">
                                        <select id="select-watt"
                                                ng-model="selectedWatt"
                                                ng-options="wattItem.watt as wattItem.label for wattItem in wattFilter"
                                                ng-change="wattChanged(selectedWatt)">
                                            <option value="">Seleziona Watt</option>
                                        </select>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div ng-hide="clickselected" class="col-md-12">
            <div ng-if="modelliClick.length == 0">
                <div ng-if="exModStatus && wattSostituzione" class="clmsg clmsg-nomodelli" id="nomodelli-infoinduzione">
                    Stai sostituendo <strong>{{modelloEsistente.nome}}</strong> da <strong>{{wattModEsistente}} Watt</strong>.<br />
                    Ti consigliamo di sostituire queste lampade con modelli Click da
                    <span class="watt-induzione" ng-repeat="watt in wattSostituzione">
                        <strong>{{watt}} Watt</strong>{{$last ? '' : (wattSostituzione.length == 2) ? ' o da ' : ', '}}
                    </span>.<br />
                    Al momento non abbiamo a disposizione modelli Click da
                    <span class="watt-induzione" ng-repeat="watt in wattSostituzione">
                        {{watt}} Watt{{$last ? '' : (wattSostituzione.length == 2) ? ' e da ' : ', '}}
                    </span>con plafoniera {{tipoPlafoniera.nome}}.<br />
                    Ecco alcuni suggerimenti:
                    <ul>
                        <li>Scegli i modelli selezionando il wattaggio.</li>
                        <li>Cambia plafoniera.</li>
                    </ul>
                </div>
                <div ng-if="exModStatus && !wattSostituzione" class="clmsg clmsg-nomodelli" id="nomodelli-nosostituzione">
                    Stai sostituendo {{modelloEsistente.nome}} da {{wattModEsistente}} Watt.
                    Al momento non possiamo consigliarti modelli Click che possano sostituire queste lampade.
                    Ti consigliamo di scegliere il modello Click con plafoniera {{tipoPlafoniera.nome}}, selezionando il wattaggio che ritieni
                    più appropiato.
                </div>
                <!--
                    <div ng-if="!exModStatus && !wattSostituzione">
                        Prova
                    </div>
                -->
            </div>
            <div ng-if="modelliClick.length != 0">
                <div ng-repeat="modello in modelliClick track by $index" class="clearfix rigamodelli">
                    <div class="modello col-md-4" ng-if="modello[0]">
                        <div class="modello-info">
                            <p class="nome">{{modello[0].nome_induzione}}</p>
                            <p class="codice">{{modello[0].codice}}</p>
                                <span class="prezzo-watt">
                                    <p class="prezzo">€ {{modello[0].prezzo}}</p>
                                    <p>Watt <span class="watt">{{modello[0].watt_induzione}}</span></p>
                                </span>
                        </div>
                        <div ng-click="clickModelSelected(modello[0])"
                             class="btn-block btn-selezmodello">Seleziona Modello</div>
                    </div>
                    <div class="modello col-md-4" ng-if="modello[1]">
                        <div class="modello-info">
                            <p class="nome">{{modello[1].nome_induzione}}</p>
                            <p class="codice">{{modello[1].codice}}</p>
                                <span class="prezzo-watt">
                                    <p class="prezzo">€ {{modello[1].prezzo}}</p>
                                    <p>Watt <span class="watt">{{modello[1].watt_induzione}}</span></p>
                                </span>
                        </div>
                        <div ng-click="clickModelSelected(modello[1])"
                             class="btn-block btn-selezmodello">Seleziona Modello</div>
                    </div>
                    <div class="modello col-md-4" ng-if="modello[2]">
                        <div class="modello-info">
                            <p class="nome">{{modello[2].nome_induzione}}</p>
                            <p class="codice">{{modello[2].codice}}</p>
                                <span class="prezzo-watt">
                                    <p class="prezzo">€ {{modello[2].prezzo}}</p>
                                    <p>Watt <span class="watt">{{modello[2].watt_induzione}}</span></p>
                                </span>
                        </div>
                        <div ng-click="clickModelSelected(modello[2])"
                             class="btn-block btn-selezmodello">Seleziona Modello</div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modello-selezionato" ng-show="clickselected" class="col-md-12">
            <div class="col-md-6">
                {{modClickSelected.foto}}
            </div>
            <div class="col-md-6">
                <div class="modello-info">
                    <div class="nome">{{modClickSelected.nome}}</div>
                    <div class="codice">{{modClickSelected.codice}}</div>
                    <div class="watt">
                        <span>Watt {{modClickSelected.watt_nom}}</span>
                        <span class="wattreali">Watt Reali {{modClickSelected.watt_reali}}</span>
                    </div>
                    <div class="prezzo">€ {{modClickSelected.prezzo}}</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="wizard-controls" class="clearfix clickstep-controls">
    <div class="col-md-3" ng-if="!plafselected" ng-show="clickselected">
        <a ng-if="modExStatus" id="wizard-prev" class="btn btn-block" ui-sref="trattativa.wizard.exmodel">Indietro</a>
        <a ng-if="!modExStatus" id="wizard-prev" class="btn btn-block" ui-sref="trattativa.wizard.committente">Indietro</a>
    </div>
    <div class="col-md-4 pull-right" ng-if="clickselected">
        <a id="wizard-next"
           class="btn btn-block"
           ng-click="addClickModel()"
           ui-sref="trattativa.wizard.riepilogo">Conferma modello. Avanti: Riepilogo</a>
    </div>
</div>