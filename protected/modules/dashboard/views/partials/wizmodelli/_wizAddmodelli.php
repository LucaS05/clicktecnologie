<?php
    $cs = Yii::app()->clientScript;
    $cs->registerCssFile($this->module->assetsUrl . '/css/wizard.css');
?>
<div id="wizard-modelli" class="row">
    <div id="main-wizard" class="col-md-12">
        <div class="row">
            <div id="wizard-buttons" class="col-md-3">
                <p id="wizard-buttons-title">Aggiungi <br> un modello alla trattativa</p>
                <p id="wizard-buttons-desc">
                    Descrivi l'installazione da effettuare presso il committente
                </p>
                <a ui-sref=".committente" ui-sref-active="active" class="wizard-button">
                    <span class="step-complete"><i class="fa fa-check"></i></span>
                    <span class="step-number">1</span>
                    <span class="step-title">Info Committente</span>
                </a>
                <a ui-sref=".exmodel" ui-sref-active="active" class="wizard-button">
                    <span class="step-complete"><i class="fa fa-check"></i></span>
                    <span class="step-number">2</span>
                    <span class="step-title">Modello Esistente</span>
                </a>
                <a ui-sref=".clickmodel" ui-sref-active="active" class="wizard-button">
                    <span class="step-complete"><i class="fa fa-check"></i></span>
                    <span class="step-number">3</span>
                    <span class="step-title">Modello Click</span>
                </a>
                <a ui-sref=".riepilogo" ui-sref-active="active" class="wizard-button">
                    <span class="step-complete"><i class="fa fa-check"></i></span>
                    <span class="step-number">4</span>
                    <span class="step-title">Riepilogo</span>
                </a>
            </div>
            <div id="wizard-content" class="col-md-9" ui-view></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var wizard = $("#main-wizard");
    var wizHeight = $(window).height() - 50;
    wizard.css("min-height", wizHeight);
    $(window).on("resize", function(){
        wizard.css("min-height", $(window).height() - 50);
    });
</script>