<h3 class="wizard-title">Info Cliente</h3>
<h4 class="wizard-title-desc">Il modello sostituirà delle lampade già presenti presso il committente?</h4>
<div id="risp-esistente">
    <div class="row">
        <div class="col-md-6" ng-click="rispModEx(true)">
            <div class="btn-exrisp" ng-class="{active : exStatus == true}">
                <p class="risp">Si</p>
                <span>Sono presenti lampade da sostituire</span>
            </div>
        </div>
        <div class="col-md-6" ng-click="rispModEx(false)">
            <div class="btn-exrisp" ng-class="{active : exStatus == false}">
                <p class="risp">No</p>
                <span>Il modello click è una nuova installazione</span>
            </div>
        </div>
    </div>
</div>
<div id="wizard-controls" class="clearfix">
    <a ng-click="chiudiWizard()"
       class="btn" id="wizard-prev">Esci dal Wizard</a>
</div>