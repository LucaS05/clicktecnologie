<h3 class="wizard-title">Riepilogo</h3>
<h4 class="wizard-title-desc">Il modello da te scelto. Specifica la quantià di modelli Click</h4>
<div class="row">
    <div id="wizard-controls" class="clearfix">
        <div class="col-md-3 pull-left">
            <a id="wizard-prev" class="btn btn-block" ui-sref="trattativa.wizard.clickmodel">Indietro</a>
        </div>
        <div class="col-md-3 pull-right">
            <a
                id="wizard-addmodel"
                class="btn btn-block btn-addmodel"
                ng-click="addTrattModel()">Aggiungi alla trattativa</a>
        </div>
    </div>
</div>