<div class="container-fluid" ui-view>
    <div class="row">
        <div id="toolbar" class="col-md-12">
            <div class="col-md-12">
                <a ui-sref=".nuovo" class="btn btn-default">
                    <i class="fa fa-plus"></i> Nuovo Agente
                </a>
            </div>
        </div>
    </div>
    <div class="row tablewrap">
        <div ng-if="agenti.length == 0" id="empty-filter">Nessun Agente presente</div>
        <table ng-if="agenti.length != 0" class="table table-striped">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Cognome</th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="agt in agenti">
                    <td data-th="Nome">{{agt.nome}}</td>
                    <td data-th="Cognome">{{agt.cognome}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>