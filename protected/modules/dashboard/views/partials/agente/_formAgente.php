<div class="row" id="addcliente">
    <div class="col-md-7">
        <form name="agenteForm" ng-submit="addAgente(agenteForm)" class="clform" novalidate>
            <div class="row form-sec">
                <div class="form-sec-title">
                    <p>Anagrafica del nuovo agente</p>
                    <p class="placeholder">Chi è l'agente da inserire? Inserisci qui i suoi dati</p>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="nomeText" class="clform__label">Nome</label>
                        <input name="nome"
                               ng-model="agente.nome"
                               type="text"
                               class="form-control clform__input"
                               id="nomeText"
                               required />
                        <div ng-show="!agenteForm.nome.$pristine">
                            <div ng-show="agenteForm.nome.$error.required && agenteForm.nome.$touched"
                                 class="clform__errorlabel">Il nome dell'agente è obbligatorio</div>
                        </div>
                        <p class="placeholder">Inserisci il nome dell'agente</p>
                    </div>
                    <div class="col-md-6">
                        <label for="cognomeText" class="clform__label">Cognome</label>
                        <input name="cognome"
                               ng-model="agente.cognome"
                               type="text"
                               class="form-control clform__input"
                               id="cognomeText"
                               required />
                        <div ng-show="!agenteForm.referente.$pristine">
                            <div ng-show="agenteForm.cognome.$error.required && agenteForm.cognome.$touched"
                                 class="clform__errorlabel">Il cognome dell'agente è obbligatorio</div>
                        </div>
                        <p class="placeholder">Inserisci il cognome dell'agente</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="emailText" class="clform__label">Email</label>
                        <input name="email"
                               ng-model="agente.email"
                               type="email"
                               class="form-control clform__input"
                               id="emailText"
                               required />
                        <div ng-show="!agenteForm.email.$pristine">
                            <p ng-show="agenteForm.email.$error.required && agenteForm.email.$touched"
                               class="clform__errorlabel">L'email dell'agente è obbligatoria</p>
                            <div ng-show="agenteForm.email.$touched">
                                <div class="clform__errorlabel" ng-show="agenteForm.email.$error.email">
                                    Formato dell'email non corretto</div>
                            </div>
                        </div>
                        <p class="placeholder">Inserisci l'indirizzo mail dell'agente</p>
                    </div>
                    <div class="col-md-6">
                        <label for="telefonoText" class="clform__label">Telefono</label>
                        <input name="telefono"
                               ng-model="agente.telefono"
                               type="tel"
                               class="form-control clform__input"
                               id="telefonoText"
                               required />
                        <div ng-show="!agenteForm.telefono.$pristine">
                            <div ng-show="agenteForm.telefono.$error.required && agenteForm.telefono.$touched"
                                 class="clform__errorlabel">Il numero di telefono dell'agente è obbligatorio</div>
                        </div>
                        <p class="placeholder">Inserisci il numero di telefono dell'agente</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="clform__label">Tipologia</label>
                    <div class="cl-select-wrap">
                        <select name="selectTipo"
                                class="cl-select"
                                ng-model="selectedTipo"
                                ng-options="tipo.nome for tipo in tipoAgenti"
                                ng-change="selectChanged(tipo.nome)" required>
                            <option value="">Tipologia agente</option>
                        </select>
                    </div>
                    <div ng-show="validationFailed">
                        <div ng-show="agenteForm.selectTipo.$error.required"
                             class="clform__errorlabel">Non hai scelto di che tipo è il nuovo agente!</div>
                    </div>
                    <div ng-show="agenteForm.selectTipo.$dirty && agenteForm.selectTipo.$error.required"
                         class="clform__errorlabel">Non hai scelto di che tipo è il nuovo agente!</div>
                    <p class="placeholder">Scegli di che tipo è il nuovo agente</p>
                </div>
            </div>
            <div class="row form-sec">
                <div class="form-sec-title">
                    <p>Credenziali di accesso</p>
                    <p class="placeholder">Inserisci l'username e genera la password per l'agente</p>
                </div>
                <div class="form-group">
                    <label for="usernameText" class="clform__label">Username</label>
                    <input name="username"
                           ng-model="agente.username"
                           type="text"
                           class="form-control clform__input"
                           ng-blur="validaAgente()"
                           id="usernameText"
                           required />
                    <div ng-show="!agenteForm.username.$pristine">
                        <div ng-show="agenteForm.username.$error.required && agenteForm.username.$touched"
                             class="clform__errorlabel">L'username è obbligatoria</div>
                    </div>
                    <p class="placeholder">Inserisci l'username del nuovo agente</p>
                </div>
                <div class="form-group">
                    <label for="passwordText" class="clform__label">Password</label>
                    <input name="password"
                           ng-model="agente.password"
                           type="password"
                           class="form-control clform__input"
                           id="passwordText"
                           required />
                    <div ng-show="!agenteForm.password.$pristine">
                        <div ng-show="agenteForm.password.$error.required && agenteForm.password.$touched"
                             class="clform__errorlabel">La password è obbligatoria</div>
                    </div>
                    <p class="placeholder">Inserisci la password del nuovo agente</p>
                </div>
                <div class="row form-sec">
                    <div class="col-md-12">
                        <input type="submit"
                               class="btn btn-success btn-block"
                               value="Inserisci Agente"
                               ng-disabled="invalidAgente" />
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>