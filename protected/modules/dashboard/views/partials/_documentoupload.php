<?php 
    $cs = Yii::app()->clientScript;
    $cs->registerCssFile($this->module->assetsUrl . '/css/riepcliente.css');
?>
<div id="toolbar" class="col-md-12">
    <div class="col-md-12"></div>
</div>
<div class="container-fluid" id="riepcliente">
    <div class="row">
        <div id="cliente">
            <i class="fa fa-file"></i>
            <b>Carica un nuovo documento</b>
            <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id'=>'main-fs-form',
                    'enableAjaxValidation' => FALSE,
                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                ));
            ?>
                <div>
                    <?php
                        echo $form->fileField($model, 'documento',
                            array("id"        => "filedocumento",
                                  "onchange"  => "angular.element($('#main-fs-form')).scope().processForm($('#main-fs-form'))"
                        ));
                    ?>
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var height = $(window).height();
        $("#riepcliente").css("height", height);
    });
</script>