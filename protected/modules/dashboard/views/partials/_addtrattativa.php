<div id="addtrattativa" ui-view>
    <header>
        <h3 class="sec-title">Nuova Trattativa</h3>
        <?php $this->renderPartial('_descrizioneTrattativa'); ?>
    </header>
    <div class="col-md-8">
        <form name="trattativaForm" ng-submit="addTrattativa(trattativaForm)" class="clform" novalidate>
            <div class="row form-sec" ng-if="!cliente.privato && !cliente.societa" id="tipologia-cliente">
                <div class="form-sec-title">
                    <p>Tipologia Cliente</p>
                    <p class="placeholder">Chi è il cliente della trattativa? Un privato o una società?</p>
                </div>
                <div class="btn cl-btn addtrattativa-opt-btn"
                     ng-click = "cliente.privato = true; cliente.societa = false">
                    Privato<i class="fa fa-check sel-badge"></i>
                </div>
                <div class="btn cl-btn addtrattativa-opt-btn"
                     ng-click="cliente.societa = true; cliente.privato = false">
                    Società<i class="fa fa-check sel-badge"></i>
                </div>
                <div ng-show="validationFailed">
                    <div ng-show="!cliente.privato && !cliente.societa"
                         class="clform__errorlabel">Non hai scelto di che tipo è il cliente!</div>
                </div>
            </div>
            <div class="row form-sec" ng-if="cliente.privato || cliente.societa">
                <div ng-if="!isClienteSelected">
                    <div class="form-sec-title">
                        <p>Anagrafica cliente</p>
                        <p class="placeholder">Qual'è il cliente della trattativa? Scegli uno dei tuoi clienti</p>
                    </div>
                    <div class="col-md-12">
                        <label class="clform__label">Ragione Sociale</label>
                        <div class="row">
                            <div class="col-md-12">
                                <div ng-if="cliente.societa">
                                    <angucomplete-alt
                                        pause="300"
                                        minlength="3"
                                        selected-object="clienteSelected"
                                        remote-url="dashboardUtilities/societa?rag_soc="
                                        remote-url-data-field="clienti"
                                        title-field="rag_soc"
                                        description-field="piva"
                                        input-class="form-control clform__input"
                                        text-searching="Cerco cliente..."
                                        text-no-results="Nessun cliente trovato!" />
                                </div>
                                <div ng-if="cliente.privato">
                                    <angucomplete-alt
                                        pause="300"
                                        minlength="3"
                                        selected-object="clienteSelected"
                                        remote-url="dashboardUtilities/privato?rag_soc="
                                        remote-url-data-field="clienti"
                                        title-field="rag_soc"
                                        description-field="cod_fisc"
                                        input-class="form-control clform__input"
                                        text-searching="Cerco cliente..."
                                        text-no-results="Nessun cliente trovato!" />
                                </div>
                            </div>
                        </div>
                        <div ng-if="validationFailed && !cliente.rag_soc" class="clform__errorlabel">
                            Non hai scelto un tuo cliente per la trattativa. Inserisci la sua ragione sociale!
                        </div>
                        <p class="placeholder">Cerca la ragione sociale
                            <span ng-if="cliente.societa">della società</span>
                            <span ng-if="cliente.privato">del cliente</span>
                        </p>
                    </div>
                    <div class="col-md-4 col-md-offset-4">
                        <div ng-click="cliente.privato = false; cliente.societa = false"
                             class="btn btn-block clchangebtn">
                            <i class="fa fa-exchange"></i>Cambia tipologia cliente
                        </div>
                    </div>
                </div>
                <div ng-if="isClienteSelected">
                    <div class="col-md-12">
                        <h3><span class="clform__label">Cliente</span></h3>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="clform__label">Ragione Sociale</label>
                                <p>{{cliente.rag_soc}}</p>
                            </div>
                            <div class="col-md-6" ng-if="cliente.privato">
                                <label class="clform__label">Codice Fiscale</label>
                                <p>{{cliente.cod_fisc}}</p>
                            </div>
                            <div class="col-md-6" ng-if="cliente.societa">
                                <label class="clform__label">Partita Iva</label>
                                <p>{{cliente.piva}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="clform__label">Città</label>
                                <p>{{cliente.citta}}</p>
                            </div>
                            <div class="col-md-6">
                                <label class="clform__label">Data di prima visita</label>
                                <p>{{cliente.prima_visita | date : "dd/MM/yyyy"}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h3><span class="clform__label">Referente</span></h3>
                                <label class="clform__label">Nome e Cognome</label>
                                <p>{{cliente.referente}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="clform__label">Email</label>
                                <p>{{cliente.email_referente}}</p>
                            </div>
                            <div class="col-md-6">
                                <label class="clform__label">Telefono</label>
                                <p>{{cliente.telefono_referente}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div ng-click="cambiaCliente()" class="btn btn-block clchangebtn clchangebtn_chcliente">
                            <i class="fa fa-exchange"></i>Cambia Cliente
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-sec">
                <div class="form-sec-title">
                    <p>Informazioni trattativa</p>
                    <p class="placeholder">Quando verrà chiusa la trattativa? Qual'è il suo importo?</p>
                </div>
                <div class="col-md-12">
                    <label class="clform__label">Causale</label>
                    <?php if($numCausali <= 10): ?>
                        <div class="causale cl-select-wrap">
                            <select name="selectCausale"
                                    class="cl-select"
                                    ng-model="causali.selectedCausale"
                                    ng-options="causale.nome for causale in causali.listaCausali"
                                    ng-change="selectChanged(selectedCausale.nome, 'causale')" required>
                                <option value="">Causale Trattativa</option>
                            </select>
                        </div>
                        <div ng-show="validationFailed">
                            <div ng-show="trattativaForm.selectCausale.$error.required"
                                 class="clform__errorlabel">Non hai scelto la causale della trattativa!</div>
                        </div>
                        <div ng-show="trattativaForm.selectCausale.$dirty &&
                                          trattativaForm.selectCausale.$error.required"
                             class="clform__errorlabel">Scegli la causale della trattativa</div>
                    <?php else: ?>
                        <div class="click_autocomplete">
                            <angucomplete-alt
                                id="txtcausali"
                                pause="300"
                                minlength="2"
                                maxlength="10"
                                selected-object="causaleSelected"
                                remote-url="dashboardutilities/causale?c="
                                remote-url-data-field="causali"
                                title-field="nome"
                                input-class="form-control clform__input" />
                        </div>
                    <?php endif; ?>
                    <p class="placeholder">Scegli o inserisci il motivo della trattativa. Per esempio: Efficentamento</p>
                </div>
                <div class="col-md-8">
                    <label class="clform__label">Categoria</label>
                    <div class="categoria cl-select-wrap">
                        <select name="selectCategoria"
                                class="cl-select"
                                ng-model="categorie.selectedCategoria"
                                ng-options="categoria.nome for categoria in categorie.listaCategorie"
                                ng-change="selectChanged(selectedCategoria.nome, 'categoria')" required>
                            <option value="">Categoria Trattativa</option>
                        </select>
                    </div>
                    <div ng-show="validationFailed">
                        <div ng-show="trattativaForm.selectCategoria.$error.required"
                             class="clform__errorlabel">Non hai scelto la categoria della trattativa!</div>
                    </div>
                    <div ng-show="trattativaForm.selectCategoria.$dirty &&
                                    trattativaForm.selectCategoria.$error.required"
                         class="clform__errorlabel">Non hai scelto la categoria della trattativa!</div>
                    <p class="placeholder">Scegli o inserisci la categoria</p>
                </div>
                <div class="col-md-4">
                    <label class="clform__label">Mese Acquisizione</label>
                    <div class="meseacquisizione cl-select-wrap">
                        <select name="selectMese"
                                class="cl-select"
                                ng-model="selectedMese"
                                ng-options="mese.nome for mese in mesi"
                                ng-change="selectChanged(selectedMese.nome, 'mese')" required>
                            <option value="">Mese di acquisizione</option>
                        </select>
                    </div>
                    <div ng-show="validationFailed">
                        <div ng-show="trattativaForm.selectMese.$error.required"
                             class="clform__errorlabel">Non hai scelto il mese trattativa!</div>
                    </div>
                    <div ng-show="trattativaForm.selectMese.$dirty &&
                                    trattativaForm.selectMese.$error.required"
                         class="clform__errorlabel">Non hai scelto il mese trattativa!</div>
                    <p class="placeholder">Scegli il mese di acquisizione</p>
                </div>
                <div class="col-md-6">
                    <label class="clform__label">Valore Offerta</label>
                    <div counter value="trattativa.importo" min="0" step="100" editable></div>
                    <div ng-show="validationFailed && trattativa.importo == 0"
                         class="clform__errorlabel">Importo non valido</div>
                    <p class="placeholder">Inserisci l'importo dell'offerta per esempio: 1000€</p>
                </div>
                <div class="col-md-6">
                    <label class="clform__label">Validità Offerta</label>
                    <div counter value="trattativa.scadenza" min="1" editable></div>
                    <p class="placeholder">Numero di giorni entro il quale verrà presa una decisione</p>
                </div>
            </div>
            <div class="row form-sec" style="text-align: center;">
                <div class="col-md-12">
                    <div class="form-sec-title">
                        <p>Che probabilità ha la trattativa di essere chiusa?</p>
                        <p class="placeholder">Scegli la probabilità di acquisizione da assegnare alla trattativa</p>
                    </div>
                    <div ng-repeat = "prob in probabilita"
                         ng-click  = "probSelected(prob, $index)"
                         ng-class  = "{'cl-btn-active' : buttonSel.btnIndex == $index}"
                         class="btn cl-btn addtrattativa-opt-btn">
                        {{prob.nome}}
                        <i class="fa fa-check sel-badge"></i>
                    </div>
                    <div ng-if="validationFailed && !trattativa.probabilita" class="clform__errorlabel">
                        Non hai scelto qual'è la probabilità che la trattativa venga chiusa!
                    </div>
                </div>
            </div>
            <div class="row form-sec">
                <div class="col-md-12">
                    <div class="form-sec-title">
                        <p>Note</p>
                        <p class="placeholder">Inserisci eventuali note da assegnare alla trattativa</p>
                    </div>
                    <textarea ng-model="trattativa.note" class="form-control clform__input" rows="10" />
                </div>
            </div>
            <div class="row form-sec">
                <div class="col-md-12">
                    <input type="submit" class="btn btn-success btn-block" value="Inserisci Trattativa" />
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        function fixHeader(){
            var el = $("header"),
                offset = el.offset(),
                scrollTop = $(window).scrollTop();
            if (scrollTop > el.height()) {
                el.addClass("fixed");
            } else {
                el.removeClass("fixed");
            }
        }
        $(window)
            .scroll(fixHeader)
            .trigger("scroll");
    });
</script>