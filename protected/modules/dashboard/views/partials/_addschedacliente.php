<?php
    Yii::app()->clientScript->registerCssFile($this->module->assetsUrl . '/css/addcliente.css');
?>
<div id="toolbar" class="col-md-12">
    <div class="col-md-12">
        <h1 id="toolbar-title">Nuovo Cliente</h1>
    </div>
</div>
<div class="container-fluid" id="addcliente">
    <div class="row">
        <div class="col-md-7">
            <form novalidate class="form-horizontal">
                <div class="form-group">
                    <label for="nomeText" class="col-sm-3 control-label">Nome</label>
                    <div class="col-sm-9">
                        <input ng-model="user.nome" type="text" class="form-control" id="nomeText" placeholder="Inserisci il nome del cliente. Es.: Mario">
                    </div>
                </div>
                <div class="form-group">
                    <label for="cognomeTex" class="col-sm-3 control-label">Cognome</label>
                    <div class="col-sm-9">
                        <input ng-model="user.cognome" type="text" class="form-control" id="cognomeTex" placeholder="Inserisci il cognome del cliente. Es.: Rossi">
                    </div>
                </div>
                <div class="form-group">
                    <label for="ragsocText" class="col-sm-3 control-label">Ragione Sociale</label>
                    <div class="col-sm-9">
                        <input ng-model="user.rag_soc" type="text" class="form-control" id="ragsocText" placeholder="Inserisci la ragione sociale">
                    </div>
                </div>
                <div class="form-group">
                    <label for="indirizzoText" class="col-sm-3 control-label">Indirizzo</label>
                    <div class="col-sm-9">
                        <input ng-model="user.indirizzo" type="text" class="form-control" id="indirizzoText" placeholder="Inserisci l'indirizzo della sede legale">
                    </div>
                </div>
                <div class="form-group">
                    <label for="emailText" class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                        <input ng-model="user.email" type="text" class="form-control" id="emailText" placeholder="Inserisci l'indirizzo mail del cliente">
                    </div>
                </div>
                <div class="form-group">
                    <label for="modpagText" class="col-sm-3 control-label">Modalità di pagamento</label>
                    <div class="col-sm-9">
                        <input ng-model="user.mod_pag" type="text" class="form-control" id="modpagText" placeholder="Inserisci le modalità di pagamento">
                    </div>
                </div>
                <div class="form-group">
                    <label for="fidoText" class="col-sm-3 control-label">Fido richiesto</label>
                    <div class="col-sm-9">
                        <input ng-model="user.fido" type="text" class="form-control" id="fidoText" placeholder="Inserisci il fido richiesto per il cliente">
                    </div>
                </div>
                <div class="form-group">
                    <label for="privaText" class="col-sm-3 control-label">Partita Iva</label>
                    <div class="col-sm-9">
                        <input ng-model="user.piva" type="text" class="form-control" id="privaText" placeholder="Inserisci la partita iva">
                    </div>
                </div>
                <div class="form-group">
                    <label for="codfiscText" class="col-sm-3 control-label">Codice Fiscale</label>
                    <div class="col-sm-9">
                        <input ng-model="user.cod_fisc" type="text" class="form-control" id="codfiscText" placeholder="Inserisci il codice fiscale">
                    </div>
                </div>
                <div class="form-group">
                    <label for="nazioneText" class="col-sm-3 control-label">Nazione</label>
                    <div class="col-sm-9">
                        <input ng-model="user.nazione" type="text" class="form-control" id="nazioneText" placeholder="Inserisci la nazione">
                    </div>
                </div>
                <div class="form-group">
                    <label for="capText" class="col-sm-3 control-label">Cap</label>
                    <div class="col-sm-9">
                        <input ng-model="user.cap" type="text" class="form-control" id="capText" placeholder="Inserisci il cap">
                    </div>
                </div>
                <div class="form-group">
                    <label for="cittaText" class="col-sm-3 control-label">Città</label>
                    <div class="col-sm-9">
                        <input ng-model="user.citta" type="text" class="form-control" id="cittaText" placeholder="Inserisci la citta">
                    </div>
                </div>
                <!--<div class="form-group">
                    <label class="col-sm-3 control-label">Ragione Sociale</label>
                    <div class="col-sm-9">
                        <select ng-model="user.idmittente" class="form-control rag-soc" style="width: 100%;">
                            <option value="select2/select2" selected="selected">Cerca la ragione sociale</option>
                        </select>
                    </div>
                </div>-->
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <btnaddcliente></btnaddcliente>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>