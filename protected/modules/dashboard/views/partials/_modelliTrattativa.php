<div ng-controller="ModelliTrattativaCtrl" class="row" id="modelli-trattativa">
    <div class="col-md-12" id="addmodelli-title">
        <h3 class="pull-left title">Modelli Inseriti</h3>
        <a ui-sref="trattativa.wizard.committente"
           ng-click="addModello(modello)"
           class="btn btn-addmodello">Aggiungi Modello</a>
    </div>
    <div id="lista-wrapper" class="col-md-12">
        <div ng-if="instNuove.length == 0 && instSost.length == 0" class="clmsg clmsg-empty">
            Nessun modello presente nella trattativa.
        </div>
        <div id="lista-modelli">
            <div ng-if="instNuove.length != 0" class="row">
                <div class="col-md-12">
                    <h3 class="inst-title" ng-if="instSost.length != 0">Nuove Installazioni</h3>
                    <div class="modello col-md-6"
                         ng-class="{'col-md-offset-3' : instNuove.length == 1}"
                         ng-repeat="modello in instNuove">
                        <div class="modello-info col-md-12">
                            <p class="tipo">{{modello.tipo}}</p>
                            <p class="nome">{{modello.nome}}</p>
                            <p class="codice">{{modello.codice}}</p>
                                        <span class="prezzo-watt">
                                            <p class="prezzo">€ {{modello.prezzo}}</p>
                                            <p>Watt <span class="watt">{{modello.watt_nom}}</span></p>
                                        </span>
                        </div>
                    </div>
                </div>
            </div>
            <div ng-if="instSost.length != 0" class="row" id="installazioni-sostitutive">
                <h3 class="inst-title" ng-if="instNuove.length != 0">Installazioni Sostitutive</h3>
                <div class="modello modello-sostituzione col-md-8 col-md-offset-2" ng-repeat="modello in instSost">
                    <div class="modello-info col-md-8">
                        <p class="tipo">{{modello.clMod.tipo}}</p>
                        <p class="nome">{{modello.clMod.nome}}</p>
                        <p class="codice">{{modello.clMod.codice}}</p>
                                    <span class="prezzo-watt">
                                        <p class="prezzo">€ {{modello.clMod.prezzo}}</p>
                                        <p>Watt <span class="watt">{{modello.clMod.watt_nom}}</span></p>
                                    </span>
                    </div>
                    <div class="exmodello-info col-md-4">
                        <div class="arr-sub">
                            <i class="fa fa-arrow-right"></i>
                        </div>
                        <p class="nome">{{modello.exMod.nome}}</p>
                        <p>Watt <span class="watt">{{modello.exMod.watt}}</span></p>
                        <p class="qta">Quantità {{modello.exMod.qta}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var listaModelli = $("#modelli-trattativa");
    var listHeight = $(window).height() - 50;
    listaModelli.css("min-height", listHeight);
    $(window).on("resize", function(){
        listaModelli.css("min-height", $(window).height() - 50);
    });
</script>