<?php
    $cs = Yii::app()->clientScript;
    $cs->registerCssFile($this->module->assetsUrl . '/css/riepilogo.css');
?>
<div class="container-fluid" id="riepilogo">
    <div class="row">
        <h1 ng-if="edit">Cliente modificato!</h1>
        <h1 ng-if="create">Cliente inserito!</h1>
<!--        <div>
            Vuoi inserire i modelli di lampade o neon per questa trattativa?
            <a ui-sref="addmodelli" class="btn btn-success">Inserisci modelli</a>
        </div>-->
    </div>
</div>