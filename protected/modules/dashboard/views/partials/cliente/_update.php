<?php
    $cs = Yii::app()->clientScript;
    $cs->registerCssFile($this->module->assetsUrl . '/css/addcliente.css');
?>
<div id="addcliente" class="row" ui-view>
    <header>
        <h3 class="sec-title">Modifica Cliente</h3>
    </header>
    <div class="col-md-7">
        <?php $this->renderPartial("cliente/_formCliente"); ?>
    </div>
</div>