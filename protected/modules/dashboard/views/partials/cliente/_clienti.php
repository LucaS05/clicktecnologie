<div class="container-fluid" ui-view>
    <div class="row">
        <div id="toolbar" class="col-md-12">
            <div class="col-md-10"><h1 class="toolbar__title">Clienti</h1></div>
            <div class="col-md-2">
                <a ui-sref=".nuovo" class="btn cl-addbtn">
                    <i class="fa fa-plus"></i> Nuovo Cliente
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div ng-if="clienti.length == 0" id="empty-filter">Nessun cliente presente!</div>
        <table ng-if="clienti.length != 0" class="table cltable table-hover table-clienti">
            <thead>
                <tr>
                    <th>Ragione Sociale</th>
                    <th>Prima Visita</th>
                </tr>
            </thead>
            <tbody>
                <tr class="cltable__row"
                    ng-repeat="clt in clienti"
                    ui-sref="cliente.modifica({codCliente : clt.id})">
                    <td class="cltable__cell_ragsoc">{{clt.rag_soc}}</td>
                    <td class="cltable__cell">{{clt.prima_visita | date:'dd/MM/yyyy'}}</td>
                    <td class="cltable__cell"><a class="btn cl-tblbtn">Modifica</a></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>