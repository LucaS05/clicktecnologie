<div class="container-fluid" ui-view>
    <div class="col-md-12">
        <h3><span class="clform__label">Cliente</span></h3>
        <div class="row">
            <div class="col-md-6">
                <label class="clform__label">Ragione Sociale</label>
                <p>{{cliente.rag_soc}}</p>
            </div>
            <div class="col-md-6">
                <label class="clform__label">Codice Fiscale</label>
                <p>{{cliente.cod_fisc}}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label class="clform__label">Città</label>
                <p>{{cliente.citta}}</p>
            </div>
            <div class="col-md-6">
                <label class="clform__label">Data di prima visita</label>
                <p>{{cliente.prima_visita | date : "dd/MM/yyyy"}}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3><span class="clform__label">Referente</span></h3>
                <label class="clform__label">Nome e Cognome</label>
                <p>{{cliente.referente}}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label class="clform__label">Email</label>
                <p>{{cliente.email_referente}}</p>
            </div>
            <div class="col-md-6">
                <label class="clform__label">Telefono</label>
                <p>{{cliente.telefono_referente}}</p>
            </div>
        </div>
    </div>
</div>