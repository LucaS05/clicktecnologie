<form name="clienteForm" ng-submit="addCliente(clienteForm)" novalidate>
    <div class="row form-sec" ng-if="!cliente.privato && !cliente.societa" id="tipologia-cliente">
        <div class="form-sec-title">
            <p>Tipologia Cliente</p>
            <p class="placeholder">Il cliente è un privato o una società?</p>
        </div>
        <div class="btn cl-btn addtrattativa-opt-btn"
             ng-click = "cliente.privato = true; cliente.societa = false">
            Privato<i class="fa fa-check sel-badge"></i>
        </div>
        <div class="btn cl-btn addtrattativa-opt-btn"
             ng-click="cliente.societa = true; cliente.privato = false">
            Società<i class="fa fa-check sel-badge"></i>
        </div>
    </div>
    <div class="row form-sec" ng-if="cliente.privato || cliente.societa">
        <div ng-if="showNotValid" id="msg-notvalidcliente">
            <?php $this->renderPartial('_messaggioErroreCliente'); ?>
        </div>
        <div ng-if="sameClientError" id="msg-notvalidcliente">
            <?php $this->renderPartial('_messaggioErroreClienteDuplicato'); ?>
        </div>
        <div class="form-sec-title">
            <p>Anagrafica cliente</p>
            <p class="placeholder">Qual'è il cliente della trattativa? Inserisci qui i suoi dati</p>
        </div>
        <div class="row">
            <div ng-if="cliente.societa">
                <div class="col-md-5">
                    <label for="pivaText" class="clform__label">Partita Iva</label>
                    <input input-number
                           id="pivaText"
                           type="text"
                           class="form-control clform__input"
                           ng-change="validaCliente()"
                           ng-model="cliente.piva"
                           ng-minlength="11"
                           ng-maxlength="11"
                           name="piva"
                           required />
                    <div ng-show="clienteForm.piva.$touched && clienteForm.piva.$error.minlength"
                         class="clform__errorlabel">La partita iva deve essere di 11 caratteri</div>
                    <div ng-show="clienteForm.piva.$touched && clienteForm.piva.$error.maxlength"
                         class="clform__errorlabel">La partita iva deve essere di 11 caratteri</div>
                    <div ng-show="clienteForm.piva.$touched && clienteForm.piva.$error.numbers"
                         class="clform__errorlabel">La partita iva deve contenere solo numeri</div>
                    <div ng-show="!clienteForm.piva.$pristine">
                        <div ng-show="clienteForm.piva.$error.required && clienteForm.piva.$touched"
                             class="clform__errorlabel">La partita iva è obbligatoria</div>
                    </div>
                    <p class="placeholder">Inserisci la partita iva della società</p>
                </div>
            </div>
            <div ng-if="cliente.privato">
                <div class="col-md-5">
                    <label for="codfiscText" class="clform__label">Codice Fiscale</label>
                    <invalid-len-display textfield="codfiscText"></invalid-len-display>
                    <input id="codfiscText"
                           class="form-control clform__input"
                           name="codfisc"
                           type="text"
                           ng-change="validaCliente()"
                           ng-model="cliente.cod_fisc"
                           ng-minlength="16"
                           ng-maxlength="16"
                           ng-model-options="{allowInvalid: true}"
                           maxlength="16"
                           required />
                    <div ng-show="clienteForm.codfisc.$touched && clienteForm.codfisc.$error.minlength"
                         class="clform__errorlabel">Il codice fiscale deve essere di 16 caratteri</div>
                    <div ng-show="clienteForm.codfisc.$error.maxlength"
                         class="clform__errorlabel">Il codice fiscale deve essere di 16 caratteri</div>
                    <div ng-show="!clienteForm.codfisc.$pristine">
                        <div ng-show="clienteForm.codfisc.$error.required && clienteForm.codfisc.$touched"
                             class="clform__errorlabel">Il codice fiscale è obbligatorio</div>
                    </div>
                    <p class="placeholder">Inserisci il codice fiscale del cliente</p>
                </div>
            </div>
            <div id="input-ragsoc" class="col-md-7">
                <label for="ragsocText" class="clform__label">Ragione Sociale</label>
                <div class="click_autocomplete">
                    <div class="row">
                        <div class="col-md-12">
                            <input id="ragsocText"
                                   type="text"
                                   name="ragsoc"
                                   class="form-control clform__input"
                                   ng-model="cliente.rag_soc"
                                   required />
                            <div ng-if="cliente.societa">
                                <div class="societa cl-select-wrap">
                                    <select name="selectSocieta"
                                            class="cl-select"
                                            ng-model="tipiSoc.selectedTipoSoc"
                                            ng-options="socItem.tipo for socItem in tipiSoc.listaTipoSoc"
                                            ng-change="selectChanged()" required>
                                        <option value="">Tipo Società</option>
                                    </select>
                                </div>
                                <div ng-show="validationFailed">
                                    <div ng-show="clienteForm.selectSocieta.$error.required"
                                         class="clform__errorlabel">Non hai il tipo di societa!</div>
                                </div>
                                <div ng-show="clienteForm.selectSocieta.$dirty &&
                                                      clienteForm.selectSocieta.$error.required" class="clform__errorlabel">
                                    Non hai scelto il tipo di società del cliente!</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div ng-show="!clienteForm.ragsoc.$pristine">
                    <div ng-show="clienteForm.ragsoc.$touched && clienteForm.ragsoc.$error.required"
                         class="clform__errorlabel">La ragione sociale del cliente è obbligatoria</div>
                </div>
                <p class="placeholder">Ragione sociale del cliente</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label class="clform__label">Città</label>
                <div ng-if="cliente.citta" class="clautocompselect">
                    <div class="clautocompselect__item">{{cliente.citta}}</div>
                    <div class="btn clautocompselect__button" ng-click="cliente.citta = false">Cambia Citta</div>
                </div>
                <div ng-if="!cliente.citta">
                    <div class="click_autocomplete">
                        <angucomplete-alt
                            pause="300"
                            minlength="2"
                            field-required
                            input-changed="comuneChanged"
                            selected-object="comuneSelected"
                            remote-url="dashboardUtilities/comuni?com="
                            remote-url-data-field="comuni"
                            title-field="comune"
                            description-field="provincia"
                            input-class="form-control clform__input"
                            text-searching="Cerco città..."
                            text-no-results="Nessuna città trovata..." />
                    </div>
                    <div ng-if="validationFailed && !cliente.citta" class="clform__errorlabel">
                        Non hai inserito la città del cliente</div>
                    <p class="placeholder">Scegli la città del cliente</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-md-offset-4" ng-if="!editMode">
            <div ng-click="cambiaTipoCliente()" class="btn btn-block clchangebtn">
                <i class="fa fa-exchange"></i>Cambia tipologia cliente
            </div>
        </div>
    </div>
    <div class="row form-sec">
        <div class="form-sec-title">
            <p>Prima Visita</p>
            <p class="placeholder">Quando ti sei recato per la prima volta dal cliente?</p>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label for="dataText" class="clform__label">Data</label>
                <div class="input-group">
                    <input type="text"
                           date-picker-locale
                           class="form-control"
                           datepicker-popup="{{format}}"
                           ng-model="cliente.prima_visita"
                           is-open="status.opened"
                           min-date="minDate"
                           datepicker-options="dateOptions"
                           ng-required="true"
                           close-text="Close" />
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default" ng-click="open($event)">
                                    Scegli la data
                                    <i class="glyphicon glyphicon-calendar"></i></button>
                            </span>
                </div>
                <div ng-if="validationFailed && !cliente.prima_visita" class="clform__errorlabel">
                    Non hai scelto la data di prima visista</div>
                <p class="placeholder">Scegli la data dal calendario</p>
            </div>
        </div>
    </div>
    <div class="row form-sec">
        <div class="form-sec-title">
            <p>Referente Trattativa</p>
            <p class="placeholder">Chi è il referente per l'azienda? Specifica le sue informazioni di contatto</p>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label for="referenteText" class="clform__label">Referente</label>
                <input name="referente"
                       ng-model="cliente.referente.nome"
                       type="text"
                       class="form-control clform__input"
                       id="referenteText"
                       required />
                <div ng-show="!clienteForm.referente.$pristine">
                    <div ng-show="clienteForm.referente.$error.required && clienteForm.referente.$touched"
                         class="clform__errorlabel">Un riferimento per il referente è obbligatorio</div>
                </div>
                <p class="placeholder">Inserisci nome e cognome o un riferimento per il referente per
                    questa trattativa</p>
            </div>
            <div class="col-md-6">
                <label for="referenteText" class="clform__label">Email</label>
                <input name="emailref"
                       ng-model="cliente.referente.email"
                       type="email"
                       class="form-control clform__input"
                       id="referenteText"
                       required />
                <div ng-show="!clienteForm.emailref.$pristine">
                    <p ng-show="clienteForm.emailref.$error.required && clienteForm.emailref.$touched"
                       class="clform__errorlabel">L'email del referente è obbligatoria</p>
                    <div ng-show="clienteForm.emailref.$touched">
                        <div class="clform__errorlabel" ng-show="clienteForm.emailref.$error.email">
                            Formato dell'email non corretto</div>
                    </div>
                </div>
                <p class="placeholder">Email del referente</p>
            </div>
            <div class="col-md-6">
                <label for="referenteText" class="clform__label">Telefono</label>
                <input input-number
                       name="telcliente"
                       id="referenteText"
                       type="text"
                       class="form-control clform__input"
                       ng-model="cliente.referente.telefono"
                       ng-maxlength="13"
                       required
                    />
                <div ng-show="!clienteForm.telcliente.$pristine">
                    <div ng-show="clienteForm.telcliente.$error.required && clienteForm.telcliente.$touched"
                         class="clform__errorlabel">Numero di telefono del referente obbligatorio</div>
                    <div ng-show="clienteForm.telcliente.$error.maxlength && clienteForm.telcliente.$touched"
                         class="clform__errorlabel">Numero di telefono non valido. Massimo 13 cifre</div>
                    <div ng-show="clienteForm.telcliente.$touched && clienteForm.telcliente.$error.numbers"
                         class="clform__errorlabel">Il numero di telefono deve contenere solo numeri</div>
                </div>
                <p class="placeholder">Telefono del referente</p>
            </div>
        </div>
    </div>
    <div class="row form-sec">
        <input type="submit"
               class="btn btn-success btn-block"
               ng-disabled="invalidCliente"
               value="{{saveLabel | capitalize}} Cliente" />
    </div>
</form>