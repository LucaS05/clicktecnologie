var clickCostants = angular.module("ClickCostants", []);

clickCostants.constant("ClCostants", {
    "FORMS" : {
        "EDIT": "modifica",
        "CREATE": "crea"
    },
    "API" : {
        "MAIN": {
            "CLIENTE" : "../api/cliente/:id",
            "LAMPADE_ESISTENTI" : "../api/lampesistenti",
            "PLAFONIERE" : "../api/plafoniere",
            "SOSTITUZIONE" : "../api/sostituzione",
            "WATT_PLAFONIERA" : "../api/wattplaf",
            "MODELLO_BY_WATT" : "../api/modwatt",
            "MODELLO_BY_CODICE" : "../api/modcod",
            "TRATTATIVA" : "../api/trattativa/:codtrattativa"
        },
        "DASHBOARD_UTILITIES" : {
            "VALIDA_CLIENTE" : "dashboardUtilities/validaCliente",
                "CAUSALI" : "dashboardUtilities/causale",
                "CATEGORIE" : "dashboardUtilities/categoria",
                "TIPI_SOCIETA" : "dashboardUtilities/tipisocieta"
        }
    },
    "EVENTS" : {
        "MODELLO_TRATTATIVA_ADDED" : "addModelloTrattativa"
    }
});