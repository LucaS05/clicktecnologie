var dsashboardApp = angular.module("ClickDashboard", ["ui.router", "clickControllers", "angucomplete-alt", "ngResource", "ClickCostants"]);

dsashboardApp.service("ClienteService", ["$resource", function($resource){
    this.createCliente = function (){};
    this.getCliente = function(idCliente){
        if(!angular.isUndefined(idCliente)){
            var Cliente = $resource("../api/cliente/:id", {
                id: ":id"
            });
            return Cliente.get({ id : idCliente });
        }
    };
    this.updateCliente = function(){};
}]);

dsashboardApp.config(["$stateProvider", "$urlRouterProvider", "ClCostants",
	function($stateProvider, $urlRouterProvider, ClCostants) {

        $urlRouterProvider.otherwise("trattative/");

        $stateProvider
            //.state('home', {
            //    url: "/",
            //    templateUrl: "partials/trattative",
            //    controller: "TrattativeCtrl"
            //})
            .state("clienti", {
                url: "/clienti/",
                templateUrl: "partials/clienti",
                controller: "ClientiCtrl"
            })
            .state("clienti.nuovo", {
                url: "nuovo/",
                templateUrl: "partials/createCliente",
                controller: "AddClienteCtrl",
                resolve:{
                    updateCliente: function(ClienteService, $stateParams){

                    },
                    form : function(){
                        return {tipo : ClCostants.FORMS.CREATE};
                    }
                }
            })
            .state("clienti.nuovo.riepilogo", {
                url: "riepilogo?mode",
                templateUrl: "partials/riepcliente",
                controller: "RiepilogoClienteCtrl"
            })
            .state("cliente", {
                url: "/clienti/:codCliente/",
                controller: "ClienteCtrl",
                templateUrl: "partials/cliente",
                resolve:{
                    cliente: function(ClienteService, $stateParams) {
                        return ClienteService.getCliente($stateParams.codCliente).$promise;
                    }
                }
            })
            .state("cliente.modifica", {
                url: "modifica/",
                templateUrl: "partials/updatecliente",
                controller: "AddClienteCtrl",
                resolve:{
                    updateCliente: function(ClienteService, $stateParams) {
                        return ClienteService.getCliente($stateParams.codCliente).$promise;
                    },
                    form : function(){
                        return {tipo : ClCostants.FORMS.EDIT};
                    }
                }
            })
            .state("cliente.modifica.riepilogo", {
                url: "riepilogo?mode",
                templateUrl: "partials/riepcliente",
                controller: "RiepilogoClienteCtrl"
            })
            .state("trattative", {
                url: "/trattative/",
                templateUrl: "partials/trattative",
                controller: "MainCtrl"
            })
            .state("trattative.nuova", {
                url: "nuova/",
                templateUrl: "partials/addtrattativa",
                controller: "AddTrattativaCtrl"
            })
            .state("trattative.nuova.riepilogo", {
                url: "riepilogo/",
                templateUrl: "partials/rieptrattativa",
                controller: "RiepTrattativaCtrl"
            })
            .state("trattativa", {
                url: "/trattative/:codTrattativa/",
                controller: "TrattativaCtrl",
                templateUrl: "partials/trattativa",
                abstract: true
            })
            .state("trattativa.riepilogo", {
                url: "riepilogo/",
                controller: "TrattativaRiepilogoCtrl",
                templateUrl: "partials/riepilogoTrattativa"
            })
            .state("trattativa.modelli", {
                url: "modelli/",
                templateUrl: "partials/modelliTrattativa"
            })
            .state("trattativa.wizard", {
                url: "addmodelli/",
                templateUrl: "partials/wizmodelli",
                abstract: true
            })
            .state("trattativa.wizard.committente", {
                url: "committente/",
                templateUrl: "partials/wizcommittente",
                controller: "CmtWizardCtrl"
            })
            .state("trattativa.wizard.clickmodel", {
                url: "modclick/",
                templateUrl: "partials/wizmodelloclick",
                controller: "ModWizardClickCtrl"
            })
            .state("trattativa.wizard.exmodel", {
                url: "modesist/",
                templateUrl: "partials/wizmodelloesistente",
                controller: "ModWizardEsistenteCtrl"
            })
            .state("trattativa.wizard.riepilogo", {
                url: "riepilogo/",
                templateUrl: "partials/wizriepilogo",
                controller: "ModWizardRiepilogoCtrl"
            });
    }
]);

dsashboardApp.directive('datePickerLocale', ["$parse", function($parse){
    var directive = {
        restrict : "A",
        require : "ngModel",
        link : link
    };
    return directive;

    function link(scope, element, attr, modelCtrl){
        modelCtrl.$parsers.push(function(viewValue){
            viewValue.setMinutes(viewValue.getMinutes() - viewValue.getTimezoneOffset());
            return viewValue;
        });

        /*ngModelController.$formatters.push(function(modelValue){
         if(!modelValue){
         return undefined;
         }
         var dt = new Date(modelValue);
         dt.setMinutes(dt.getMinutes() + dt.getTimezoneOffset());
         return dt;
         });*/
    }
}]);

dsashboardApp.filter("capitalize", function() {
    return function(token) {
        return token.charAt(0).toUpperCase() + token.slice(1);
    }
});

dsashboardApp.directive("btnAddModel", ["ClienteService", function (ClienteService){
    return {
        restrict: "E",
        replace: true,
        template: "<input type='submit' class='btn btn-success btn-block' value='Inserisci' />",
        link: function (scope, element, attrs) {
            scope.$watch("isValid", function (val) {
                element.attr("value", "Inserisci " + attrs.modello);
                if(val){
                    $(element).prop("disabled", false);
                } else {
                    $(element).prop("disabled", true);
                }
            });
        }
    };
}]);

dsashboardApp.directive('counter', function() {
    return {
        restrict: 'A',
        scope: { value: '=value' },
        template: '<div href="javascript:;" class="counter-minus input-group-addon" ng-click="minus()">-</div>\
                  <input type="text" class="counter-field form-control" ng-model="value" ng-change="changed()" ng-readonly="readonly">\
                  <div href="javascript:;" class="counter-plus input-group-addon" ng-click="plus()">+</div>',
        link: function( scope , element , attributes ) {
            // Make sure the value attribute is not missing.
            if ( angular.isUndefined(scope.value) ) {
                throw "Missing the value attribute on the counter directive.";
            }

            var min = angular.isUndefined(attributes.min) ? null : parseInt(attributes.min);
            var max = angular.isUndefined(attributes.max) ? null : parseInt(attributes.max);
            var step = angular.isUndefined(attributes.step) ? 1 : parseInt(attributes.step);

            element.addClass('counter-container input-group');

            // If the 'editable' attribute is set, we will make the field editable.
            scope.readonly = angular.isUndefined(attributes.editable) ? true : false;

            /**
             * Sets the value as an integer.
             */
            var setValue = function( val ) {
                scope.value = parseInt( val );
            };

            // Set the value initially, as an integer.
            setValue( scope.value );

            /**
             * Decrement the value and make sure we stay within the limits, if defined.
             */
            scope.minus = function() {
                if ( min && (scope.value <= min || scope.value - step <= min) || min === 0 && scope.value < 1 ) {
                    setValue( min );
                    return false;
                }
                setValue( scope.value - step );
            };

            /**
             * Increment the value and make sure we stay within the limits, if defined.
             */
            scope.plus = function() {
                if ( max && (scope.value >= max || scope.value + step >= max) ) {
                    setValue( max );
                    return false;
                }
                setValue( scope.value + step );
            };

            /**
             * This is only triggered when the field is manually edited by the user.
             * Where we can perform some validation and make sure that they enter the
             * correct values from within the restrictions.
             */
            scope.changed = function() {
                // If the user decides to delete the number, we will set it to 0.
                if ( !scope.value ) setValue( 0 );

                // Check if what's typed is numeric or if it has any letters.
                if ( /[0-9]/.test(scope.value) ) {
                    setValue( scope.value );
                }
                else {
                    setValue( scope.min );
                }

                // If a minimum is set, let's make sure we're within the limit.
                if ( min && (scope.value <= min || scope.value - step <= min) ) {
                    setValue( min );
                    return false;
                }

                // If a maximum is set, let's make sure we're within the limit.
                if ( max && (scope.value >= max || scope.value + step >= max) ) {
                    setValue( max );
                    return false;
                }

                // Re-set the value as an integer.
                setValue( scope.value );
            };
        }
    };
});

dsashboardApp.directive('ctoptbutton', function () {
    return {
        restrict: 'E',
        replace: true,
        scope : {
            selected : '=',
            type : '=',
            label : '='
        },
        template: '<div class="cl-btn" ng-click="setActive()">{{label}}<i class="fa fa-check sel-badge"></i></div>',
        link: function (scope, element, attr) {
            scope.setActive = function(){
                element.toggleClass("cl-btn-active");
            };
        }
    };
});