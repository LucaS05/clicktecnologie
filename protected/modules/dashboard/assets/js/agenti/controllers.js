var clickControllers = angular.module("clickControllers", ["ngResource", "ui.bootstrap", "ClickCostants"]);

clickControllers.factory("Modelli", ["$resource", function($resource){
    return $resource("/clicktecnologie/index.php/api/modello", {
        "tipo[]": "@tipo",
        prezzomin: "@prezzomin",
        prezzomax: "@prezzomax"
    });
}]);

clickControllers.service("TrattativaService", ["$resource", "ClCostants", function($resource, ClCostants){
    this.getTrattativa = function(codice){
        if(!angular.isUndefined(codice)){
            var Trattativa = $resource(ClCostants.API.MAIN.TRATTATIVA, {
                codtrattativa : ":codtrattativa"
            });
            return Trattativa.get({ codtrattativa : codice });
        }
    };
}]);

clickControllers.factory("listaModelli", ["modelParam",
    function(modelParam) {
        var installazioniSostitutive = [];
        var installazioniNuove = [];
        var totaleTrattativa = 0;
        return {
            aggiornaLista : function(){
                var modelloClick = modelParam.getModClick();
                var modelloEsistente = modelParam.getModEsistente();
                if(modelloEsistente.getStatus() === false) {
                    var prezzo = parseInt(modelloClick.getPrezzo());
                    totaleTrattativa += prezzo;
                    installazioniNuove.push(modelloClick.getModello());
                } else if(modelloEsistente.getStatus() === true){
                    var instSost = {};
                    instSost["exMod"] = modelloEsistente.getModello();
                    instSost["clMod"] = modelloClick.getModello();
                    installazioniSostitutive.push(instSost);
                }
                modelParam.resetModelli();
            },
            createList : function(){
                return {
                    getInstNuove : function(){
                        return installazioniNuove;
                    },
                    getInstSost : function(){
                        return installazioniSostitutive;
                    },
                    length : function(){
                        return installazioniNuove.length + installazioniSostitutive.length;
                    },
                    getTotale : function(){
                        return totaleTrattativa;
                    }
                };
            }
        }
    }
]);

clickControllers.factory("filtriClickStep", function() {
    var wattFilter = [];
    var plafoniera = {};
    return {
        setWattFilter : function(filter){
            if(filter instanceof Array){
                wattFilter = filter;
            }
        },
        getWattFilter : function(){
            return wattFilter;
        },
        setPlafoniera : function(plaf){
            if(plaf instanceof Object){
                plafoniera = plaf;
            }
        },
        getPlafoniera : function(){
            return plafoniera;
        }
    }
});

clickControllers.factory("modelParam", function(){
    var ClMod = function(){
        this.codice = null;
        this.nome = null;
        this.watt_nom = null;
        this.watt_reali = null;
        this.prezzo = null;
        this.tipo = null;
        this.qta = null;
        this.status = null;
    };

    var ExMod = function(){
        this.nome =  null;
        this.watt = 0;
        this.qta = 0;
        this.status = null;
    };

    var plafoniera = {
        plaf: null
    };

    var copyMod = function(src){
        var tempModel;
        if(src.status === true){
            tempModel = {};
            angular.copy(src, tempModel);
        }
        return tempModel;
    };

    var modelloClick = new ClMod();
    var modelloEsistente = new ExMod();

    return {
        getModEsistente : function(){
            return {
                setNome : function(nome){
                    if(typeof nome === "string"){
                        modelloEsistente.nome = nome;
                    }
                },
                getNome : function(){
                    return modelloEsistente.nome;
                },
                setQta : function(qta){
                    if(isNaN(qta) == false){
                        modelloEsistente.qta = qta;
                    }
                },
                getQta : function(){
                    return modelloEsistente.qta;
                },
                setWatt : function(watt){
                    if(isNaN(watt) == false){
                        modelloEsistente.watt = watt;
                    }
                },
                getWatt : function(){
                    return modelloEsistente.watt;
                },
                getStatus : function(){
                    return modelloEsistente.status;
                },
                setStatus : function(status){
                    if(typeof status === "boolean"){
                        modelloEsistente.status = status;
                    }
                },
                getModello : function(){
                    return copyMod(modelloEsistente);
                }
            }
        },
        getModClick : function(){
            return {
                setModello : function(mod){
                    if(angular.isObject(mod)){
                        modelloClick.codice = mod.codice;
                        modelloClick.nome = mod.nome;
                        modelloClick.watt_nom = mod.watt_nom;
                        modelloClick.watt_reali = mod.watt_reali;
                        modelloClick.prezzo = mod.prezzo;
                        modelloClick.tipo = mod.tipo;
                        modelloClick.status = true;
                    }
                },
                getModello : function(){
                    return copyMod(modelloClick);
                },
                getPrezzo : function(){
                    return modelloClick.prezzo;
                },
                setQta : function(qta){
                    if(isNaN(qta) == false) {
                        modelloClick.qta = qta;
                    }
                },
                getQta : function(){
                    return modelloClick.qta;
                },
                setStatus : function(status){
                    if(typeof status === "boolean") {
                        modelloClick.status = status;
                    }
                },
                getStatus : function(){
                    return modelloClick.status;
                }
            }
        },
        getModello : function(){
            var tempModel;
            if(modelloClick.status === true){
                tempModel = {};
                angular.copy(modelloClick, tempModel);
                angular.extend(tempModel, this.getModClick());
                if(modelloEsistente.status === true){
                    angular.copy(modelloEsistente, tempModel);
                    angular.extend(tempModel, this.getModEsistente());
                }
            }
            return tempModel;
        },
        resetModelli : function(){
            modelloClick = new ClMod();
            modelloEsistente = new ExMod();
        }
    };
});

//clickControllers.controller("MainCtrl", ["$scope", "$http",
//	function ($scope, $http) {
//		$http.get("../api/documenti").success(function(data) {
//			if( !(data instanceof Array) ){
//				$scope.documenti = [];
//			} else {
//				$scope.documenti = data;
//			}
//		});
//	}
//]);

clickControllers.controller("MessaggiCtrl", ["$scope", "$http",
    function ($scope, $http){
        $http.get("../api/messaggi").success(function(data) {
            if( !(data instanceof Array) ){
                $scope.messaggi = [];
            } else {
                $scope.messaggi = data;
            }
        });

        $scope.getMessaggio = function(msg){
            $http.get("../api/messaggio", { params: {"idmsg" : msg.id} }).success(function(data) {
                if( !(data instanceof Array) ){
                    $scope.messaggio = [];
                } else {
                    $scope.messaggio = data[0];
                }
            });
        }

    }
]);

clickControllers.controller("UploadDocCtrl", ["$scope", "$http",
    function ($scope, $http){
        $scope.processForm = function(form){

            var fd = new FormData(form[0]);

            $http.post("documento/carica", fd, {
                headers: {
                    "Content-Type": undefined
                },
                transformRequest: angular.identity
            }).success(function(data, status, headers, config){

            });

        };
    }
]);

clickControllers.controller("MainCtrl", ["$scope", "$http",
    function ($scope, $http){
        $http.get("../api/trattative").success(function(data) {
            if( !(data instanceof Array) ){
                $scope.trattative = [];
            } else {
                $scope.trattative = data;
            }
        });
    }
]);

clickControllers.controller("ClientiCtrl", ["$scope", "$http",
    function ($scope, $http){

        function aggiornaClienti() {
            $http.get("../api/clienti").success(function (data) {
                if (data instanceof Object){
                    $scope.clienti = data.clienti;
                } else {
                    $scope.clienti = [];
                }
            });
        }

        aggiornaClienti();
    }
]);

clickControllers.controller("ClienteCtrl", ["$scope", "cliente", "ClienteService",
    function ($scope, cliente, ClienteService){

    }
]);

clickControllers.controller("RiepilogoClienteCtrl", ["$scope", "$stateParams", "ClCostants",
    function ($scope, $stateParams, ClCostants){
        if($stateParams.mode === ClCostants.FORMS.EDIT){
            $scope.edit = true;
        } else if($stateParams.mode === ClCostants.FORMS.CREATE){
            $scope.create = true;
        }
    }
]);

clickControllers.controller("AddClienteCtrl", ["$scope", "$http", "$state", "updateCliente", "form", "ClCostants",
    function ($scope, $http, $state, updateCliente, form, ClCostants){
        $scope.tipiSoc = {selectedTipoSoc : null, listaTipoSoc : []};
        $scope.invalidCliente = false;
        $scope.format = "dd/MM/yyyy";
        $scope.pivacfLen = 0;
        $scope.sameClientError = false;

        if(form.tipo === ClCostants.FORMS.EDIT){
            $scope.editMode = true;
            $scope.saveLabel = ClCostants.FORMS.EDIT;
        } else if(form.tipo === ClCostants.FORMS.CREATE){
            $scope.saveLabel = ClCostants.FORMS.CREATE;
        }

        if(!angular.isUndefined(updateCliente)){
            $scope.cliente = {};
            $scope.referente = {};
            $scope.cliente.privato = updateCliente.privato;
            $scope.cliente.societa = updateCliente.societa;
            angular.extend($scope.cliente, updateCliente);
            var oldRagSoc = $scope.cliente.rag_soc;
            $scope.cliente.orag_soc = oldRagSoc;
            if($scope.cliente.societa){
                $scope.cliente.otipoSocieta = updateCliente.tipoSocieta;
            }
            $scope.referente.nome = $scope.cliente.referente;
            $scope.referente.email = $scope.cliente.email_referente;
            $scope.referente.telefono = $scope.cliente.telefono_referente;
            $scope.cliente.referente = $scope.referente;
            $scope.cliente.prima_visita = updateCliente.prima_visita;
            $scope.minDate = updateCliente.prima_visita;
        } else {
            $scope.referente = {};
            $scope.cliente = {prima_visita : new Date(), citta : null, referente : $scope.referente };
        }
        $scope.cambiaTipoCliente = function(){
            $scope.cliente = {};
            $scope.cliente.privato = false;
            $scope.cliente.societa = false;
        };

        $scope.clear = function () {
            $scope.prima_visita = null;
        };

        $scope.toggleMin = function() {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function() {
            $scope.status.opened = true;
        };

        $scope.status = {
            opened: false
        };

        $scope.dateOptions = {
            formatYear: "yy",
            startingDay: 1
        };

        function setUpdateTipoSocieta(){
            if(!angular.isUndefined(updateCliente) && updateCliente.societa){
                $scope.tipiSoc.listaTipoSoc.forEach(function(tipoSocObj){
                    if(tipoSocObj.tipo === updateCliente.tipoSocieta){
                        $scope.tipiSoc.selectedTipoSoc = tipoSocObj;
                    }
                });
            }
        }

        $http.get("dashboardUtilities/tipisocieta")
            .success(function(data) {
                $scope.tipiSoc.listaTipoSoc = data.societa;
                setUpdateTipoSocieta();
            });

        $scope.comuneSelected = function(selected){
            if(selected){
                $scope.cliente.citta = selected.originalObject.comune;
            }
        };

        $scope.validaCliente = function() {
            if ($scope.cliente.societa != undefined ||
                $scope.cliente.privato != undefined ) {
                var paramCliente = {};
                var cfLen = 0, pivaLen = 0;

                if($scope.cliente.societa){
                    if($scope.cliente.piva){
                        paramCliente["tipo"] = "societa";
                        paramCliente["piva"] = $scope.cliente.piva;
                        pivaLen = $scope.cliente.piva.length;
                    }
                }

                if($scope.cliente.privato){
                    if($scope.cliente.cod_fisc){
                        paramCliente["tipo"] = "privato";
                        paramCliente["codfisc"] = $scope.cliente.cod_fisc;
                        cfLen = $scope.cliente.cod_fisc.length;
                    }
                }

                if(cfLen === 16 || pivaLen === 11){
                    $http.get("dashboardUtilities/validaCliente", {
                        params: paramCliente
                    }).error(function(data) {
                        if (data.errore === "invalid") {
                            $scope.ragSocErr = data.rag_soc;
                            $scope.ag_nome = data.ag_nome;
                            $scope.ag_cognome = data.ag_cognome;
                            $scope.invalidCliente = true;
                            $scope.showNotValid = true;
                            $scope.sameClientError = false;
                        } else if(data.errore === "duplicato"){
                            if(!$scope.editMode === true){
                                $scope.invalidCliente = true;
                                $scope.sameClientError = true;
                                $scope.ragSocErr = data.rag_soc;
                            }
                        }
                    });
                } else {
                    $scope.invalidCliente = false;
                    $scope.sameClientError = false;
                    $scope.showNotValid = false;
                }
            }
        };

        $scope.selectChanged = function() {
            var tipoSocieta = $scope.tipiSoc.selectedTipoSoc.tipo;
            $scope.cliente.tipoSocieta = tipoSocieta;
        };

        $scope.validationFailed = false;
        $scope.addCliente = function(clienteForm){
            if(!$scope.cliente.citta || !$scope.cliente.prima_visita){
                clienteForm.$valid = false;
                $scope.validationFailed = true;
            }else if(clienteForm.$valid){
                if(form.tipo === ClCostants.FORMS.EDIT){
                    $http.post("cliente/update", {cliente: $scope.cliente}).
                        success(function(){
                            $state.go(".riepilogo", {mode : ClCostants.FORMS.EDIT});
                        });
                } else {
                    $http.post("cliente/create", {cliente: $scope.cliente}).
                        success(function(){
                            $state.go(".riepilogo", {mode : ClCostants.FORMS.CREATE});
                        });
                }
            } else {
                $scope.validationFailed = true;
                $scope.isValid = false;
            }
        }
    }
]);

clickControllers.directive("invalidLenDisplay",function() {
    return {
        restrict: "E",
        replace: true,
        template: "<label></label>",
        link : function (scope, $element, attrs) {

        }
    }
});

clickControllers.directive("inputNumber",function() {
    return {
        restrict: "A",
        require : "ngModel",
        link : function (scope, $element, attrs, modelCtrl){
            modelCtrl.$parsers.push(function(viewValue){
                if(!(/^\d+$/.test(viewValue))){
                    modelCtrl.$setValidity("numbers", false);
                    modelCtrl.$setValidity("maxlength", true);
                } else {
                    modelCtrl.$setValidity("numbers", true);
                }
                return viewValue;
            });
        }
    }
});

clickControllers.controller("AddTrattativaCtrl", ["$scope", "$http", "$state",
    function ($scope, $http, $state){
        $scope.trattativa = { probabilita: null, note: null, scadenza : 1, importo : 1};
        $scope.cliente = {};
        $scope.societa = null;
        $scope.mese = null;
        $scope.causali = {selectedCausale : null, listaCausali : []};
        $scope.categorie = {selectedCategoria: null, listaCategorie : []};
        $scope.isClienteSelected = false;

        $scope.mesi = [{nome : "Gennaio"}, {nome : "Febbraio"}, {nome : "Marzo"}, {nome : "Aprile"}, {nome : "Maggio"},
            {nome : "Giugno"}, {nome : "Luglio"}, {nome : "Agosto"}, {nome : "Settmbre"}, {nome : "Ottobre"},
            {nome : "Novembre"}, {nome : "Dicembre"}];

        $http.get("dashboardUtilities/causale")
            .success(function (data) {
                $scope.causali.listaCausali = data.causali;
            });

        $http.get("dashboardUtilities/categoria")
            .success(function (data) {
                $scope.categorie.listaCategorie = data.categorie;
            });

        $http.get("../api/probabilita")
            .success(function (data) {
                $scope.probabilita = data;
            });

        $scope.comuneChanged = function(strComune){
            if(strComune.length === 0){
                $scope.cliente.comune = null;
            }
        };

        var autoCompleteSelected = function(selected, tipo){
            if(selected != null){
                switch (tipo){
                    case "rag_soc":
                        $scope.cliente.rag_soc = selected.originalObject.rag_soc;
                        break;
                    case "comune":
                        $scope.cliente.comune = selected.originalObject;
                        break;
                }
            }
        };

        $scope.ragsocSelected = function(selected){
            if(selected){
                autoCompleteSelected(selected, "rag_soc");
            }
        };

        $scope.buttonSel = { btnIndex : -1 };
        $scope.probSelected = function(mod, index){
            $scope.buttonSel.btnIndex = index;
            $scope.trattativa.probabilita = mod;
            if($scope.validationFailed === true){
                $scope.validationFailed = false;
            }
        };


        $scope.cambiaCliente = function(){
            $scope.isClienteSelected = false;
            var cliente = {
                privato : $scope.cliente.privato,
                societa : $scope.cliente.societa
            };
            $scope.cliente = cliente;
        };

        $scope.validationFailed = false;
        $scope.addTrattativa = function(trattativaForm){
            if($scope.trattativa.importo == 0) {
                $scope.validationFailed = true;
                trattativaForm.$valid = false
            }
            if(!$scope.trattativa.probabilita) {
                $scope.validationFailed = true;
                trattativaForm.$valid = false
            }
            if(!trattativaForm.$valid) {
                $scope.validationFailed = true;
            } else {
                var categoria = $scope.categorie.selectedCategoria.nome;
                var causale = $scope.causali.selectedCausale.nome;
                $scope.trattativa.categoria = categoria;
                $scope.trattativa.causale = causale;
                $http.post("trattative/create", {trattativa : $scope.trattativa, cliente: $scope.cliente}).
                    success(function(){
                        $state.go(".riepilogo");
                    });
                $scope.validationFailed = false;
            }
        };

        $scope.selectChanged = function(selectVal, tipo){
            switch (tipo){
                case "societa":
                    $scope.cliente.tipoSocieta = selectVal;
                    break;
                case "causale":
                    $scope.trattativa.causale = selectVal;
                    break;
                case "categoria":
                    $scope.trattativa.categoria = selectVal;
                    break;
                case "mese":
                    $scope.trattativa.mese = selectVal;
                    break;
            }
        };

        $scope.clienteSelected = function(selected){
            if(selected){
                $scope.isClienteSelected = true;
                angular.extend($scope.cliente, selected.originalObject);
            }
        }
    }
]);

clickControllers.controller("RiepTrattativaCtrl", ["$scope", "$http",
    function ($scope, $http){

    }
]);

clickControllers.controller("TrattativaCtrl", ["$scope", "$stateParams", "$rootScope", "TrattativaService",
    function ($scope, $stateParams, $rootScope, TrattativaService){
        $scope.codTrattativa = $stateParams.codTrattativa;
        $scope.trattativa = TrattativaService.getTrattativa($scope.codTrattativa);

        $scope.$on("addModelloTrattativa", function() {
            $rootScope.$broadcast("addModello");
        });
    }
]);

clickControllers.controller("TrattativaRiepilogoCtrl", ["$scope", "$state",
    function ($scope, $state){

    }
]);

clickControllers.controller("ModelliTrattativaCtrl", ["$scope", "$http", "$rootScope", "listaModelli",
    function ($scope, $http, $rootScope, listaModelli){
        var modelliTrattativa = listaModelli.createList();
        $scope.instSost = modelliTrattativa.getInstSost();
        $scope.instNuove = modelliTrattativa.getInstNuove();
        $scope.listLength = modelliTrattativa.length();
        $scope.totale = modelliTrattativa.getTotale();
        $rootScope.$on("addModello", function() {
            listaModelli.aggiornaLista();
        });
    }
]);

clickControllers.controller("AddModelliCtrl", ["$scope", "$http", "$stateParams", "modelParam", "listaModelli", "Modelli",
    function ($scope, $http, $stateParams, modelParam, listaModelli, Modelli){
        $scope.codTrattativa = $stateParams.codTrattativa;
        $scope.modelli = [];
        $scope.modelliTrattativa = [];
        $scope.tipiModelli = [];
        $scope.checkFilter = [];
        $scope.textSearchFocus = false;
        var tipiFilter = [];

        $http.get("../api/tipimodello")
            .success(function(data){
                $scope.tipiModelli = data.tipi;
            });

        $scope.addModello = function(modello){
            modelParam.setClMod(modello);
        };

        $scope.$on("addModelloTrattativa", function() {
            var modello = modelParam.getModello();
            var clickModel = {modello : modello.clMod.mod, qta : modello.clMod.qta};
            listaModelli.addModello(modello);
            $scope.modelliTrattativa.push(clickModel);
            modelParam.resetModelli();
        });

        $scope.addModelli = function(){
            $http.post("../api/modello", {modelli: $scope.modelliTrattativa})
                .success(function(data){
                    console.log(data);
                });
        };

        $http.get("../api/modello")
            .success(function (data){
                $scope.modelli = data.modelli;
            });

        $scope.nomeCodMod = function(){
            var nomeCod = $scope.nomeCod;
            var listaModelli = Modelli.get({mod : nomeCod}, function(){
                $scope.modelli = listaModelli["modelli"];
            });
        };

        $scope.setFilter = function(tipo, checkIndex){
            var index = tipiFilter.indexOf(tipo.id);
            if(index == -1){
                $scope.checkFilter[checkIndex] = true;
                tipiFilter.push(tipo.id);
            } else {
                $scope.checkFilter[checkIndex] = false;
                tipiFilter.splice(index, 1);
            }

            if(tipiFilter.length >= 0){
                var q = null;
                if(tipiFilter.length > 0){
                    q = {tipo : tipiFilter.map(function(el){return "tipo[]="+el}).join("&")};
                }
                var listaModelli = Modelli.get(q, function(){
                    $scope.modelli = listaModelli["modelli"];
                });
            }
        };

        $scope.searchPHClick = function(){
            $scope.textSearchFocus = true;
        }
    }
]);

clickControllers.controller("CmtWizardCtrl", ["$scope", "modelParam", "$state",
    function ($scope, modelParam, $state){
        var modelloEsistente = modelParam.getModEsistente();
        $scope.exStatus = modelloEsistente.getStatus();
        $scope.chiudiWizard = function(){
            modelParam.resetModelli();
            $state.go("trattativa.listamodelli");
        };
        $scope.rispModEx = function(risp){
            modelloEsistente.setStatus(risp);
            if(risp){
                $state.go("trattativa.wizard.exmodel");
            } else {
                $state.go("trattativa.wizard.clickmodel");
            }
        };
    }
]);

clickControllers.controller("ModWizardEsistenteCtrl", ["$scope", "$http", "$state", "modelParam",
    function ($scope, $http, $state, modelParam){
        var modelloEsistente = modelParam.getModEsistente();
        $scope.quantita = 1;
        $scope.watt = 1;
        $scope.buttonSel = { btnIndex : -1 };
        var exStatus = modelloEsistente.getStatus();

        if(exStatus === null || exStatus === false){
            $state.go("trattativa.wizard.committente");
        }

        if(exStatus === true){
            //$scope.buttonSel.btnIndex = exMod.mod.id - 1;
            $scope.quantita = modelloEsistente.getQta();
            $scope.watt = modelloEsistente.getWatt();
        } else {
            $scope.buttonSel = { btnIndex : -1 };
        }

        $http.get("../api/lampesistenti")
            .success(function (data){
                $scope.modesistenti = data;
            });

        if($scope.modesistenti != undefined){
            $scope.modesistenti.forEach(function(element){
                element.statoSel = "inactive";
            });
        }

        $scope.modSelected = function(mod, index){
            $scope.buttonSel.btnIndex = index;
            modelloEsistente.setNome(mod.nome);
        };

        $scope.addExModel = function(){
            modelloEsistente.setWatt($scope.watt);
            modelloEsistente.setQta($scope.quantita);
        };
    }
]);

clickControllers.controller("ModWizardClickCtrl", ["$scope", "$state", "$http", "modelParam", "filtriClickStep",
    function($scope, $state, $http, modelParam, filtriClickStep) {
        var modelloEsistente = modelParam.getModEsistente();
        var modelloClick = modelParam.getModClick();
        var modClStatus = modelloClick.getStatus();
        var exStatus = modelloEsistente.getStatus();
        $scope.plafselected = false;
        $scope.clickselected = false;
        $scope.wattFilter = [];
        $scope.modelloEsistente = {};
        $scope.modelloEsistente.nome = modelloEsistente.getNome();
        $scope.modelloEsistente.watt = modelloEsistente.getWatt();
        $scope.quantita = 1;
        $scope.exModStatus = modelloEsistente.getStatus();
        $scope.clModStatus = modelloClick.getStatus();

        if(exStatus === null){
            $state.go("trattativa.wizard.committente");
        }

        var setWattFilter = function(watts, selectedItem){
            if(watts instanceof Array){
                var wattArray = [];
                var selectedWatt = null;
                watts.forEach(function(watt){
                    var filterObj = {watt : watt, label : watt + " Watt"};
                    wattArray.push(filterObj);
                    if(watt == selectedItem){
                        selectedWatt = watt;
                    }
                });
                return { watts : wattArray , selectedWatt : selectedWatt }
            }
        };

        if(modClStatus === true){
            var modelloClick = modelParam.getModClick();
            $scope.clickselected = true;
            $scope.plafselected = true;
            $scope.modClickSelected = modelloClick.getModello();
            $scope.quantita = modelloClick.getQta();
            $scope.tipoPlafoniera = filtriClickStep.getPlafoniera();
            $scope.wattFilter = filtriClickStep.getWattFilter();
        }

        $http.get("../api/plafoniere")
            .success(function(data) {
                $scope.tipoplafoniere = data.tipi;
            });

        $scope.tipoPlafSelected = function(tipoPlaf){
            $scope.plafselected = true;
            $scope.tipoPlafoniera = tipoPlaf;
            filtriClickStep.setPlafoniera(tipoPlaf);

            if(modelloEsistente.getStatus() === true) {
                $http.get("../api/sostituzione", {
                    params: {
                        watt_lampada: modelloEsistente.getWatt(),
                        tipo_lampada: modelloEsistente.getNome(),
                        tipo_plaf: tipoPlaf.nome
                    }
                }).success(function(data){
                    if(data.modelli != null){
                        $scope.wattFilter = [];
                        $scope.modelliClick = data.modelli;
                        var wattClick = setWattFilter(data.wattFilter, data.selectedWatt);
                        $scope.wattFilter = wattClick.watts;
                        $scope.selectedWatt = wattClick.selectedWatt;
                        filtriClickStep.setWattFilter($scope.wattFilter);
                        if(data.wattSostituzione != null){
                            if(data.wattSostituzione.length === 0) {
                                $scope.wattSostituzione = null;
                            } else {
                                $scope.wattSostituzione = data.wattSostituzione;
                            }
                        }
                    }
                });
            } else {
                $http.get("../api/wattplaf", {
                    params: {
                        tipo_plaf: tipoPlaf.nome
                    }
                }).success(function(data){
                    $scope.wattFilter = [];
                    var wattClick = setWattFilter(data.watt, data.selectedWatt);
                    $scope.wattFilter = wattClick.watts;
                });
            }
        };

        $scope.wattChanged = function(selectedWatt){
            $scope.selectedWatt = selectedWatt;
            $http.get("../api/modwatt", {
                params : {
                    tipo : $scope.tipoPlafoniera.nome,
                    watt : selectedWatt
                }
            }).success(function(data){
                $scope.modelliClick = data.modelli;
            });
        };

        $scope.cambioPlafoniera = function(){
            $scope.plafselected = false;
            $scope.clickselected = false;
            $scope.modelliClick = [];
        };

        $scope.cambioModello = function(){
            $scope.clickselected = false;
        };

        $scope.modExStatus = modelloEsistente.getStatus();

        $scope.clickModelSelected = function(modello) {
            $http.get("../api/modcod", {
                params: {
                    codice: modello.codice
                }
            }).success(function (data){
                $scope.clickselected = true;
                $scope.modClickSelected = data.modello;
            });
        };

        $scope.addClickModel = function(){
            /*modelParam.setClQta($scope.quantita);*/
            modelloClick.setModello($scope.modClickSelected);
        }
    }
]);

clickControllers.controller("ModWizardRiepilogoCtrl", ["$scope", "$state", "modelParam",
    function ($scope, $state, modelParam){
        $scope.addTrattModel = function(){
            $scope.$emit("addModelloTrattativa");
            $state.go("trattativa.listamodelli");
            modelParam.resetModelli();
        };
    }]
);