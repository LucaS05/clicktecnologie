var dsashboardApp = angular.module("ClickDashboard", ["ui.router", "clickControllers"]);

dsashboardApp.config(["$stateProvider", "$urlRouterProvider",
    function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise("agenti/");

        $stateProvider
            .state("agenti", {
                url: "/agenti/",
                templateUrl: "partials/agenti",
                controller: "AgentiCtrl"
            })
            .state("agenti.nuovo", {
                url: "nuovo/",
                templateUrl: "partials/createAgente",
                controller: "AgenteCtrl"
            })
            .state("agenti.nuovo.riepilogo", {
                url: "riepilogo?mode",
                templateUrl: "partials/riepagente",
                controller: "RiepilogoAgenteCtrl"
            })
            .state("agente", {
                url: "/clienti/:codCliente/",
                controller: "ClienteCtrl",
                templateUrl: "partials/cliente",
                resolve:{
                    cliente: function(ClienteService, $stateParams) {
                        return ClienteService.getCliente($stateParams.codCliente).$promise;
                    }
                }
            })
            .state("agente.modifica", {
                url: "modifica/",
                templateUrl: "partials/updatecliente",
                controller: "AddClienteCtrl",
                resolve:{
                    updateCliente: function(ClienteService, $stateParams) {
                        return ClienteService.getCliente($stateParams.codCliente).$promise;
                    },
                    form : function(){
                        return {tipo : ClCostants.FORMS.EDIT};
                    }
                }
            })
            .state("agente.modifica.riepilogo", {
                url: "riepilogo?mode",
                templateUrl: "partials/riepcliente",
                controller: "RiepilogoClienteCtrl"
            });
    }
]);