var clickControllers = angular.module("clickControllers", ["agente.service"]);

clickControllers.controller("AgentiCtrl", ["$scope", "$http", "$location",
    function($scope, $http, $location) {

        $http.get("../api/agenti").success(function(data) {

            if (!(data instanceof Array)) {
                $scope.agenti = [];
            } else {
                $scope.agenti = data;
            }

        });

        $scope.getSpedizione = function(spd){
            $location.path("spedizioni/" + spd.tracking);
        };

    }
]);

clickControllers.controller("AgenteCtrl", ["$scope", "$http", "$state", "AgenteService",
    function($scope, $http, $state, agenteService){
        $scope.agente = {};
        $scope.validationFailed = false;
        $http.get("../api/tipoagente").success(function(data) {
            if (!(data instanceof Array)) {
                $scope.tipoAgenti = [];
            }else{
                $scope.tipoAgenti = data;
                $scope.selectedTipo = data[0];
            }
        });
        $scope.addAgente = function(agenteForm){
            if(agenteForm.$valid){
                console.log($scope.agente);
                var successCallback = function(data){
                    console.log(data);
                    $state.go("agenti.nuovo.riepilogo");
                };
                var errorCallback = function(){
                    console.log("errorCallback");
                };
                agenteService.createAgente($scope.agente, successCallback, errorCallback);
            }
        };
        $scope.validaAgente = function(){
            if(angular.isDefined($scope.agente.username)){
                var agenteNotValid = function(){
                    $scope.validationFailed = true;
                    $scope.invalidAgente = true;
                };
                agenteService.validaAgente($scope.agente.username, agenteNotValid);
            }
        }
    }
]);

clickControllers.controller("RiepilogoAgenteCtrl", ["$scope", "$http", "$state", "AgenteService",
    function($scope, $http, $state, agenteService){

    }]
);