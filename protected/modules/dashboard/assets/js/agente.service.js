var dsashboardApp = angular.module("agente.service", ["ngResource"]);
dsashboardApp.service("AgenteService", ["$resource", function($resource){
    this.createAgente = function(agente, success, error){
        if(!angular.isUndefined(agente)) {
            var Agente = $resource("agente/create/");
            var agenteData = {agente: agente};
            Agente.save(agenteData, function(data){
                success(data);
            }, function(){
                error();
            })
        }
    };
    this.updateCliente = function(){};
    this.validaAgente = function(username, errorCallback){
        if(angular.isDefined(username)){
            var Validazione = $resource("dashboardUtilities/validaAgente");
            Validazione.get({username : username}, function(){}, function(){
                errorCallback();
            });
        }
    };
}]);