<?php
/* @var $this AgenteController */
/* @var $model Agente */

$this->breadcrumbs=array(
	'Agentes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Agente', 'url'=>array('index')),
	array('label'=>'Manage Agente', 'url'=>array('admin')),
);
?>

<h1>Create Agente</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>