<?php
/* @var $this AgenteController */
/* @var $model Agente */

$this->breadcrumbs=array(
	'Agentes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Agente', 'url'=>array('index')),
	array('label'=>'Create Agente', 'url'=>array('create')),
	array('label'=>'Update Agente', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Agente', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Agente', 'url'=>array('admin')),
);
?>

<h1>View Agente #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
		'cognome',
	),
)); ?>
