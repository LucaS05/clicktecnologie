<?php
/* @var $this AgenteController */
/* @var $model Agente */

$this->breadcrumbs=array(
	'Agentes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Agente', 'url'=>array('index')),
	array('label'=>'Create Agente', 'url'=>array('create')),
	array('label'=>'View Agente', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Agente', 'url'=>array('admin')),
);
?>

<h1>Update Agente <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>