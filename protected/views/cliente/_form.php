<?php
/* @var $this ClienteController */
/* @var $model Cliente */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cliente-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nome'); ?>
		<?php echo $form->textField($model,'nome',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nome'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cognome'); ?>
		<?php echo $form->textField($model,'cognome',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'cognome'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'citta'); ?>
		<?php echo $form->textField($model,'citta',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'citta'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'piva'); ?>
		<?php echo $form->textField($model,'piva'); ?>
		<?php echo $form->error($model,'piva'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rag_soc'); ?>
		<?php echo $form->textField($model,'rag_soc',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'rag_soc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cod_fisc'); ?>
		<?php echo $form->textField($model,'cod_fisc',array('size'=>16,'maxlength'=>16)); ?>
		<?php echo $form->error($model,'cod_fisc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'indirizzo'); ?>
		<?php echo $form->textField($model,'indirizzo',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'indirizzo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cap'); ?>
		<?php echo $form->textField($model,'cap'); ?>
		<?php echo $form->error($model,'cap'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nazione'); ?>
		<?php echo $form->textField($model,'nazione',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nazione'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'agente'); ?>
		<?php echo $form->textField($model,'agente'); ?>
		<?php echo $form->error($model,'agente'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->