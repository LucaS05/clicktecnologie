<?php
/* @var $this ClienteController */
/* @var $data Cliente */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nome')); ?>:</b>
	<?php echo CHtml::encode($data->nome); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cognome')); ?>:</b>
	<?php echo CHtml::encode($data->cognome); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('citta')); ?>:</b>
	<?php echo CHtml::encode($data->citta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('piva')); ?>:</b>
	<?php echo CHtml::encode($data->piva); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rag_soc')); ?>:</b>
	<?php echo CHtml::encode($data->rag_soc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cod_fisc')); ?>:</b>
	<?php echo CHtml::encode($data->cod_fisc); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('indirizzo')); ?>:</b>
	<?php echo CHtml::encode($data->indirizzo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cap')); ?>:</b>
	<?php echo CHtml::encode($data->cap); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nazione')); ?>:</b>
	<?php echo CHtml::encode($data->nazione); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agente')); ?>:</b>
	<?php echo CHtml::encode($data->agente); ?>
	<br />

	*/ ?>

</div>