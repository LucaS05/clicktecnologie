<?php

class RbacCommand extends CConsoleCommand
{

    private $_authManager;


    public function getHelp()
    {

        $description = "ClickTecnologie RBAC\n";
        $description .= '    '."Questo comando genera la RBAC per l'applicazione.\n";
        return parent::getHelp() . $description;
    }


    /**
     * The default action - create the RBAC structure.
     */
    public function actionIndex()
    {

        $this->ensureAuthManagerDefined();

        //provide the oportunity for the use to abort the request
        $message = "Il comando creerà i ruoli admin e agente";
        $message .= " e i seguenti permessi:\n";
        $message .= "crud per il modello utente,\n";
        $message .= "crud per il modello  documento\n";
        $message .= "crud per il modello  cliente\n";
        $message .= "Continuare?";

        //check the input from the user and continue if
        //they indicated yes to the above question
        if($this->confirm($message))
        {
            //first we need to remove all operations,
            //roles, child relationship and assignments
            $this->_authManager->clearAll();

            //create the lowest level operations for users
            $this->_authManager->createOperation(
                "createUser",
                "creare un nuovo utente");
            $this->_authManager->createOperation(
                "readUser",
                "leggere le info di un utente");
            $this->_authManager->createOperation(
                "updateUser",
                "modificare le informazioni di un utente");
            $this->_authManager->createOperation(
                "deleteUser",
                "rimuovere un utente");

            //create the lowest level operations for projects
            $this->_authManager->createOperation(
                "deleteDoc",
                "rimuovere un documento");

            $this->_authManager->createOperation(
                "createDoc",
                "creare un documento");

            $this->_authManager->createOperation(
                "updateDoc",
                "modificare un documento");

            $this->_authManager->createOperation(
                "readDoc",
                "leggere un documento");

            $this->_authManager->createOperation(
                "createCliente",
                "creare un cliente");

            $this->_authManager->createOperation(
                "readCliente",
                "leggere i dati di un cliente");

            $this->_authManager->createOperation(
                "updateCliente",
                "modificare un cliente");

            $this->_authManager->createOperation(
                "deleteCliente",
                "rimuovere un cliente");

            $this->_authManager->createOperation(
                "readAgente",
                "leggere i dati di un agente");

            //Ruolo: agente
            //CRUD Documenti
            //Read Agente
            //CRUD Cliente
            $role = $this->_authManager->createRole("agente");
            $role->addChild("readAgente");
            $role->addChild("createDoc");
            $role->addChild("readDoc");
            $role->addChild("updateDoc");
            $role->addChild("deleteDoc");
            $role->addChild("createCliente");
            $role->addChild("readCliente");
            $role->addChild("updateCliente");
            $role->addChild("deleteCliente");

            //Ruolo: admin
            //CRUD Utente
            //CRUD Documenti
            //CRUD Clienti
            $role=$this->_authManager->createRole("admin");
            $role->addChild("agente");
            $role->addChild("createUser");
            $role->addChild("readUser");
            $role->addChild("updateUser");
            $role->addChild("deleteUser");

            //provide a message indicating success
            echo "Authorization hierarchy successfully generated.\n";
        }
        else
            echo "Operation cancelled.\n";
    }

    public function actionDelete()
    {
        $this->ensureAuthManagerDefined();
        $message = "Questo comando elimina tutta la RBAC definita.\n";
        $message .= "Continuare?";
        //check the input from the user and continue if they indicated
        //yes to the above question
        if($this->confirm($message))
        {
            $this->_authManager->clearAll();
            echo "Authorization hierarchy removed.\n";
        }
        else
            echo "Delete operation cancelled.\n";

    }

    public function actionAddAdmin($uid)
    {
        Yii::app()->authManager->assign("admin", $uid);
    }

    public function actionAddAgente($uid)
    {
        Yii::app()->authManager->assign("agente", $uid);
    }

    protected function ensureAuthManagerDefined()
    {
        //ensure that an authManager is defined as this is mandatory for creating an auth heirarchy
        if(($this->_authManager=Yii::app()->authManager)===null)
        {
            $message = "Error: an authorization manager, named 'authManager' must be con-figured to use this command.";
            $this->usageError($message);
        }
    }
}