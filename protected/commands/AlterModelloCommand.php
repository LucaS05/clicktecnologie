<?php

class RbacCommand extends CConsoleCommand
{
    public function getHelp()
    {

        $description = "ClickTecnologie Modifica Modelli\n";
        return parent::getHelp() . $description;
    }

    public function actionIndex()
    {
        $message = "Il comando modifica i modelli e i relativi watt reali";
        $message .= "Continuare?";

        if($this->confirm($message))
        {
            $modelli = Modello::model()->findAll();
            foreach($modelli as $modello){
                $modello->stato = 1;
                $modello->watt_reali = $modello->watt * 1.05;
                $modello->save();
            }
        } else {
            echo "Operation cancelled.\n";
        }
    }
}