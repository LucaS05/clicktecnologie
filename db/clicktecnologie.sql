-- phpMyAdmin SQL Dump
-- version 4.3.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Ott 09, 2015 alle 17:21
-- Versione del server: 5.6.23
-- PHP Version: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `clicktecnologie`
--
CREATE DATABASE IF NOT EXISTS `clicktecnologie` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `clicktecnologie`;

DELIMITER $$
--
-- Procedure
--
DROP PROCEDURE IF EXISTS `setLampade`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `setLampade`(IN `tipoLampada` INT)
BEGIN
 
 DECLARE v_finished INTEGER DEFAULT 0;
 DECLARE watt INT DEFAULT 0;
 
 -- declare cursor for employee email
 DECLARE wattCursor CURSOR FOR 
	SELECT tempwatt.watt FROM tempwatt;
 
 -- declare NOT FOUND handler
 DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET v_finished = 1;
 
 OPEN wattCursor;
 
 get_email: LOOP
 
 FETCH wattCursor INTO watt;
 
 IF v_finished = 1 THEN 
	 LEAVE get_email;
 END IF;
 
 -- build email list
 INSERT INTO tbl_lampade(tbl_lampade.tipo_lampada, tbl_lampade.watt_lampada) VALUES (tipoLampada, watt);
 
 END LOOP get_email;
 
 CLOSE wattCursor;
 
END$$

DROP PROCEDURE IF EXISTS `setSostituzione`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `setSostituzione`(IN tipoLampada INT, IN wattLampada INT, IN wattModello INT)
BEGIN
			DECLARE idModello INTEGER DEFAULT -1;
            DECLARE modFinished INTEGER DEFAULT 0;
            DECLARE modelloClick INTEGER DEFAULT -1;
            DECLARE modelloCursor CURSOR FOR 
                SELECT id FROM tbl_modello WHERE tbl_modello.watt_nom = wattModello;
            DECLARE CONTINUE HANDLER FOR NOT FOUND SET modFinished = 1;

			SELECT id INTO idModello FROM tbl_lampade WHERE watt_lampada = wattLampada AND tipo_lampada = tipoLampada;

            OPEN modelloCursor;
            set_sost: LOOP
                FETCH modelloCursor INTO modelloClick;
                IF modFinished = 1 THEN 
                    LEAVE set_sost;
                END IF;
                INSERT INTO tbl_sostituzione(lampada, tipo_induzione) VALUES(idModello, modelloClick);
            END LOOP set_sost;
            CLOSE modelloCursor;
	END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `sostituzionemodelli`
--
DROP VIEW IF EXISTS `sostituzionemodelli`;
CREATE TABLE IF NOT EXISTS `sostituzionemodelli` (
`idlampada` int(11)
,`tipo_lampada` varchar(255)
,`watt_lampada` int(11)
,`idinduzione` int(11)
,`codice` varchar(255)
,`tipo_plafoniera` varchar(255)
,`nome_induzione` varchar(255)
,`watt_induzione` int(11)
,`prezzo` decimal(10,0)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_agente`
--

DROP TABLE IF EXISTS `tbl_agente`;
CREATE TABLE IF NOT EXISTS `tbl_agente` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `cognome` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `cellulare` varchar(255) NOT NULL,
  `tipo` int(2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_auth_assignment`
--

DROP TABLE IF EXISTS `tbl_auth_assignment`;
CREATE TABLE IF NOT EXISTS `tbl_auth_assignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` int(11) NOT NULL,
  `bizrule` text,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_auth_item`
--

DROP TABLE IF EXISTS `tbl_auth_item`;
CREATE TABLE IF NOT EXISTS `tbl_auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_auth_item_child`
--

DROP TABLE IF EXISTS `tbl_auth_item_child`;
CREATE TABLE IF NOT EXISTS `tbl_auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_banche`
--

DROP TABLE IF EXISTS `tbl_banche`;
CREATE TABLE IF NOT EXISTS `tbl_banche` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_businessplan`
--

DROP TABLE IF EXISTS `tbl_businessplan`;
CREATE TABLE IF NOT EXISTS `tbl_businessplan` (
  `id` int(11) NOT NULL,
  `trattativa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_categorietratt`
--

DROP TABLE IF EXISTS `tbl_categorietratt`;
CREATE TABLE IF NOT EXISTS `tbl_categorietratt` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_causale`
--

DROP TABLE IF EXISTS `tbl_causale`;
CREATE TABLE IF NOT EXISTS `tbl_causale` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_cliente`
--

DROP TABLE IF EXISTS `tbl_cliente`;
CREATE TABLE IF NOT EXISTS `tbl_cliente` (
  `id` int(11) NOT NULL,
  `rag_soc` varchar(255) NOT NULL,
  `agente` int(11) NOT NULL,
  `citta` int(11) NOT NULL,
  `prima_visita` date NOT NULL,
  `stato` int(1) NOT NULL DEFAULT '1',
  `referente` varchar(255) NOT NULL,
  `email_referente` varchar(50) NOT NULL,
  `telefono_referente` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_cltbanche`
--

DROP TABLE IF EXISTS `tbl_cltbanche`;
CREATE TABLE IF NOT EXISTS `tbl_cltbanche` (
  `banca` int(11) NOT NULL,
  `cliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_cltmodpag`
--

DROP TABLE IF EXISTS `tbl_cltmodpag`;
CREATE TABLE IF NOT EXISTS `tbl_cltmodpag` (
  `mod_pag` int(11) NOT NULL,
  `cliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_comuni`
--

DROP TABLE IF EXISTS `tbl_comuni`;
CREATE TABLE IF NOT EXISTS `tbl_comuni` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `id_provincia` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8095 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_documento`
--

DROP TABLE IF EXISTS `tbl_documento`;
CREATE TABLE IF NOT EXISTS `tbl_documento` (
  `id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `data` datetime NOT NULL,
  `autore` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_lampade`
--

DROP TABLE IF EXISTS `tbl_lampade`;
CREATE TABLE IF NOT EXISTS `tbl_lampade` (
  `id` int(11) NOT NULL,
  `tipo_lampada` int(11) NOT NULL,
  `watt_lampada` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_mesi`
--

DROP TABLE IF EXISTS `tbl_mesi`;
CREATE TABLE IF NOT EXISTS `tbl_mesi` (
  `id` int(2) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_messaggio`
--

DROP TABLE IF EXISTS `tbl_messaggio`;
CREATE TABLE IF NOT EXISTS `tbl_messaggio` (
  `id` int(11) NOT NULL,
  `mittente` int(11) DEFAULT NULL,
  `destinatario` int(11) DEFAULT NULL,
  `contenuto` text NOT NULL,
  `data` datetime NOT NULL,
  `stato` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_migration`
--

DROP TABLE IF EXISTS `tbl_migration`;
CREATE TABLE IF NOT EXISTS `tbl_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_modello`
--

DROP TABLE IF EXISTS `tbl_modello`;
CREATE TABLE IF NOT EXISTS `tbl_modello` (
  `id` int(11) NOT NULL,
  `codice` varchar(255) NOT NULL,
  `nome` int(11) NOT NULL,
  `watt_nom` int(11) NOT NULL,
  `prezzo` decimal(10,0) NOT NULL,
  `watt_reali` int(11) NOT NULL,
  `stato` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_modname`
--

DROP TABLE IF EXISTS `tbl_modname`;
CREATE TABLE IF NOT EXISTS `tbl_modname` (
  `nome` varchar(255) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_modpag`
--

DROP TABLE IF EXISTS `tbl_modpag`;
CREATE TABLE IF NOT EXISTS `tbl_modpag` (
  `id` int(11) NOT NULL,
  `nome_mod` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_privato`
--

DROP TABLE IF EXISTS `tbl_privato`;
CREATE TABLE IF NOT EXISTS `tbl_privato` (
  `id` int(11) NOT NULL,
  `cliente` int(11) NOT NULL,
  `codfisc` varchar(16) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_probabilita`
--

DROP TABLE IF EXISTS `tbl_probabilita`;
CREATE TABLE IF NOT EXISTS `tbl_probabilita` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `prob_val` decimal(10,1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_province`
--

DROP TABLE IF EXISTS `tbl_province`;
CREATE TABLE IF NOT EXISTS `tbl_province` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `id_regione` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_regioni`
--

DROP TABLE IF EXISTS `tbl_regioni`;
CREATE TABLE IF NOT EXISTS `tbl_regioni` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_schedacliente`
--

DROP TABLE IF EXISTS `tbl_schedacliente`;
CREATE TABLE IF NOT EXISTS `tbl_schedacliente` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `cognome` varchar(255) NOT NULL,
  `piva` int(11) NOT NULL,
  `cod_fisc` char(16) NOT NULL,
  `indirizzo` varchar(255) NOT NULL,
  `cap` int(5) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mod_pag` int(11) NOT NULL,
  `fido` int(11) NOT NULL,
  `cliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_societa`
--

DROP TABLE IF EXISTS `tbl_societa`;
CREATE TABLE IF NOT EXISTS `tbl_societa` (
  `id` int(11) NOT NULL,
  `cliente` int(11) NOT NULL,
  `piva` varchar(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_sostituzione`
--

DROP TABLE IF EXISTS `tbl_sostituzione`;
CREATE TABLE IF NOT EXISTS `tbl_sostituzione` (
  `lampada` int(11) NOT NULL,
  `tipo_induzione` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_statitrat`
--

DROP TABLE IF EXISTS `tbl_statitrat`;
CREATE TABLE IF NOT EXISTS `tbl_statitrat` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_tipoagente`
--

DROP TABLE IF EXISTS `tbl_tipoagente`;
CREATE TABLE IF NOT EXISTS `tbl_tipoagente` (
  `id` int(2) NOT NULL,
  `nome` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_tipolampade`
--

DROP TABLE IF EXISTS `tbl_tipolampade`;
CREATE TABLE IF NOT EXISTS `tbl_tipolampade` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_tipoplafoniere`
--

DROP TABLE IF EXISTS `tbl_tipoplafoniere`;
CREATE TABLE IF NOT EXISTS `tbl_tipoplafoniere` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_tiposocieta`
--

DROP TABLE IF EXISTS `tbl_tiposocieta`;
CREATE TABLE IF NOT EXISTS `tbl_tiposocieta` (
  `id` int(11) NOT NULL,
  `tipo` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_trattative`
--

DROP TABLE IF EXISTS `tbl_trattative`;
CREATE TABLE IF NOT EXISTS `tbl_trattative` (
  `id` int(11) NOT NULL,
  `codice` varchar(255) NOT NULL,
  `cliente` int(11) NOT NULL,
  `prob` int(11) NOT NULL,
  `mese_acq` int(2) NOT NULL,
  `stato` int(11) NOT NULL DEFAULT '1',
  `data` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `importo` int(11) NOT NULL,
  `causale` int(11) NOT NULL,
  `gg_scadenza` int(11) NOT NULL,
  `note` text,
  `categoria` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_trattmodcl`
--

DROP TABLE IF EXISTS `tbl_trattmodcl`;
CREATE TABLE IF NOT EXISTS `tbl_trattmodcl` (
  `idmod` int(11) NOT NULL,
  `idtratt` int(11) NOT NULL,
  `qta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_trattmodes`
--

DROP TABLE IF EXISTS `tbl_trattmodes`;
CREATE TABLE IF NOT EXISTS `tbl_trattmodes` (
  `idtratt` int(11) NOT NULL,
  `idesis` int(11) NOT NULL,
  `qta` int(11) DEFAULT NULL,
  `watt` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `last_login_time` datetime DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL,
  `stato` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tempwatt`
--

DROP TABLE IF EXISTS `tempwatt`;
CREATE TABLE IF NOT EXISTS `tempwatt` (
  `watt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura per la vista `sostituzionemodelli`
--
DROP TABLE IF EXISTS `sostituzionemodelli`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sostituzionemodelli` AS select `tbl_sostituzione`.`lampada` AS `idlampada`,`tbl_tipolampade`.`nome` AS `tipo_lampada`,`tbl_lampade`.`watt_lampada` AS `watt_lampada`,`tbl_sostituzione`.`tipo_induzione` AS `idinduzione`,`tbl_modello`.`codice` AS `codice`,`tbl_tipoplafoniere`.`nome` AS `tipo_plafoniera`,`tbl_modname`.`nome` AS `nome_induzione`,`tbl_modello`.`watt_nom` AS `watt_induzione`,`tbl_modello`.`prezzo` AS `prezzo` from (((((`tbl_lampade` join `tbl_sostituzione` on((`tbl_lampade`.`id` = `tbl_sostituzione`.`lampada`))) join `tbl_modello` on((`tbl_sostituzione`.`tipo_induzione` = `tbl_modello`.`id`))) join `tbl_modname` on((`tbl_modello`.`nome` = `tbl_modname`.`id`))) join `tbl_tipoplafoniere` on((`tbl_modello`.`tipo` = `tbl_tipoplafoniere`.`id`))) join `tbl_tipolampade` on((`tbl_lampade`.`tipo_lampada` = `tbl_tipolampade`.`id`))) where (`tbl_modello`.`stato` = 1) order by `tbl_tipolampade`.`nome`;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_agente`
--
ALTER TABLE `tbl_agente`
  ADD PRIMARY KEY (`id`), ADD KEY `fk_agente_user` (`user_id`), ADD KEY `fk_agente_tipo` (`tipo`);

--
-- Indexes for table `tbl_auth_assignment`
--
ALTER TABLE `tbl_auth_assignment`
  ADD PRIMARY KEY (`itemname`,`userid`), ADD KEY `fk_auth_assignment_userid` (`userid`);

--
-- Indexes for table `tbl_auth_item`
--
ALTER TABLE `tbl_auth_item`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `tbl_auth_item_child`
--
ALTER TABLE `tbl_auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`), ADD KEY `fk_auth_item_child_child` (`child`);

--
-- Indexes for table `tbl_banche`
--
ALTER TABLE `tbl_banche`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_businessplan`
--
ALTER TABLE `tbl_businessplan`
  ADD PRIMARY KEY (`id`), ADD KEY `fk_businessplan_trattative_idtratt` (`trattativa`);

--
-- Indexes for table `tbl_categorietratt`
--
ALTER TABLE `tbl_categorietratt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_causale`
--
ALTER TABLE `tbl_causale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cliente`
--
ALTER TABLE `tbl_cliente`
  ADD PRIMARY KEY (`id`), ADD KEY `fk_clientetratt_agente` (`agente`), ADD KEY `fk_cliente_comuni_citta` (`citta`);

--
-- Indexes for table `tbl_cltbanche`
--
ALTER TABLE `tbl_cltbanche`
  ADD KEY `fk_cltbanche_cliente` (`cliente`), ADD KEY `fk_cltbanche_banche` (`banca`);

--
-- Indexes for table `tbl_cltmodpag`
--
ALTER TABLE `tbl_cltmodpag`
  ADD KEY `fk_cltmodpag_modpag` (`mod_pag`), ADD KEY `fk_cltmodpag_cliente` (`cliente`);

--
-- Indexes for table `tbl_comuni`
--
ALTER TABLE `tbl_comuni`
  ADD PRIMARY KEY (`id`), ADD KEY `id_provincia` (`id_provincia`);

--
-- Indexes for table `tbl_documento`
--
ALTER TABLE `tbl_documento`
  ADD PRIMARY KEY (`id`), ADD KEY `fk_documento_agente_autore` (`autore`);

--
-- Indexes for table `tbl_lampade`
--
ALTER TABLE `tbl_lampade`
  ADD PRIMARY KEY (`id`), ADD KEY `fk_lampade_tipolampade_tipolampada` (`tipo_lampada`);

--
-- Indexes for table `tbl_mesi`
--
ALTER TABLE `tbl_mesi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_messaggio`
--
ALTER TABLE `tbl_messaggio`
  ADD PRIMARY KEY (`id`), ADD KEY `fk_messaggio_agente_mittente` (`mittente`), ADD KEY `fk_messaggio_agente_destinatario` (`destinatario`);

--
-- Indexes for table `tbl_migration`
--
ALTER TABLE `tbl_migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `tbl_modello`
--
ALTER TABLE `tbl_modello`
  ADD PRIMARY KEY (`id`), ADD KEY `fk_modello_tipomodello` (`tipo`), ADD KEY `fk_modello_modname_nome` (`nome`);

--
-- Indexes for table `tbl_modname`
--
ALTER TABLE `tbl_modname`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_modpag`
--
ALTER TABLE `tbl_modpag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_privato`
--
ALTER TABLE `tbl_privato`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `unique_cf` (`codfisc`), ADD UNIQUE KEY `cliente` (`cliente`);

--
-- Indexes for table `tbl_probabilita`
--
ALTER TABLE `tbl_probabilita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_province`
--
ALTER TABLE `tbl_province`
  ADD PRIMARY KEY (`id`), ADD KEY `id_regione` (`id_regione`);

--
-- Indexes for table `tbl_regioni`
--
ALTER TABLE `tbl_regioni`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_schedacliente`
--
ALTER TABLE `tbl_schedacliente`
  ADD PRIMARY KEY (`id`), ADD KEY `fk_schedacliente_cliente` (`cliente`);

--
-- Indexes for table `tbl_societa`
--
ALTER TABLE `tbl_societa`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `unique_piva` (`piva`), ADD UNIQUE KEY `cliente` (`cliente`);

--
-- Indexes for table `tbl_sostituzione`
--
ALTER TABLE `tbl_sostituzione`
  ADD PRIMARY KEY (`lampada`,`tipo_induzione`), ADD KEY `fk_sostituzione_modello_induzione` (`tipo_induzione`);

--
-- Indexes for table `tbl_statitrat`
--
ALTER TABLE `tbl_statitrat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_tipoagente`
--
ALTER TABLE `tbl_tipoagente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_tipolampade`
--
ALTER TABLE `tbl_tipolampade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_tipoplafoniere`
--
ALTER TABLE `tbl_tipoplafoniere`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_tiposocieta`
--
ALTER TABLE `tbl_tiposocieta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_trattative`
--
ALTER TABLE `tbl_trattative`
  ADD PRIMARY KEY (`id`), ADD KEY `fk_zoneagenti_statitrat_stato` (`stato`), ADD KEY `fk_trattative_cliente` (`cliente`), ADD KEY `fk_trattatuve_probabilita_prob` (`prob`), ADD KEY `fk_trattative_causale` (`causale`), ADD KEY `fk_trattative_categoria` (`categoria`), ADD KEY `fk_trattative_mese_mese_acq` (`mese_acq`);

--
-- Indexes for table `tbl_trattmodcl`
--
ALTER TABLE `tbl_trattmodcl`
  ADD PRIMARY KEY (`idmod`,`idtratt`), ADD KEY `fk_lampadetratt_trattative_idtratt` (`idtratt`);

--
-- Indexes for table `tbl_trattmodes`
--
ALTER TABLE `tbl_trattmodes`
  ADD PRIMARY KEY (`idtratt`,`idesis`), ADD KEY `fk_modestratt_lampesistente_idesis` (`idesis`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tempwatt`
--
ALTER TABLE `tempwatt`
  ADD PRIMARY KEY (`watt`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_agente`
--
ALTER TABLE `tbl_agente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `tbl_banche`
--
ALTER TABLE `tbl_banche`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_businessplan`
--
ALTER TABLE `tbl_businessplan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_categorietratt`
--
ALTER TABLE `tbl_categorietratt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tbl_causale`
--
ALTER TABLE `tbl_causale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_cliente`
--
ALTER TABLE `tbl_cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_comuni`
--
ALTER TABLE `tbl_comuni`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8095;
--
-- AUTO_INCREMENT for table `tbl_documento`
--
ALTER TABLE `tbl_documento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_lampade`
--
ALTER TABLE `tbl_lampade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `tbl_messaggio`
--
ALTER TABLE `tbl_messaggio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `tbl_modello`
--
ALTER TABLE `tbl_modello`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=159;
--
-- AUTO_INCREMENT for table `tbl_modpag`
--
ALTER TABLE `tbl_modpag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_privato`
--
ALTER TABLE `tbl_privato`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_probabilita`
--
ALTER TABLE `tbl_probabilita`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_province`
--
ALTER TABLE `tbl_province`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT for table `tbl_regioni`
--
ALTER TABLE `tbl_regioni`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tbl_schedacliente`
--
ALTER TABLE `tbl_schedacliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_societa`
--
ALTER TABLE `tbl_societa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_statitrat`
--
ALTER TABLE `tbl_statitrat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_tipolampade`
--
ALTER TABLE `tbl_tipolampade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_tipoplafoniere`
--
ALTER TABLE `tbl_tipoplafoniere`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tbl_tiposocieta`
--
ALTER TABLE `tbl_tiposocieta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_trattative`
--
ALTER TABLE `tbl_trattative`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `tbl_agente`
--
ALTER TABLE `tbl_agente`
ADD CONSTRAINT `fk_agente_tipo` FOREIGN KEY (`tipo`) REFERENCES `tbl_tipoagente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_agente_user` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `tbl_auth_assignment`
--
ALTER TABLE `tbl_auth_assignment`
ADD CONSTRAINT `fk_auth_assignment_itemname` FOREIGN KEY (`itemname`) REFERENCES `tbl_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_auth_assignment_userid` FOREIGN KEY (`userid`) REFERENCES `tbl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `tbl_auth_item_child`
--
ALTER TABLE `tbl_auth_item_child`
ADD CONSTRAINT `fk_auth_item_child_child` FOREIGN KEY (`child`) REFERENCES `tbl_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_auth_item_child_parent` FOREIGN KEY (`parent`) REFERENCES `tbl_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `tbl_businessplan`
--
ALTER TABLE `tbl_businessplan`
ADD CONSTRAINT `fk_businessplan_trattative_idtratt` FOREIGN KEY (`trattativa`) REFERENCES `tbl_trattative` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `tbl_cliente`
--
ALTER TABLE `tbl_cliente`
ADD CONSTRAINT `fk_cliente_comuni_citta` FOREIGN KEY (`citta`) REFERENCES `tbl_comuni` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_clientetratt_agente` FOREIGN KEY (`agente`) REFERENCES `tbl_agente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `tbl_cltbanche`
--
ALTER TABLE `tbl_cltbanche`
ADD CONSTRAINT `fk_cltbanche_banche` FOREIGN KEY (`banca`) REFERENCES `tbl_banche` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_cltbanche_cliente` FOREIGN KEY (`cliente`) REFERENCES `tbl_schedacliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `tbl_cltmodpag`
--
ALTER TABLE `tbl_cltmodpag`
ADD CONSTRAINT `fk_cltmodpag_cliente` FOREIGN KEY (`cliente`) REFERENCES `tbl_schedacliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_cltmodpag_modpag` FOREIGN KEY (`mod_pag`) REFERENCES `tbl_modpag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `tbl_comuni`
--
ALTER TABLE `tbl_comuni`
ADD CONSTRAINT `tbl_comuni_ibfk_1` FOREIGN KEY (`id_provincia`) REFERENCES `tbl_province` (`id`) ON DELETE CASCADE;

--
-- Limiti per la tabella `tbl_documento`
--
ALTER TABLE `tbl_documento`
ADD CONSTRAINT `fk_documento_agente_autore` FOREIGN KEY (`autore`) REFERENCES `tbl_agente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `tbl_lampade`
--
ALTER TABLE `tbl_lampade`
ADD CONSTRAINT `fk_lampade_tipolampade_tipolampada` FOREIGN KEY (`tipo_lampada`) REFERENCES `tbl_tipolampade` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `tbl_messaggio`
--
ALTER TABLE `tbl_messaggio`
ADD CONSTRAINT `fk_messaggio_agente_destinatario` FOREIGN KEY (`destinatario`) REFERENCES `tbl_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
ADD CONSTRAINT `fk_messaggio_agente_mittente` FOREIGN KEY (`mittente`) REFERENCES `tbl_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Limiti per la tabella `tbl_modello`
--
ALTER TABLE `tbl_modello`
ADD CONSTRAINT `fk_modello_modname_nome` FOREIGN KEY (`nome`) REFERENCES `tbl_modname` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_modello_tipomodello` FOREIGN KEY (`tipo`) REFERENCES `tbl_tipoplafoniere` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `tbl_privato`
--
ALTER TABLE `tbl_privato`
ADD CONSTRAINT `fk_privato_cliente` FOREIGN KEY (`cliente`) REFERENCES `tbl_cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `tbl_province`
--
ALTER TABLE `tbl_province`
ADD CONSTRAINT `tbl_province_ibfk_1` FOREIGN KEY (`id_regione`) REFERENCES `tbl_regioni` (`id`) ON DELETE CASCADE;

--
-- Limiti per la tabella `tbl_schedacliente`
--
ALTER TABLE `tbl_schedacliente`
ADD CONSTRAINT `fk_schedacliente_cliente` FOREIGN KEY (`cliente`) REFERENCES `tbl_cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `tbl_societa`
--
ALTER TABLE `tbl_societa`
ADD CONSTRAINT `fk_societa_cliente` FOREIGN KEY (`cliente`) REFERENCES `tbl_cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `tbl_sostituzione`
--
ALTER TABLE `tbl_sostituzione`
ADD CONSTRAINT `fk_sostituzione_modello_induzione` FOREIGN KEY (`tipo_induzione`) REFERENCES `tbl_modello` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_sostituzione_tbllampade_lampada` FOREIGN KEY (`lampada`) REFERENCES `tbl_lampade` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `tbl_trattative`
--
ALTER TABLE `tbl_trattative`
ADD CONSTRAINT `fk_trattative_categoria` FOREIGN KEY (`categoria`) REFERENCES `tbl_categorietratt` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_trattative_causale` FOREIGN KEY (`causale`) REFERENCES `tbl_causale` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_trattative_cliente` FOREIGN KEY (`cliente`) REFERENCES `tbl_cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_trattative_mese_mese_acq` FOREIGN KEY (`mese_acq`) REFERENCES `tbl_mesi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_trattative_statitrat_stato` FOREIGN KEY (`stato`) REFERENCES `tbl_statitrat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_trattatuve_probabilita_prob` FOREIGN KEY (`prob`) REFERENCES `tbl_probabilita` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `tbl_trattmodcl`
--
ALTER TABLE `tbl_trattmodcl`
ADD CONSTRAINT `fk_lampadetratt_trattative_idtratt` FOREIGN KEY (`idtratt`) REFERENCES `tbl_trattative` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_tblmodelli_trattativa` FOREIGN KEY (`idmod`) REFERENCES `tbl_modello` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `tbl_trattmodes`
--
ALTER TABLE `tbl_trattmodes`
ADD CONSTRAINT `fk_modestratt_lampesistente_idesis` FOREIGN KEY (`idesis`) REFERENCES `tbl_tipolampade` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_modestratt_trattative_idtratt` FOREIGN KEY (`idtratt`) REFERENCES `tbl_trattative` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
